#-----Quick PLot---------
# Helper functions for quickly plotting and reading data in matplotlib

import matplotlib.pyplot as plt
import seaborn as sns
import csv as csv

#-------------------------------PLOT SETTINGS-----------------------------
sns.set_style("whitegrid")
default_dpi = 300
default_figsize = [8, 5]

#--------------------------------FUNCTIONS-----------------------------------
## Plots an errorbar plot in matplotlib.
# Reads in a csv file and outputs a graph and the data in a fixed format. Csv file
# must be in the format of x, y, yerr, xerr with the headers as the first row.
# Arguments:
#   filename: String of of .csv file and path to it
#   title: String of the title of the graph
#   x: Array for the independent variable to be stored
#   y: Array for dependent variable to be stored
#   xerr: Array for the error on x to be stored
#   yerr: Array for the error on y to be stored
# Returns:
#   matplotlib figure
def QuickPlotError(filename, title, x, y, legend_name = None, yerr = None, xerr = None):
    # Read the file
    with open(filename) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter = ',')
        line_count = 0
        for row in csv_reader:
            # Get axes titles from headers of rows in file
            if line_count == 0:
                xtitle = row[0]
                ytitle = row[1]
                if yerr is not None:
                    yerrtitle = row[2]
                if xerr is not None:
                    xerrtitle = row[3]
                line_count += 1
            # Read in data to input arrays
            else:
                x.append(float(row[0]))
                y.append(float(row[1]))
                if yerr is not None:
                    yerr.append(float(row[2]))
                else:
                    if xerr is not None:
                        xerr.append(float(row[2]))
                if xerr is not None:
                    xerr.append(float(row[3]))
                line_count += 1
    # Plot the data
    plt.figure(figsize = default_figsize, dpi = default_dpi)
    plt.errorbar(x, y, xerr = xerr, yerr = yerr, marker = 'x', markersize = 5, linestyle = "None", label = legend_name)
    plt.xlabel(xtitle)
    plt.ylabel(ytitle)
    plt.legend()
    plt.title(title)
