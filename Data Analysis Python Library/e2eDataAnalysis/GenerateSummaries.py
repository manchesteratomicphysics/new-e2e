import FileDialog # fileDialog.py in same folder
import json
from pathlib import Path
from fpdf import FPDF
fontPath = '/Windows/Fonts'
FPDF.SYSTEM_TTFONTS = fontPath

'''
Get all information in the Summary of json file.
Parameters:
    filepath - string of path to json file
Returns:
    Four lists: summaryList with all information about the run, EGList with
    all gun voltages and energies, A1List with all analyser 1 voltages, A2List
    with all analyser 2 voltages
Examples:
    getSummaryInfo('CH405_35_gun_0deg_SAS_190628_1047.json')
'''
def getSummaryInfo(filePath):
    with open(filePath, encoding = 'utf-8-sig') as f:
        data = json.load(f)
    summary = data['Summary']
    #fileName = f.name
    species = summary['Species']
    countType = summary['CountType']
    exptType = summary["ExperimentType"]
    startDate = summary['StartDate']
    stepTime = summary['StepTime']
    fCupCurrent = summary['FCup']['Current (µA)']
    fCupVoltage = summary['FCup']['Voltage (V)']
    description = summary['Description']
    gunE = summary['Gun']['Energies']
    gunAngle = summary['Gun']['Angles']

    run0Settings = data['Runs'][0]['Settings']
    gunE = run0Settings['Gun']['Set']['GUNE']
    gunC = run0Settings['Gun']['Set']['GUNC']
    gunF = run0Settings['Gun']['Set']['GUNF']
    grid = run0Settings['Gun']['Set']['GRID']
    anode = run0Settings['Gun']['Set']['ANODE']
    GL1B = run0Settings['Gun']['Set']['GL1B']
    GD1X = run0Settings['Gun']['Set']['GD1X']
    GD1Y = run0Settings['Gun']['Set']['GD1Y']
    GL1C = run0Settings['Gun']['Set']['GL1C']
    GD2X = run0Settings['Gun']['Set']['GD2X']
    GD2Y = run0Settings['Gun']['Set']['GD2Y']
    GL2B = run0Settings['Gun']['Set']['GL2B']
    GD3X = run0Settings['Gun']['Set']['GD3X']
    GD3Y = run0Settings['Gun']['Set']['GD3Y']

    class analyserVoltages:
        def __init__(self, analyser):
            self.analyser = analyser
            self.setV = run0Settings[self.analyser]['Set']
            self.measuredV = run0Settings[self.analyser]['Measured']

    A1 = analyserVoltages('A1')
    A2 = analyserVoltages('A2')

    summaryList = []
    EGList = []
    A1List = []
    A2List = []
    # summaryList.append(description)
    #summaryList.append(f'Folder:\t\t{f.relative_to(Path.home())}')
    summaryList.append(f'{"Filename:": <20}{filePath.stem}')
    summaryList.append(f'{"Path:": <20}{filePath.parent}')
    summaryList.append(f'{"Species:": <20}{species}')
    summaryList.append(f'{"Experiment Type:": <20}{exptType}')
    summaryList.append(f'{"Count Type:": <20}{countType}')
    summaryList.append(f'{"Start Date:": <20}{startDate}')
    summaryList.append(f'{"Step Time (s):": <20}{stepTime}')
    summaryList.append(f'{"FCup Current (µA):": <20}{fCupCurrent:.2f}')
    summaryList.append(f'{"FCup Voltage (V):": <20}{fCupVoltage}')
    summaryList.append(f'Description:')
    summaryList.append(f'{description}')
    # summaryList.append(f'Electron Gun Settings')
    EGList.append(f'{"Energy(eV):": <12}{gunE: >6}    {"Coarse (eV):": <14}{gunC:>6}    {"Fine (eV):":<12}{gunF: >6}')
    EGList.append(f'{"Grid (V):": <12}{grid: >6}    {"Anode (V):": <14}{anode: >6}    {"GL1B (V):":<12}{GL1B: >6}')
    EGList.append(f'{"GD1X (V):": <12}{GD1X: >6}    {"GD2X (V):": <14}{GD2X: >6}    {"GD3X (V):":<12}{GD3X: >6}')
    EGList.append(f'{"GD1Y (V):": <12}{GD1Y: >6}    {"GD2Y (V):": <14}{GD2Y: >6}    {"GD3Y (V):":<12}{GD3Y: >6}')
    EGList.append(f'{"GL1C (V):": <12}{GL1C: >6}    {"GL2B (V):": <14}{GL2B: >6}')
    # summaryList.append(f'A1 Settings')
    A1List.append(f'{"Element:": <13}Set (V):  Measured (V):')
    A1List.append(f'{"RSE:": <12}{A1.setV["RSE"]: >6.2f}{A1.measuredV["RSE"]: >15.2f}')
    A1List.append(f'{"LENS:": <12}{A1.setV["LENS"]: >6.2f}{A1.measuredV["LENS"]: >15.2f}')
    A1List.append(f'{"DX:": <12}{A1.setV["DX"]: >6.2f}{A1.measuredV["DX"]: >15.2f}')
    A1List.append(f'{"DY:": <12}{A1.setV["DY"]: >6.2f}{A1.measuredV["DY"]: >15.2f}')
    A1List.append(f'{"AM:": <12}{A1.setV["AM"]: >6.2f}{A1.measuredV["AM"]: >15.2f}')
    A1List.append(f'{"IH:": <12}{A1.setV["IH"]: >6.2f}{A1.measuredV["IH"]: >15.2f}')
    A1List.append(f'{"OH:": <12}{A1.setV["OH"]: >6.2f}{A1.measuredV["OH"]: >15.2f}')
    # summaryList.append(f'A2 Settings')
    A2List.append(f'{"Element:": <13}Set (V):  Measured (V):')
    A2List.append(f'{"RSE:": <12}{A2.setV["RSE"]: >6.2f}{A2.measuredV["RSE"]: >15.2f}')
    A2List.append(f'{"LENS:": <12}{A2.setV["LENS"]: >6.2f}{A2.measuredV["LENS"]: >15.2f}')
    A2List.append(f'{"DX:": <12}{A2.setV["DX"]: >6.2f}{A2.measuredV["DX"]: >15.2f}')
    A2List.append(f'{"DY:": <12}{A2.setV["DY"]: >6.2f}{A2.measuredV["DY"]: >15.2f}')
    A2List.append(f'{"AM:": <12}{A2.setV["AM"]: >6.2f}{A2.measuredV["AM"]: >15.2f}')
    A2List.append(f'{"IH:": <12}{A2.setV["IH"]: >6.2f}{A2.measuredV["IH"]: >15.2f}')
    A2List.append(f'{"OH:": <12}{A2.setV["OH"]: >6.2f}{A2.measuredV["OH"]: >15.2f}')
    return summaryList, EGList, A1List, A2List

'''
Prints all information in the Summary of a json data file
Parameters:
    filepath - string of path to json file
Returns:
    None
Examples:
    printSummary('CH405_35_gun_0deg_SAS_190628_1047.json')
'''
def printSummary(filePath):
    summaryList, EGList, A1List, A2List = getSummaryInfo(filePath)
    print(filePath.stem)
    for line in summaryList:
        print(line)
    print(EGList[0])
    print('Initial Electron Gun Settings')
    for line in EGList:
        print(line)
    print()
    print('Initial A1 Settings')
    for line in A1List:
        print(line)
    print()
    print('Initial A2 Settings')
    for line in A2List:
        print(line)

'''
Makes a pdf file of all information in the Summary of a json data file.
Outputs the pdf file in same directory as data file with name '[filename]_Summary.pdf'.
Parameters:
    filepath - string of path to json file
Returns:
    None
Examples:
    makeSummaryPDF('CH405_35_gun_0deg_SAS_190628_1047.json')
'''
def makeSummaryPDF(filePath, plot = None):
    summaryList, EGList, A1List, A2List = getSummaryInfo(filePath)
    pdf = FPDF()
    #pdf.add_font('Consola', '', "consola.ttf", uni=True)
    #pdf.add_font('Consola', 'B', "consolab.ttf", uni=True)
    #pdf.set_right_margin(margin= 30)
    pdf.add_page()

    pdf.set_font('Helvetica', 'B', 10)
    pdf.cell(0, 10, filePath.stem, 0, 1, 'C')
    # pdf.ln()
    #pdf.set_font('Consola', '', 9)
    for line in summaryList:
        pdf.multi_cell(0, 5, line, 0, 1)
    pdf.ln(1)
    #pdf.set_font('Consola', 'B', 9)
    pdf.multi_cell(0, 5, 'Electron Gun Settings', 0, 1)
    #pdf.set_font('Consola', '', 9)
    for line in EGList:
        pdf.multi_cell(0, 5, line, 0, 1)
    pdf.ln(1)
   # pdf.set_font('Consola', 'B', 9)
    pdf.multi_cell(0, 5, 'A1 Settings', 0, 1)
    #pdf.set_font('Consola', '', 9)
    for line in A1List:
        pdf.multi_cell(0, 5, line, 0, 1)
    pdf.ln(1)
    #pdf.set_font('Consola', 'B', 9)
    pdf.multi_cell(0, 5, 'A2 Settings', 0, 1)
   # pdf.set_font('Consola', '', 9)
    for line in A2List:
        pdf.multi_cell(0, 5, line, 0, 1)
    
    if plot != None:
        pdf.image(plot, x=70, y=110, h=95)
    pdfFileName = f'{filePath.parent/filePath.stem}_Summary.pdf'
    pdf.output(pdfFileName, 'F')
