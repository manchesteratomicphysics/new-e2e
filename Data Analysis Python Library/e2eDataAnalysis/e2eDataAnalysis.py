'''
This is a file containing useful functions for e2e data analysis for plotting
and analysing data
Author: Manish Patel
Created on: 12/12/2019
'''

import numpy as np
import importlib
import pandas as pd
import csv
from itertools import zip_longest
import json
import copy
from collections import defaultdict
import GenerateSummariesV1
from pathlib import Path
import plotly.graph_objects as go  
#Need to have kaleido installed or some other image export package is using plotly
#Go here https://plotly.com/python/static-image-export/ for more details

#-----------------------------------------HELPER FUNCTIONS----------------------------------------
'''
Opens a json file and returns the containing data.
Parameters:
    filepath - string name of the file.
    header - string of local path of file on device.
Returns:
    dictionary of data contained in the JSON file
'''
def openSaveFile(filePath, header = None):
    if header != None:
        filePath = filePath / (header + '.json')
    with open(filePath, encoding = 'utf-8-sig') as f:
        data = json.load(f)
    return data

'''
Recursive return from dictionary.
Feed it a dictionary (JSON) and a list of keystrings and it will return the value.
Parameters:
    data - dictionary of the data
    fields - list of strings listing the headings of the wanted data
Returns:
    list of data
Examples:
    energies = fromNestedDict(data, ['Summary', 'Gun', 'Energies'])
'''
def fromNestedDict(data, fields):

    firstDict = fields[0]
    remainingDicts = fields[1:]

    if remainingDicts:
        return fromNestedDict(data[firstDict], remainingDicts)
    else:
        return data[firstDict]
    
'''
Creates a csv file using pandas dataframe.
Parameters: 
    headers - array of strings used as column headers
    data - array of arrays of data used for the columns, must be same size as headers
    filename - string of the name of the csv file output
'''
def writeCSV(filename, headers, data):
    # Check for errors
    if len(headers) != len(data):
        print("Must have the same number of headers as data columns!")
        return
    # Create csv by using zip_longest
    with open(filename, 'w') as f:
        writer = csv.writer(f)
        writer.writerow(headers)
        rows = zip_longest(*data, fillvalue = '')
        for row in rows:
            writer.writerow(row)
        
'''
Slices a list by using minimum and maximum values.
Parameters:
    data - list of data
    min - slices the data including and past this value
    max - slices the data up to and including this max value
Returns:
    data - list of truncated data
Examples:
    data = truncateData(min)
'''
def truncateData(data, min = None, max = None):
    # Find the min limit in the list and remove elements before this
    if min != None:
        min_index = data.index(min)
        data = data[min_index:]
    # Find the max element in the list and remove elements after this
    if max != None:
        max_index = data.index(max)
        print(max_index)
        data = data[:max_index+1]
    return data

'''
Strips the duplicated fragment LabVIEW adds to the end of save files.
Parameters:
    filename - string of JSON file to be fixed
Returns:
    New JSON file with same name as the input JSON with '_formatted' at the end.
'''
def fixDuplicateEndingLabVIEWBug(filename):
    with open(filename, 'r', encoding="utf-8") as JSONfile:
        data = JSONfile.read()  #data is string type
    duplicateString = '''g":[
    ]
}g":[
    ]
}'''
    if(data[-len(duplicateString):] == duplicateString):
        data = data[:-12]
        newFilename = filename[:-5] +'_formatted.json'
        with open(newFilename, 'w', encoding="utf-8") as JSONfile:
            JSONfile.write(data)

'''
Normalises the peak of the dataset to a set value.
Parameters:
    data - list of data
    peak - value to normalise to
Returns:
    data - list of normalised data to the peak
'''
def normaliseToPeak(data, peak = None):
    maximum = max(data)  # find maximum of dataset
    if(peak != None):  # normalises to a given peak
        data = [index/max(data)*peak for index in data]
        return data
    else:  # normalise to the max of the data set
        data = [index/max(data) for index in data]
        return data

'''
Calculates the mean, standard error on the mean, and sorts values for data with multiple runs.
Parameters:
    x - independent variable which is run over multiple times in the data
    y - dependent variable with varying values for each x
    x_final - empty array to store the unique values of x
    y_final - empty array to store mean of y values for each unique x
    err_y_final - empty array to store standard error on mean for each unique x
'''
def multipleRunsToSingleValues(x, y, x_final, y_final, err_y_final, exclude_negative_y_in_mean = None):
    # Create dictionary with x value as a key and y as a list of values
    for xrun, yrun in zip(x,y):
        data_dict = defaultdict(list)  # Create empty dictionary with a list as a value
        xrun_final = []
        yrun_final = []
        erryrun_final = []
        for xvalue, yvalue in zip(xrun, yrun):  
            data_dict[xvalue].append(yvalue)
    
        # Now we have sorted by x so calculate the mean, range for each x
        for xvalue, yvalues in data_dict.items():
            xrun_final.append(xvalue)
            n = len(yvalues)
            # Exclude -ve y values in mean calculation
            if(exclude_negative_y_in_mean != None):
                ymean = []
                for y in yvalues:
                    if(y > 0):
                        ymean.append(y)
                yrun_final.append(sum(ymean)/len(ymean))  # mean of non-zero values
            else:
                yrun_final.append(sum(yvalues)/n)  # mean of values
            erryrun_final.append(np.std(yvalues)/(np.sqrt(n))) # standard error on mean
        # Append each run to the final arrays
        x_final.append(xrun_final)
        y_final.append(yrun_final)
        err_y_final.append(erryrun_final)
        
'''
Deletes from multiple lists based on the index of values found from one array.
Parameters:
    deletion_values - values to delete from the search_array
    search_array - contains values to delete. The indices of the matched deletion
    values will be used to delete the other arrays
    deletion_arrays - arrays which will be deleted from. Must match the size of the
    search array
'''
def deleteFromMultipleLists(deletion_values, search_array, deletion_arrays):
    # Find the indices to be deleted
    deletion_indices = []
    for value in deletion_values:
        deletion_indices += [i for i in range(len(search_array)) if search_array[i] == value]
    
    # Delete the corresponding indices in the deletion arrays
    for array in deletion_arrays:
        for i in sorted(deletion_indices, reverse = True):
            del array[i]

def flatten2DArray(two_D_array):
    '''
    Turns a 2D array into a 1D array by flattening it
    '''
    one_D_array = []
    for array in two_D_array:
        for element in array:
            one_D_array.append(element)
    return one_D_array

def sortMultipleRunData(x_runs, y_runs, err_y_runs, sorted_x_runs, sorted_y_runs, sorted_err_y_runs):
    '''
    Sorts x and y data for multiple runs.
    Inputs:
    - x_runs: A 2D array where each array contains the x values of each run
    - y_runs: A 2D array where each array contains the y values of each run
    - err_y_runs: A 2D array where each array contains the error on y values of each run
    - sorted_x_runs: Empty array to store sorted x values
    - sorted_y_runs: Empty array to store a 2D array of sorted y values for each run
    - sorted_err_y_runs: Empty array to store a 2D array of sorted errors on y for each run
    '''
    # Have to sort the data into a square matrix in order of x data
    # First get an array of the full range of sorted x data
    all_x_runs = []
    for x_run in x_runs:
        for x in x_run:
            all_x_runs.append(x)
    all_x_runs = sorted(set(all_x_runs))  # Get unique values and sort
    for x_val in all_x_runs:
        sorted_x_runs.append(x_val)
    

    # Make a 2D array of sorted x data for the length of data
    sorted_x_matrix = []
    for i in range(len(x_runs)):
        sorted_x_matrix.append(sorted_x_runs)

    # Make a dictionary for x, y and x, err_y pair for each run
    data_dict = [defaultdict(float) for i in range(len(y_runs))]
    error_dict = [defaultdict(float) for i in range(len(err_y_runs))]
    for i in range(len(y_runs)):
        for j in range(len(y_runs[i])):
            data_dict[i][x_runs[i][j]] = y_runs[i][j]
            error_dict[i][x_runs[i][j]] = err_y_runs[i][j]

    # Now we have a dictionary for each run need to loop over sorted angles
    # and append value if the key is found or append zero if key is not found
    for i in range(len(sorted_x_matrix)):
        sorted_y_run = []
        sorted_err_y_run = []
        for j in range(len(sorted_x_matrix[i])):
            if(sorted_x_matrix[i][j] in data_dict[i]):  # If element in the run dictionary
                sorted_y_run.append(data_dict[i][sorted_x_matrix[i][j]])
                sorted_err_y_run.append(error_dict[i][sorted_x_matrix[i][j]])
            else:
                sorted_y_run.append(0)
                sorted_err_y_run.append(0)
        sorted_y_runs.append(sorted_y_run)
        sorted_err_y_runs.append(sorted_err_y_run)
    

def weightedMeanAndErrorFromMultipleRuns(x_runs, y_runs, err_y_runs, sorted_x_runs, wy, wyerr):
    '''
    Takes a 2D array of y data and the error on y and calculates the 
    weighted mean for each y values using the error as weights 
    and calculates the error on y
    '''
    # First have to sort the data
    sorted_y_runs = []
    sorted_err_y_runs = []
    sortMultipleRunData(x_runs, y_runs, err_y_runs, sorted_x_runs, sorted_y_runs, sorted_err_y_runs)
    
    weighted_y_sum = []
    sum_of_weights = []
    
    # To iterate over the value for each run need to iterate over the columns
    iterable_y_err_runs = np.dstack(sorted_err_y_runs)[0]
    iterable_y_runs = np.dstack(sorted_y_runs)[0]
    
    # Calculate sum of weights Sum(1/w_i^2)
    for err_i in iterable_y_err_runs:  # Get the error for the i'th run
        weight = 0
        for err in err_i:
            if(err != 0):  # Check for zero
                weight += 1/(err*err)
        sum_of_weights.append(weight)
    
    # Calculate the weighted y sum Sum(w_i*x_i)
    for err_i, y_i in zip(iterable_y_err_runs, iterable_y_runs):
        weighted_sum = 0
        for err, y in zip(err_i, y_i):
            if(err != 0):  # Check for zero
                weighted_sum += y/(err*err)
        weighted_y_sum.append(weighted_sum)
    
    # Now calculate the weighted mean <y> = Sum(w_i*x_i)/Sum(w_i)
    for i in range(len(weighted_y_sum)):
        wy.append(weighted_y_sum[i]/sum_of_weights[i])
    
    # Calculate the error on the weighted mean sigma_<y> = sqrt(SUM(sigma_i^2))
    for err_i in iterable_y_err_runs:
        sum_err = 0
        for err in err_i:
            sum_err += err*err
        wyerr.append(np.sqrt(sum_err))

#---------------------------------------PLOTLY PLOTTING FUNCTIONS----------------------------------
'''
Implements default settings for a plot.
    goFig - figure object (plotly)
    title - string of the main title of the graph
    xaxis_label - string of the x-axis label
    yaxis_label - string of the y-axis label
'''
def plotlySetPlot(goFig, title = None, xaxis_label = None, yaxis_label = None):
    goFig.update_layout(paper_bgcolor = 'rgba(0,0,0,0)', plot_bgcolor = 'rgba(0,0,0,0)')
    goFig.update_xaxes(showline = True,
                     linewidth=1,
                     linecolor = 'black',
                     mirror = True,
                     ticks = "outside",
                     tickwidth = 1,
                     tickcolor = 'black',
                     ticklen = 4)

    goFig.update_yaxes(showline = True,
                     linewidth=1,
                     linecolor = 'black',
                     mirror = True,
                     ticks = "outside",
                     tickwidth = 1,
                     tickcolor = 'black',
                     ticklen = 4)
    if title != None:
        goFig.update_layout(title=
                            {
                                'text': title,
                                'x': 0.5,
                                'y': 0.85
                            })
    if xaxis_label != None:
        goFig.update_layout(xaxis_title = xaxis_label)
    if yaxis_label != None:
        goFig.update_layout(yaxis_title = yaxis_label)
