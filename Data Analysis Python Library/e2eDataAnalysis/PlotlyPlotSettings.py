import plotly.graph_objects as go
#Need to have kaleido installed or some other image export package is using plotly
#Go here https://plotly.com/python/static-image-export/ for more details

#---------------------------------------PLOTLY PLOTTING FUNCTIONS----------------------------------
'''
Implements default settings for a plot.
    goFig - figure object (plotly)
    title - string of the main title of the graph
    xaxis_label - string of the x-axis label
    yaxis_label - string of the y-axis label
'''
def plotlySetPlot(goFig, title = None, xaxis_label = None, yaxis_label = None):
    goFig.update_layout(paper_bgcolor = 'rgba(0,0,0,0)', plot_bgcolor = 'rgba(0,0,0,0)')
    goFig.update_xaxes(showline = True,
                     linewidth=1,
                     linecolor = 'black',
                     mirror = True,
                     ticks = "outside",
                     tickwidth = 1,
                     tickcolor = 'black',
                     ticklen = 4)

    goFig.update_yaxes(showline = True,
                     linewidth=1,
                     linecolor = 'black',
                     mirror = True,
                     ticks = "outside",
                     tickwidth = 1,
                     tickcolor = 'black',
                     ticklen = 4)
    if title != None:
        goFig.update_layout(title=
                            {
                                'text': title,
                                'x': 0.5,
                                'y': 0.85
                            })
    if xaxis_label != None:
        goFig.update_layout(xaxis_title = xaxis_label)
    if yaxis_label != None:
        goFig.update_layout(yaxis_title = yaxis_label)
