# -*- coding: utf-8 -*-
"""
A file to define fit functions for atomic spectra
Manish Patel
Date Created: 05/07/2019
"""

import numpy as np
from scipy.special import wofz

''' 
Calculates N Gaussian peaks. Pass 3*N+1 p0 parameters. 
'''
def gaussianNincOffset(x, *p):

    r = (len(p)-1) % 3
    n = (len(p)-1) // 3
    if r != 0 :
        # invalid number of parameters entered. There must be
        # 3*n+1 where n is the number of Gaussian peaks.
        g =-1
        print("Incorrect number of parameters: " + str(len(p)))
    else:
        g = p[0]
        for i in range(n):
            g = g + p[1+3*i]*np.exp(-(x-p[2+3*i])**2/(2*p[3+3*i]**2))
    
    return g

'''
    Calculates N Lorentzian peaks. Pass 3*N+1 parameters. 
    
    If i = 0,1,2,...,N to fit N lorentzians:
    p[0] = vertical offset for whole curve
    p[1+3i] = amplitude for ith lorentzian
    p[2+3i] = centre/peak for ith lorentzian
    p[3+3i] = full-width at half-maximum for ith lorentzian
'''
def lorentzianNincOffset(x, *p):

    r = (len(p)-1) % 3
    n = (len(p)-1) // 3
    if r != 0 :
        # invalid number of parameters entered. There must be
        # 3*n+1 where n is the number of Gaussian peaks.
        L =-1
        print("Incorrect number of parameters: " + str(len(p)))
    else:
        L = p[0]
        for i in range(n):
            A = p[1 + 3*i]  # Amplitude
            x0 = p[2 + 3*i] # Centre
            w = p[3 + 3*i] # FWHM
            L = L + A*(0.5*w)**2/((x-x0)**2 + (0.5*w)**2)
    
    return L

'''
    Calculates N Voigt peaks. Pass 4*N+1 parameters.
    If i = 0,1,2,...,N to fit N voigts:
    p[0] = vertical offset for whole curve
    p[1+4i] = amplitude for ith voigt
    p[2+4i] = centre/peak for ith voigt
    p[3+4i] = Gaussian width (sigma) for ith voigt
    p[4+4i] = Lorentzian width (gamma) for ith voigt
'''
def VoigtNIncOffset(x, p):

    r = (len(p)-1) % 4
    n = (len(p)-1) // 4  #number of voigt profiles to fit
    if r != 0 :
        # invalid number of parameters entered. There must be
        # 3*n+1 where n is the number of Gaussian peaks.
        L =-1
        print("Incorrect number of parameters: " + str(len(p)))
    else:
        L = p[0]
        for i in range(n):
            A = p[1 + 4*i]  # Amplitude
            x0 = p[2 + 4*i] # Centre
            sigma = p[3 + 4*i]  # Gaussian width
            gamma = p[4 + 4*i] # Lorentzian width
            L += A*np.real(wofz(((x-x0)+1j*gamma)/(sigma*np.sqrt(2)))) / (sigma*np.sqrt(2*np.pi))
            #Convolution definition: L += (A*gamma*sigma / (np.sqrt(2)*np.power(np.pi, 3/2)))*integrate.quad(lambda t: np.exp(-((t-x0)**2)/(2*sigma**2))/((x-t-x0)**2 + gamm**2), -np.inf, np.inf)
    return L
