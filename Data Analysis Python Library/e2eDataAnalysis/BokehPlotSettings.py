'''
This is a file containing useful functions for e2e data analysis for plotting
and analysing data
Author: Manish Patel
Created on: 12/12/2019
'''

import json
from bokeh.plotting import figure, show
from bokeh.io import output_notebook

#-----------------------------------SETTINGS--------------------------------------
output_notebook()  # Sets the output of bokeh to this notebook

'''
'''
def setScatterPlot(figure, title = None, xaxis_label = None, yaxis_label = None):
    if title != None:
        figure.title.text = title
        figure.title.align = "center"
        figure.title.text_font_size = "25px"
    if xaxis_label != None:
        figure.xaxis.axis_label = xaxis_label
    if yaxis_label != None:
        figure.yaxis.axis_label = yaxis_label
