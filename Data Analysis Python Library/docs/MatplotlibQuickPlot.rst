MatplotlibQuickPlot module
==========================

.. automodule:: MatplotlibQuickPlot
   :members:
   :undoc-members:
   :show-inheritance:
