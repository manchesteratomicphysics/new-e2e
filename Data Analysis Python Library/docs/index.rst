.. e2eDataAnalysis documentation master file, created by
   sphinx-quickstart on Fri May 21 12:34:47 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to e2eDataAnalysis's documentation!
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
  
  e2eDataAnalysis

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
