
/* YourDuino SKETCH TEMPLATE
 Used Opto-Switch OPB 810W55
 terry@yourduino.com */
#include <LiquidCrystal_I2C.h>
#include <Wire.h>
/*-----( Import needed libraries )-----*/
/*-----( Declare Constants )-----*/
/*-----( Declare objects )-----*/
int IR_DETECTOR_PIN2  = 52;
int IR_DETECTOR_PIN3  = 50;
int switchpin = 13;
int sig;
int one=0;
int two=0;
int angle= 0;
int offcheck1=1;
int offcheck2=1;
int messagelength=0;
int anglein=0;
int lcdangle=0;
int lcdangle2=0;
long int time=0;
long int time2=0;
long int time3=0;
int buttonPin=6;
int buttonstate = LOW;
int buttonrelease=0;
int movetime=0;
int movetimestart=0;
int movetimestop=0;
int startcommand=0;
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address



void setup()   /****** SETUP: RUNS ONCE ******/
{
  Serial.begin(9600);
  pinMode(switchpin, OUTPUT);
  lcd.begin(16,2); // initialize the lcd for 16 chars 2 lines, turn on backlight
   lcd.setCursor(0, 0);
   lcd.print("SET ANGLE:");
  lcd.setCursor(0, 1);
   lcd.print("HWP ANGLE:");
   pinMode(buttonPin, INPUT_PULLUP);
}


void loop()   /****** LOOP: RUNS CONSTANTLY ******/
{
  //Read number of 5 degree steps to rotate the half wave plate by from the serial if available and convert to angle
  while(Serial.available()){
 anglein=5*Serial.read();
 startcommand=1;

  }
//If angle read from serial not current angle, start command initiated, and serial angle divisible by 10 degrees start program
 if((anglein !=angle) && (startcommand==1) && (anglein % 10 == 0)){
 movetimestart=millis();  //start the clock

 startcommand=0;
 digitalWrite(switchpin, HIGH);//turn the motor on
}
 //If serial angle divisible by 5
 else if ((anglein !=angle) && (startcommand==1) &&(anglein % 10 != 0) && (anglein % 5 == 0)){
   digitalWrite(switchpin, HIGH);  //turn the motor on
   movetimestart=millis();
   delay(1500);  //This is the time it takes for the motor to move the half-wave plate by 5+/-0.4 degrees
   digitalWrite(switchpin, LOW);  //stop the motor
   movetimestop=millis();  //stop the clock
    movetime=movetimestop-movetimestart;
    Serial.println(movetime);
   startcommand=0;
   angle = anglein;
 }


   if  (digitalRead(IR_DETECTOR_PIN2) == 0)  //Beam is Seen
  {
    //Print 1 on the lcd screen to show that photo-interruptor has been triggered
    //and that a multiple of 10 degrees has been reached
    one=1;
    lcd.setCursor(15, 0);
   lcd.print("1");
  }
  else //print 0 if angle reached not a multiple of 10
  {
  lcd.setCursor(15, 0);
   lcd.print("0");
    offcheck1=0;
    one=0;
  }


    if  (digitalRead(IR_DETECTOR_PIN3) == 0)  //Beam is Seen
  {
    //Print 1 on the lcd screen to show that photo-interruptor has been triggered
    two=1;
    lcd.setCursor(15, 1);
    lcd.print("1");
  }
  else
  {
    lcd.setCursor(15, 1);
    lcd.print("0");
    offcheck2=0;
    two=0;
  }

  if((one==0) && (two==1) && (offcheck1==0)&& (offcheck2==0)) {
    offcheck1=1;
    offcheck2=1;

    angle=angle+5;
    if(anglein==angle){
     digitalWrite(switchpin, LOW);
   }
    movetimestop=millis();
    movetime=movetimestop-movetimestart;
    Serial.println(movetime);

  //Serial.println(angle);
}
  else if((one==1) && (two==1) && (offcheck1==0)) {
    offcheck1=1;
    offcheck2=1;
    angle=0;
     if(anglein==angle){
     digitalWrite(switchpin, LOW);}
    movetimestop=millis();
    movetime=movetimestop-movetimestart;
    Serial.println(movetime);
 // Serial.println(angle);
 }

  one=0;
  two=0;


//Change the angles displayed on the LCD screen
  if (lcdangle!=anglein){
    lcd.setCursor(11, 0);
    lcd.print(" ");
    lcd.setCursor(12, 0);
    lcd.print(" ");
    lcd.setCursor(13, 0);
    lcd.print(" ");

  lcd.setCursor(11, 0);
    lcd.print(anglein);
    // prints 1
  lcdangle=anglein;
}

  if (lcdangle2!=angle){
    lcd.setCursor(11, 1);
    lcd.print(" ");
    lcd.setCursor(12, 1);
    lcd.print(" ");
    lcd.setCursor(13, 1);
    lcd.print(" ");
  lcd.setCursor(11, 1);
    lcd.print(angle);
    //Serial.println(angle);
    // prints 1
  lcdangle2=angle;
}


if (buttonstate == HIGH ){
buttonrelease=1;
}
buttonstate = digitalRead(buttonPin);
time=millis();  //read clock after read button state
while ((buttonstate==LOW) && (time2 <2000) && (buttonrelease==1)){
   time3=millis();  //read clock after button state read to be low
   time2= time3 - time;  //record time difference
   buttonstate = digitalRead(buttonPin);  //read button state again (likely high if button pressed once)
 }

//Press button once to increment set angle by 5 degrees
if ((time2 < 2000) && (time2 > 100)){
anglein += 5;
startcommand=1;

buttonrelease=0;
}

//If button held down then set angle goes to zero
else if (time2 >=2000){
anglein=0;
startcommand=1;
buttonrelease=0;
}

time2=0;

 /*delay(10);*/
}//--(end main loop )---

/*-----( Declare User-written Functions )-----*/


//*********( THE END )***********
