# FaradayCupFrequencyMeasurement

FaradayCupFrequencyMeasurement.ino
Sketch to read electrometer frequency to nearest 1 Hz and report it to Serial when a request is received from computer.
Electrometer gives out 0 -> 10 kHz signal which corresponds to full scale as set by front panel rotary switch.
Arduino just monitors the frequency output on an interrupt pin and reports the latest reading when a request is made by PC.
 
TTL frequency signal must be plugged in to pin 2 (INT0) on ATmega328P.
 
In order for Get ID command to work in time for auto connection, add 1uF capacitor between RESET and GND.
 
Serial is at 250 kbps
#### Protocol:
ID\n = Return ID which is FC for faraday cup
GF\n = Return latest frequency reading if one is available
UPx\n = Continuous updates are sent at 1 Hz if x = 1. No updates if x = 0.

LabVIEW:
Use FGV_FaradayCupFreqHandler.vi inside NewE2E_FaradayCup.llb which has serial code dependencies in SerialLibrary.llb
 
Version 1: Initial version.
