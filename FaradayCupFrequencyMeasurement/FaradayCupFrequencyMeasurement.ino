// FaradayCupFrequencyMeasurement.ino
// Sketch to read electrometer frequency to nearest 1 Hz and report it to Serial when a request is received from computer.
// Electrometer gives out 0 -> 10 kHz signal which corresponds to full scale as set by front panel rotary switch.
// Arduino just monitors the frequency output on an interrupt pin and reports the latest reading when a request is made by PC.
// 
// TTL frequency signal must be plugged in to pin 2 (INT0) on ATmega328P.
// 
// In order for Get ID command to work in time for auto connection, add 1uF capacitor between RESET and GND.
// 
// Serial is at 250 kbps
// Protocol:
// ID\n = Return ID which is FC for faraday cup
// GF\n = Return latest frequency reading if one is available
// UPx\n = Continuous updates are sent at 1 Hz if x = 1. No updates if x = 0.
// 
// Version 1: Initial version.

#include "SerialChecker.h"
#include "MilliTimer.h"

MilliTimer update(100);
volatile MilliTimer readinAge(1000);


SerialChecker sc;

const uint8_t freqPin = 2;

volatile uint32_t dt;
volatile bool newReading = false;

bool continuousUpdates = false;

void setup(){
    sc.init();
    sc.enableACKNAK();
    // Serial.println("Connected to ArduinoFrequencyMeasurement.ino");

    pinMode(freqPin, INPUT);
    attachInterrupt(digitalPinToInterrupt(freqPin), stopWatch, FALLING);
}

void loop(){
    checkSerial();
    if(continuousUpdates && update.timedOutAndReset()){
        getReading();
        
    }
}

void checkSerial(){
    if(sc.check()){
        if(sc.contains("GF")){
            // return latest current reading
            getReading();
        }
        else if(sc.contains("UP1")){
            continuousUpdates = true;
            sc.sendACK();
        }
        else if(sc.contains("UP0")){
            continuousUpdates = false;
            sc.sendACK();
        }
        else if(sc.contains("ID")){
            Serial.println("FC"); // for faraday cup current
        }
        else if(sc.contains("$ID")){ // legacy for compatibility with Ahmad's protocol.
            Serial.println("FC"); // for faraday cup current
        }
        else{
            // message not understood
            sc.sendNAK();
        }
    }
}

void getReading(){
    // Serial prints a reading if there is one available and it isn't too old, otherwise serial prints 0.
    if(newReading && !readinAge.timedOutAndReset()){
        newReading = false;
        Serial.println(1000000 / dt);
    }
    else{
        Serial.println(0);
    }
}

void stopWatch(){
    volatile static uint32_t tOld = 0;// tOld = t;
    uint32_t t = micros();
    dt = t - tOld;
    tOld = t;
    newReading = true;
    readinAge.reset();
}