# Analyser Angle Control

The electron energy analyser angles for the new (e,2e) spectrometer are controlled through talking serial commands to an arduino which controls a stepper motor coupled to a turntable which the analyser sits on. Each analyser also has a potentiometer coupled to the turntable and motor to sense the angle. Around each turntable there are crenelations every 5 degrees. These crenelations block opto-interrupts positioned around the vacuum chamber which provide a calibration check on the potentiomter. There are two electron energy analysers in the vacuum chamber.  

## Serial Commands

The serial commands available are:
* "1xx" or "2xx" - choose analyser 1 or 2 to command
* "xSS" - stop the analyser
* "xSPxxx" - set the angle of the analyser to move to. Angles are input as multiplied by 10 so if you want to move an analyser 1 to 90 degrees input "1SP900"
* "xSH" - set the home position. Move to the home position of the analyser. The home flag in the chamber is close to 45 degrees.
* "xSZxxx" - set the number of steps for the stepper motor to move
* "xSAxxx" - set the maximum acceleration of the steppers in steps/s^2
* "xSaxxx" - set the minimum acceleration of the steppers in steps/s^2
* "xSRxxx" - set the maximum speed of the steppers in steps/s
* "xSrxxx" - set the minimum speed of the steppers in steps/s
* "xSJxxx" - set the jerk of the steppers in steps/s^3
* "xSExxx" - set the maximum angle the analyser can move to e.g. if you want analyser 1's maximum angle to be 120 deg input "1SE1200"
* "xSFxxx" - set the minimum angle the analyser can move to
* "xShxxx" - set the initial home angle (angle where the home flag is). This does not need to be very exact. When homing the controller will move in the direction of the home angle but only stop when the home flag blocks an opto-interrupt. The controller saves this angle as the new home angle. 
* "xSC" - set calibration mode. See section below for more information on how to calibrate.
* "xGV" - reads the potentiometer to get the current angle and prints the potentiometer reading to the serial
* "xGA" - reads the potentiometer and prints the current angle
* "xGP" - prints the set angle
* "xGS" - prints the moving status of the analyser
* "xGN" - prints the number of steps set to move (approximation)
* "xGR" - gets the current rate the analyser is stepping at in steps/s
* "xGH" - prints the home status of the analyser
* "xGO" - print the crenelation opto-interrupt state for the analyser
* "ID" or "$ID" - prints "SC" for "Stepper Controller". Used to identify the device when connecting
* "SC1" - enable a checksum character added to the end of each serial message. Uses the Spellman MPS checksum method to generate the checksum character.
* "SC0" - disable the checksum. Note, that this would be 

## How to calibrate

1. Set the stepper controller in calibration mode 
2. Home the analysere 
3. Move to the maximum angle (this is moving in the +ve direction)
4. As a crenelation is hit a number indicating the pot voltage corresponding to that crenelation angle is printed to the serial.
5. Record these voltage readings from the potentiomenter (pot voltages) and their corresponding angles
6. Go to the minimum analyser angle recording the pot voltage and corresponding angle
7. Average the pot voltages for each angle
8. Perform a linear fit on this data and edit the DegsPerVolt and ConstAngle at the beginning of AnalyserAngleDetector.ino using the gradient and intercept of your linear fit