/*
Firmware for the Arduino which controls the Analyser Angle Detector.
Author: Manish Patel
Date created: 21/08/2020
*/

#include "Stepper.hpp"
#include "SerialChecker.h"

//--------------------PINS--------------------------
uint8_t opto_pin_an2{3};
uint8_t home_pin_an2{5};
uint8_t dir_pin_an2{8};
uint8_t drive_pin_an2{10};
uint8_t pot_pin_an2{16};

uint8_t opto_pin_an1{2};
uint8_t home_pin_an1{4};
uint8_t dir_pin_an1{7};
uint8_t drive_pin_an1{9};
uint8_t pot_pin_an1{15};

uint8_t interrupt_relay_pin{6};

//---------------------------- STEPPER SETTINGS -----------------------------
uint32_t delay_step_time{1e3};  // width of LOW in step time is 1000 us
uint32_t default_ramp_time{1e5};  // change velocity and acceleration over this timescale (us).
uint32_t default_debounce_time{3e6};  // debounce time for crenelations (us)
float default_min_rate{75};  // units in steps s^-1
float default_max_rate{75};  // units in steps s^-1
float default_min_accel{10};  // units in steps s^-2
float default_max_accel{20};  // units in steps s^-2
float default_jerk{2};  // units in steps s^-3

Stepper Analyser1(drive_pin_an1, dir_pin_an1, home_pin_an1, opto_pin_an1, pot_pin_an1);
Stepper Analyser2(drive_pin_an2, dir_pin_an2, home_pin_an2, opto_pin_an2, pot_pin_an2);
Stepper *stepperP;  //pointer to stepper class

//----------------------SERIAL SETTINGS----------------------
SerialChecker sc;

//--------------------------TIMERS-----------------------------
MicroTimer debug_timer; // Print debug text every second

void setup()
{
  sc.init();  //Initialise serial

  // Set default max rates
  Analyser1.setMinRate(default_min_rate);
  Analyser1.setMaxRate(default_max_rate);
  Analyser1.setMinAccel(default_min_accel);
  Analyser1.setMaxAccel(default_max_accel);
  Analyser1.setJerk(default_jerk);

  Analyser2.setMinRate(default_min_rate);
  Analyser2.setMaxRate(default_max_rate);
  Analyser2.setMinAccel(default_min_accel);
  Analyser2.setMaxAccel(default_max_accel);
  Analyser2.setJerk(default_jerk);
  
  // Turn on interrupts. Pin has to be HIGH for the opto-interrupters to be active
  pinMode(interrupt_relay_pin, OUTPUT);
  digitalWrite(interrupt_relay_pin, HIGH);

  // Initialise timers
  Analyser1.initDelayTimer(delay_step_time);
  Analyser1.initRampTimer(default_ramp_time);
  Analyser1.initDebounceTimer(default_debounce_time);

  Analyser2.initDelayTimer(delay_step_time);
  Analyser2.initRampTimer(default_ramp_time);
  Analyser2.initDebounceTimer(default_debounce_time);
  
  debug_timer.init(3e6);

   // Set calibration
  Analyser1.setStepsPerVolt(51.2);  
  Analyser1.setDegsPerVolt(-3.65);
  Analyser1.setConstAngle(2772);

  Analyser2.setStepsPerVolt(51.2);  // Degrees in format 45.0 = 450
  Analyser2.setDegsPerVolt(6.49);
  Analyser2.setConstAngle(-2271);

  // Set max, min, and home angles
  Analyser1.setMaxAngle(1300);
  Analyser1.setMinAngle(200);
  Analyser1.setHomeAngle(450);

  Analyser2.setMaxAngle(460);
  Analyser2.setMinAngle(250);
  Analyser2.setHomeAngle(450);

  // Initailise the pins and angles
  Analyser1.init();
  Analyser2.init();
}


void loop()
{
  checkSerial();
  Analyser1.run();
  Analyser2.run();

}

void checkSerial()
{
  // If sc.check() returns false then no message recieved
  if(sc.check())
  {
     if(sc.contains("1"))
     {
      stepperP = &Analyser1;
     }
     else if(sc.contains("2"))
     {
      stepperP = &Analyser2;
     }

     if(sc.contains("SS", 1))
     {
      stepperP->stop();
      stepperP->setSetAngle(stepperP->getCurrentAngle());
      sc.sendACK();
     }
     else if(sc.contains("SP", 1))
     {
      uint16_t tmp = sc.toInt16(3)*10;
      if(tmp >= stepperP->getMinAngle() && tmp <= stepperP->getMaxAngle())
      {
        stepperP->setSetAngle(tmp);
        sc.sendACK();
        State tmpState = MOVING;
        stepperP->setState(tmpState);
      }
      else
      {
        sc.sendNAK();
        Serial.println("Angle out of bounds!");
      }
     }
     else if(sc.contains("SH", 1))
     {
      sc.sendACK();
      State tempState = HOMING;
      stepperP->setState(tempState);
     }
     else if(sc.contains("SZ", 1))
     {
      float tmp = float(sc.toInt32(3));
      stepperP->setStepsToMove(tmp);
      sc.sendACK();
     }
     else if(sc.contains("SA", 1))
     {
      float tmp = float(sc.toInt32(3));
      stepperP->setMaxAccel(tmp);
      sc.sendACK();
     }
     else if(sc.contains("Sa", 1))
     {
      float tmp = float(sc.toInt32(3));
      stepperP->setMinAccel(tmp);
      sc.sendACK();
     }
     else if(sc.contains("SR", 1))
     {
      float tmp = float(sc.toInt32(3));
      stepperP->setMaxRate(tmp);
      sc.sendACK();
     }
     else if(sc.contains("Sr", 1))
     {
      float tmp = float(sc.toInt32(3));
      stepperP->setMinRate(tmp);
      sc.sendACK();
     }
     else if(sc.contains("SJ", 1))
     {
      float tmp = float(sc.toInt32(3));
      stepperP->setJerk(tmp);
      sc.sendACK();
     }
     else if(sc.contains("SE", 1))
     {
      uint16_t tmp = sc.toInt16(3)*10;
      stepperP->setMaxAngle(tmp);
      sc.sendACK();
     }
     else if(sc.contains("SF", 1))
     {
      uint16_t tmp = sc.toInt16(3)*10;
      stepperP->setMinAngle(tmp);
      sc.sendACK();
     }
     else if(sc.contains("Sh", 1))
     {
      uint16_t tmp = sc.toInt16(3)*10;
      if(tmp >= stepperP->getMinAngle() && tmp <= stepperP->getMaxAngle())
      {
        stepperP->setHomeAngle(tmp);
        sc.sendACK();
      }
      else
      {
        sc.sendNAK();
        Serial.println("Angle out of bounds!");
      }
     }
     else if(sc.contains("SC", 1))
     {
      stepperP->setCalibrationMode(!stepperP->getCalibrationMode());
      sc.sendACK();
     }
     else if(sc.contains("GV", 1))
     {
      stepperP->readPotVoltage();
      Serial.println(stepperP->getPotVoltage());
     }
     else if(sc.contains("GA", 1))
     {
      stepperP->readPotVoltage();
      Serial.println(float(stepperP->getCurrentAngle())/10);
     }
     else if(sc.contains("GP", 1))
     {
      Serial.println(float(stepperP->getSetAngle())/10);
     }
     else if(sc.contains("GS", 1))
     {
      Serial.println(stepperP->getMovingStatus());
     }
     else if(sc.contains("GN", 1))
     {
      Serial.println(stepperP->getStepsToMove());
     }
     else if(sc.contains("GR", 1))
     {
      Serial.println(stepperP->getCurrentRate());
     }
     else if(sc.contains("GH", 1))
     {
      Serial.println(stepperP->readHome());
     }
     else if(sc.contains("GO", 1)) // Prints 1 if opto blocked and 0 if not
     {
      Serial.println(!(stepperP->readCrenOpto()));
     }
     else if(sc.contains("ID") || sc.contains("$ID"))
     {
      Serial.println("SC");
     }
     else if(sc.contains("SC1"))
     {
      sc.enableChecksum();  //Enables Readable8bitChars checksum
      sc.sendACK();
     }
     else if(sc.contains("SC0"))
     {
      sc.disableChecksum();
      sc.sendACK();
     }
     else 
     {
      sc.sendNAK();
     }
  }
}

// Prints debugging information from the stepper everytime timer times out.
void debug()
{
  if(debug_timer.timedOut(true))
  {
  	// Put information you want to print here
  }
}