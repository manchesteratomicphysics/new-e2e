/*
Function definitions for MicroTimer class.
From https://github.com/MatthewAHarvey/OldE2EStepperController/
Go into Arduino folder in link above.
*/

#include "Arduino.h"
#include "MicroTimer.hpp"

MicroTimer::MicroTimer(){
  timeOut=1000000;
  start=micros();
  current=start;
}

MicroTimer::MicroTimer(uint32_t t){
  timeOut=t;
  start=micros();
  current=start;
}

void MicroTimer::init(uint32_t t){
  timeOut=t;
  start=micros();
  current=start;
}

uint32_t MicroTimer::elapsed(){
  current=micros();
  return current-start;
}

bool MicroTimer::timedOut(){
  if(elapsed() >= timeOut){
    return true;
  }
  else{
    return false;
  }
}

bool MicroTimer::timedOut(bool RESET){
  if(elapsed() >= timeOut){
    if(RESET) reset();
    return true;
  }
	else{
    return false;
  }
}

void MicroTimer::updateTimeOut(uint32_t t){
  timeOut=t;
}

void MicroTimer::reset(){
  start=micros();
  current=start;
}

uint32_t MicroTimer::getTimeOut(){
  return timeOut;
}
