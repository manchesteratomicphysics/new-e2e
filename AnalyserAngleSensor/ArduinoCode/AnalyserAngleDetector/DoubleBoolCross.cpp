/*
This is a file to define the double bool crossing class functions.
Author: Manish Patel
Date Created: 25/08/2020
*/

#include "DoubleBoolCross.hpp"

// Parameterised constructor
DoubleBoolCross::DoubleBoolCross(String cross_type, bool current_bool)
{
  _state = false;
  _cross_type = cross_type;
  // Set the single bool crossing types
  if(cross_type == "TFT")
  {
    _first_bool_cross.setCrossType("TF");
    _second_bool_cross.setCrossType("FT");
  }
  else if(cross_type == "FTF")
  {
    _first_bool_cross.setCrossType("FT");
    _second_bool_cross.setCrossType("TF");
  }
  //Input the first bool
  _first_bool_cross.updateState(current_bool);
}

// Initialisation if default constructor used
void DoubleBoolCross::init(String cross_type, bool current_bool)
{
  _cross_type = cross_type;
  // Set the single bool crossing types
  if(cross_type == "TFT")
  {
    _first_bool_cross.setCrossType("TF");
    _second_bool_cross.setCrossType("FT");
  }
  else if(cross_type == "FTF")
  {
    _first_bool_cross.setCrossType("FT");
    _second_bool_cross.setCrossType("TF");
  }
  //Input the first bool
  _first_bool_cross.updateState(current_bool);
}

 // Sets the current bool and first bool crossing
void DoubleBoolCross::updateState(bool current_bool)
{
  // Check if first bool crossing condition met
  if(!(_first_bool_cross.getState()))
  {
    _first_bool_cross.updateState(current_bool);
  }

  // If first bool crossed then check second bool
  if(_first_bool_cross.getState())
  {
    _second_bool_cross.updateState(current_bool);
    // Both conditions met then state is true and reset first bool cross
    if(_second_bool_cross.getState())
    {
      _state = true;
      _first_bool_cross.updateState(current_bool);
    }
    else
    {
      _state = false;
    }
  }
  else
  {
    _state = false;
  }
}

// Updates the double bool crossing with a new bool and gets the state
bool DoubleBoolCross::updateAndGetState(bool current_bool)
{
  // Check if first bool crossing condition met
  if(!(_first_bool_cross.getState()))
  {
    _first_bool_cross.updateState(current_bool);
  }

  // If first bool crossed then check second bool
  if(_first_bool_cross.getState())
  {
    _second_bool_cross.updateState(current_bool);
    // Both conditions met then state is true and reset first bool cross
    if(_second_bool_cross.getState())
    {
      _state = true;
      _first_bool_cross.updateState(current_bool);
      return _state;
    }
    else
    {
      _state = false;
    }
  }
  else
  {
    _state = false;
  }

  return _state;
}

// Gets the current state of the double bool crossing
bool DoubleBoolCross::getState()
{
  Serial.print(" 1st: ");
  Serial.print(_first_bool_cross.getState());
  Serial.print(" 2nd: ");
  Serial.print(_second_bool_cross.getState());
  Serial.print(" Double: ");
  return _state;
}

// Cross type is either "TFT" or "FTF"
void DoubleBoolCross::setCrossType(String cross_type)
{
  _cross_type = cross_type;

  if(cross_type == "TFT")
  {
    _first_bool_cross.setCrossType("TF");
    _second_bool_cross.setCrossType("FT");
  }
  else if(cross_type == "FTF")
  {
    _first_bool_cross.setCrossType("FT");
    _second_bool_cross.setCrossType("TF");
  }
}

 // Gets the cross type
String DoubleBoolCross::getCrossType()
{
  return _cross_type;
}
