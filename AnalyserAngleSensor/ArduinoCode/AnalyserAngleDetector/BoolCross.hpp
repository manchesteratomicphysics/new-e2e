/*
This is a file to declare the bool crossing class
Author: Manish Patel
Date Created: 24/08/2020
*/

#ifndef BOOLCROSS_HPP
#define BOOLCROSS_HPP

#include "Arduino.h"

/*
Class to find a type of bool crossing e.g. True->False
*/
class BoolCross
{
private:
  bool _current_bool;  // The current bool that the class compares to the previous bool
  bool _prev_bool;  // The previous bool that the class compares to the current bool
  bool _state;  // True if cross type condition is met
  String _cross_type;  // Is "TF" and "FT" for True->False and False->True respectively
public:
  BoolCross(){_state = false;}  // Default constructor
  BoolCross(String cross_type, bool current_bool);  // Parameterised constructor
  void init(String cross_type, bool current_bool);  // Initialisation if default constructor used

  // Getter and setter functions
  bool updateAndGetState(bool current_bool);  // Updates the bool crossing with a new bool and gets the state
  bool getState();  // Gets the current state of the bool crossing
  String getCrossType();  // Gets the cross type
  void setCrossType(String cross_type);  // Cross type is either "TF" or "FT"
  void updateState(bool current_bool);  // Sets the current bool and previous bool
};

#endif
