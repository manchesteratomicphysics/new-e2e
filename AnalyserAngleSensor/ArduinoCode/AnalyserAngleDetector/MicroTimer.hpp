/*
MicroTimer class declaration.
From https://github.com/MatthewAHarvey/OldE2EStepperController/
Go into ArduinoCode folder in the link above.
*/

#ifndef MICROTIMER_HPP
#define MICROTIMER_HPP

/*
This class sets up a tidy timer which allows the caller to check how much time has
elapsed since it was last polled and report whether a minimum interval has been
passed. This allows events to be setup to occur at minimum set intervals within
a block of code.
*/
class MicroTimer
{
    volatile uint32_t start;
    volatile uint32_t current;
    volatile uint32_t timeOut;
    //volatile bool autoReset;
  public:
    MicroTimer();
    MicroTimer(uint32_t t);
    void init(uint32_t t); //initialisation function to be used if the timer is created with the null constructor.
    uint32_t elapsed();// return the elapsed time.
    bool timedOut();// function to check whether the timer has timed out. If it has, reports true and resets the timer
    bool timedOut(bool RESET);
    void updateTimeOut(uint32_t t); //update the timeout variable without restarting the current time
    uint32_t getTimeOut();
    void reset(); // reset the timer.
};

//MicroTimer::MicroTimer(){ //default null constructor that initialises to default 100us interval
//  timeOut=100;
//  start=0;
//  current=start;
//  autoReset=true;
//}

#endif
