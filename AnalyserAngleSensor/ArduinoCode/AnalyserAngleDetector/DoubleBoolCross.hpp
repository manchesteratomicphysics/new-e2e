/*
File to declare the Double Bool Cross class. This class changes its state
depending on the boolean values input to the class and the crossing type set.
Author: Manish Patel
Date created: 25/08/2020
*/

#ifndef DOUBLEBOOLCROSS_HPP
#define DOUBLEBOOLCROSS_HPP

#include "BoolCross.hpp"

/*
Class to find a type of double bool crossing e.g. True->False->True
*/
class DoubleBoolCross
{
private:
  BoolCross _first_bool_cross;  // Holds the first part of the double bool cross
  BoolCross _second_bool_cross;  // Holds the second part of the double bool cross
  bool _state;  // Whether the cross type condition is met
  String _cross_type;  //"TFT" or "FTF" for True->False->True or False->True->False respectively
public:
  DoubleBoolCross(){_state = false;}  // Default constructor
  DoubleBoolCross(String cross_type, bool current_bool);  // Parameterised constructor
  void init(String cross_type, bool current_bool);  // Initialisation

  // Getter and setter functions
  bool updateAndGetState(bool current_bool);  // Updates the double bool crossing with a new bool and gets the state
  bool getState();  // Gets the current state of the double bool crossing
  String getCrossType();  // Gets the cross type
  void setCrossType(String cross_type);  // Cross type is either "TFT" or "FTF"
  void updateState(bool current_bool);  // Sets the current bool and first bool crossing
};

#endif
