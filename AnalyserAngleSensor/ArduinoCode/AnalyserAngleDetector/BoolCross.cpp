/*
This is a file to define the bool crossing class functions
Author: Manish Patel
Date Created: 24/08/2020
*/

#include "BoolCross.hpp"

//-------------------------Bool Cross Class-----------------------------------
// Parameterised constructor
BoolCross::BoolCross(String cross_type, bool current_bool)
{
  _cross_type = cross_type;
  _current_bool = current_bool;
  _state = false;
}

// Initialisation if default constructor used
void BoolCross::init(String cross_type, bool current_bool)
{
  _cross_type = cross_type;
  _current_bool = current_bool;
  _state = false;
}

// Sets the current bool and previous bool
void BoolCross::updateState(bool current_bool)
{
  _prev_bool = _current_bool;
  _current_bool = current_bool;

  // If cross type matches bool crossing then return true
  if(_cross_type == "TF" && _prev_bool && !(_current_bool)){_state = true;}
  else if(_cross_type == "FT" && !(_prev_bool) && _current_bool){_state = true;}
  else{_state = false;}
}

 // Updates the bool crossing with a new bool and gets the state
bool BoolCross::updateAndGetState(bool current_bool)
{
  // Update bools
  _prev_bool = _current_bool;
  _current_bool = current_bool;

  // If cross type matches bool crossing then return true
  if(_cross_type == "TF" && _prev_bool && !(_current_bool)){_state = true;}
  else if(_cross_type == "FT" && !(_prev_bool) && _current_bool){_state = true;}
  else{_state = false;}
  return _state;
}

// Gets the current state of the bool crossing
bool BoolCross::getState()
{
  return _state;
}

// Gets the cross type
String BoolCross::getCrossType()
{
  return _cross_type;
}

void BoolCross::setCrossType(String cross_type)
{
  _cross_type = cross_type;
}
