/*
This file contains class functions for the Stepper class to move the electron
energy analysers for the new e,2e spectrometer.
Author: Manish Patel
Date created: 19/08/2020
*/

#include "Stepper.hpp"

//-------------------------Initialiser---------------------------------

// Sets the pin modes for the stepper
void Stepper::init()
{
  // Initialise pins
  pinMode(_drive_pin, OUTPUT);
  pinMode(_dir_pin, OUTPUT);
  pinMode(_cren_opto_pin, INPUT);
  pinMode(_home_pin, INPUT);
  pinMode(_pot_pin, INPUT);

  // Initialise states
  _moving = false;
  _steps_to_move = 0;
  _state = IDLE;
  _calibration_step = 0;
  _calibration_mode = false;

  //Initialise angles
  readPotVoltage();
  _set_angle = _current_angle;
}

//---------------------------Setter Functions----------------------------
  void Stepper::setMaxAngle(uint16_t max_angle)
  {
    _max_angle = max_angle;
    _max_pot_voltage = uint16_t((max_angle - _const_angle)/_degs_per_volt);
  }

  void Stepper::setMinAngle(uint16_t min_angle)
  {
    _min_angle = min_angle;
    _min_pot_voltage = uint16_t((min_angle - _const_angle)/_degs_per_volt);
  }

  void Stepper::setCurrentAngle(uint16_t current_angle)
  {
    _current_angle = current_angle;
    _pot_voltage = uint16_t((current_angle - _const_angle)/_degs_per_volt);
  }

  void Stepper::setSetAngle(uint16_t set_angle)
  {
    if(_set_angle >= _min_angle && _set_angle <= _max_angle)
    {
      _set_angle = set_angle;
      _set_pot_voltage = uint16_t((set_angle - _const_angle)/_degs_per_volt);
    }
    else
    {
      Serial.println("Input an angle within bounds!");
    }

  }

  void Stepper::setHomeAngle(uint16_t home_angle)
  {
    _home_angle = home_angle;
    _home_pot_voltage = uint16_t((home_angle - _const_angle)/_degs_per_volt);
  }
//----------------------------Helper Functions------------------------------

// Reads the voltage on the voltage pot and increments the current angle
void Stepper::readPotVoltage()
{
  _pot_voltage = analogRead(_pot_pin);
  _current_angle = uint16_t(_pot_voltage*_degs_per_volt + _const_angle);  // calibration curve
}

/*
Status of the crenelations. Opto is only on one edge so a True->False->True
of the opto state gives the position of 5 degrees if going in the +ve _direction
and False->True->False gives 5 degrees in the -ve direction.
*/
bool Stepper::readCrenelations()
{
  _opto_flag = digitalRead(_cren_opto_pin);  // Goes LOW when blocked
  return _cren_flag.updateAndGetState(_opto_flag);
}

bool Stepper::readHome()
{
  _home_flag = !(digitalRead(_home_pin));  // Goes LOW when home opto is blocked
  return _home_flag;
}

// Coerces rates and acceleration to their min/max values if out of range
void Stepper::coerceRateAndAccel()
{
 if(_current_rate < _min_rate)
  {
    _current_rate = _min_rate;
  }
  else if(_current_rate > _max_rate)
  {
   _current_rate = _max_rate;
  }

  if(_current_accel < _min_accel)
  {
     _current_accel = _min_accel;
  }
  else if(_current_accel > _max_accel)
  {
    _current_accel = _max_accel;
  }
}

/*
Converts rates, acceleration, and jerk to the timescale of the ramp timer.
Allows the rates, acceleration, and jerk to be kept in units of steps s^-1,
steps s^-2, steps s^-3 respectively so the only scaling necessary is from the
ramp timer.
*/
float Stepper::toRampTimescale(float v)
{
  //unsigned long t{_ramp_timer.getTimeOut()};
  //Serial.println(t);
  //Serial.println(t/uint32_t(1e6));
  return v*(_ramp_timer.getTimeOut()/float(1e6));  // Divide by 1e6 to get timer in seconds
}

// Calculates the velocity of the stepper with the ramp settings
void Stepper::calculateRate()
{
  if(_ramp_timer.timedOut(true))
  {
    if(_steps_to_move > 0)
    {
      /*Serial.print("Steps to move: ");
      Serial.println(_steps_to_move);*/
      // If steps to move > half way point then accelerating
      if(_steps_to_move > _half_way_point)
      {
        // Increment rate and acceleration if less than max
        if(_current_rate < _max_rate)
        {
          float v0{toRampTimescale(_current_accel)};
          //Serial.println(v0);
          _current_rate += v0; // v = v0 + a*t, t = 1 s
          //Serial.println(_current_rate);
        }
        if(_current_accel < _max_accel)
        {
          // a = a0 + j*t, t = 1 s
          // Serial.print("Increasing Accel by: ");
          // Serial.println(toRampTimescale(_jerk));
          _current_accel += toRampTimescale(_jerk);
        }
      }
      // If steps to move < half way point then decellerating
      else if(_steps_to_move <= _half_way_point)
      {
        // Decrement rate and acceleration if larger than min
        if(_current_rate > _min_rate)
          {
            _current_rate -= toRampTimescale(_current_accel);
          }
        if(_current_accel > _min_accel)
          {
            //Serial.print("Decreasing Accel by: ");
           //Serial.println(toRampTimescale(_jerk));
            _current_accel -= toRampTimescale(_jerk);
          }
      }
      // Coerce the values to max/min if above/below
      coerceRateAndAccel();
      // Calculate the step timer value for the current rate
      // step time = 1 s/rate, multiply by 1e6 as we want result in microseconds
      _step_timer.updateTimeOut((1e6/(_current_rate)));
    }
  }
}

// Stops the stepper moving
void Stepper::stop()
{
  _moving = false;
  _steps_to_move = 0;
  _current_rate = 0;  // Set rate and acceleration to zero
  _current_accel = 0;
  _state = IDLE;
}

// Calculates the number of steps to move to the input angle
void Stepper::calculateStepsToMove(uint16_t voltage)
{
  // Steps = |delta(angle)|*steps per angle
    _steps_to_move = uint16_t(abs(voltage - _pot_voltage)*_steps_per_volt);
    _half_way_point = _steps_to_move/2;  // Rounds down to nearest integer by default
}

// Writes the direction pin and crenelation logic to move to the input angle
void Stepper::writeDirection(uint16_t voltage)
{
  // Write direction, +ve direction = 1, -ve direction = 0
    if(voltage > _pot_voltage)  // +ve direction
    {
      _moving = true;
      _direction = true;
      digitalWrite(_dir_pin, LOW);  // need to drive pin opposite to direction
      _cren_flag.init("FTF", _opto_flag);  //F->T->F is 5 deg flag in +ve dir
    }
    else if(voltage < _pot_voltage)  // -ve direction
    {
      _moving = true;
      _direction = false;
      _cren_flag.init("TFT", _opto_flag);  //T->F->T is 5 deg flag in -ve dir
      digitalWrite(_dir_pin, HIGH);
    }
}

// Checks the crenelations for the left edge
bool Stepper::checkCrenelations()
{
  if(_direction == true) // +ve direction
  {
    if(readCrenOpto() == true)  // opto goes HIGH when no longer blocked
    {
      return true;
    }
  }
  else // -ve direction
  {
    if(readCrenOpto() == false)  // opto goes LOW when blocked
    {
      return true;
    }
  }
}

//-----------------------------------State functions----------------------------
// Moves to a set angle
void Stepper::moveToSetAngle()
{
  // At start of move calculate number of steps to move, direction, & half way point
  if(!_moving)
  {
    calculateStepsToMove(_set_pot_voltage);
    writeDirection(_set_pot_voltage);
    _debounce_timer.reset();
  }
  else  // If there are steps to move
  {
    step();  // step one step
    calculateRate();  // calculate rate for next step
    readPotVoltage(); // read the pot voltage to get angle

    if(_calibration_mode == true)  // Print voltages when crenelation logic fulfilled
    {
      if(readCrenelations() && _debounce_timer.timedOut())
      {
        _debounce_timer.reset();
        Serial.print("Crenelation found! Pot Voltage: ");
        Serial.println(_pot_voltage);
      }
    }

    // If reached set angle then stop moving
    if(_pot_voltage == _set_pot_voltage)
    {
      stop();
      Serial.println("Reached set angle.");
      _set_angle = _current_angle;
    }
  }
}

// Moves the analyser to the home angle
void Stepper::home()
{
  // If not at home flag then move
  if(!readHome())
  {
    if(!_moving) // Setup the move
    {
      calculateStepsToMove(_home_pot_voltage);
      writeDirection(_home_pot_voltage);
    }
    else  // Move
    {
      step();
      calculateRate();
      readPotVoltage();
    }
  }
  else
  {
    stop();
    Serial.println("Found home.");
    _set_pot_voltage = _pot_voltage;
    _set_angle = _current_angle;
    _home_angle = _current_angle;  // Update home angle
    _home_pot_voltage = _pot_voltage;
  }
}

//---------------------------State Machine------------------------
// Run the state machine for an analyser
void Stepper::run()
{
  switch(_state)
  {
    case IDLE:
    {
      //debug();
      break;
    }

    case MOVING:
    {
      moveToSetAngle();
      if(!_moving)
      {
        _state = IDLE;
      }
      break;
    }

    case HOMING:
    {
      home();
      if(!_moving)
      {
        _state = IDLE;
      }
      break;
    }
  }
}
