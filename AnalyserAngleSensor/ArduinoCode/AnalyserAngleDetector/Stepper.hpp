/*
Header file to encapsulate the Stepper class for a stepper motor.
Author: Manish Patel
Date created: 1get8/08/2020
*/

#ifndef STEPPER_HPP
#define STEPPER_HPP

#include "Arduino.h"
#include "MicroTimer.hpp"
#include "DoubleBoolCross.hpp"

//----------------------------------STATES----------------------------------
enum State{IDLE, MOVING, HOMING};

/*
Class for the new e,2e stepper motor.
*/
class Stepper
{
private:
  // Pins
  uint8_t _home_pin;  // pin number for the 45 degree (home) flag
  uint8_t _drive_pin;  // pin number to write to step the stepper motor
  uint8_t _cren_opto_pin;  // pin number for the opto-interrupter reading the crenelations (every 5 degrees)
  uint8_t _dir_pin;  // pin number to write the direction of the motor
  uint8_t _pot_pin;  // pin number for the potentiometer reading

  // Stepper settings
  uint16_t _max_angle;  // maximum angle stepper can rotate analysers to
  uint16_t _max_pot_voltage;  // maximum pot voltage can rotate to
  uint16_t _min_angle;  // minimum angle stepper can rotate analysers to
  uint16_t _min_pot_voltage;  // minimum pot voltage
  uint16_t _current_angle;  // current angle analyser is currently at
  uint16_t _set_angle;  // the angle which analysers is set to move to
  uint16_t _set_pot_voltage;  // the voltage which the analyser is set to move to
  uint16_t _home_angle;  // angle of the centre of the home flag
  uint16_t _home_pot_voltage;  // pot voltage at the centre of the home flag
  uint16_t _steps_to_move; // number of steps for stepper to move
  uint16_t _pot_voltage;  // digital reading of potentiometer voltage 0 to 1023
  bool _direction;  // direction of stepper: 1 is +ve (increasing analyser angle), 0 is -ve (decreasing analyser angle)
  
  // Timing settings
  MicroTimer _delay_timer;  // timer for the width of the LOW portion of the step.
  MicroTimer _step_timer;  // timer for the width of the total step.
  MicroTimer _ramp_timer;  // timer to calculate the velocity and acceleration 
  MicroTimer _debounce_timer;  // timer for the debounce of the crenelations

  // Ramp variables
  uint16_t _half_way_point;  // number of steps to the half way point of the move
  float _max_rate;  // maximum no. of steps s^-1
  float _min_rate;  // minimum no. of steps s^-1
  float _current_rate;  // current no. of steps s^-1
  float _max_accel;  // max no. of steps s^-2
  float _min_accel;  // max no. of steps s^-2
  float _current_accel;  // current no. of steps s^-2
  float _jerk;  // no. of steps s^-3

  // Calibration variables
  float _steps_per_volt;  // no. of steps to move one volt reading of the potentiometer
  float _degs_per_volt;  // no. of degrees of rotation of the analysers per volt on the pots
  float _const_angle;  // constant for calibration curve
  uint8_t _calibration_step;  // step of the calibration state
  bool _calibration_mode;  // whether in calibration mode when moving to a set angle
  
  // Flags
  bool _home_flag;  // whether reached the home position (between 45 and 50 deg)
  bool _opto_flag;  // status of the crenelation opto-interrupter
  DoubleBoolCross _cren_flag;  // status of the crenelation flag (every 5 deg)
  bool _moving;  // status of the movement of the analyser

  // State for state machine
  State _state;

  // Private functions
  // Step the stepper for the step time set by the step timer
  void step()
  {
    if(_moving)
    {
      // If delay timer elapsed then write HIGH, if not write LOW
      digitalWrite(_drive_pin, _delay_timer.timedOut());

      if(_step_timer.timedOut())  // If step timer elapsed then write LOW
      {
        _step_timer.reset();  // Reset both timers for the beginning of next step
        _delay_timer.reset();
        _steps_to_move--;  // Decrement steps
      }
    }
  }

public:
  // Parameterised Constructor
  Stepper(uint8_t drive_pin, uint8_t dir_pin, uint8_t home_pin, uint8_t cren_opto_pin, uint8_t pot_pin)
  {
    _drive_pin = drive_pin;
    _dir_pin = dir_pin;
    _home_pin = home_pin;
    _cren_opto_pin = cren_opto_pin;
    _pot_pin = pot_pin;
  }

  // Initialiser
  void init();

  //-----Getter Functions---------
  float getStepsPerVolt() {return _steps_per_volt;}
  bool getHomeStatus() {return _home_flag;}
  uint16_t getCurrentAngle() {return _current_angle;}
  uint16_t getSetAngle() {return _set_angle;}
  uint16_t getPotVoltage() {return _pot_voltage;}
  bool getDirection() {return _direction;}
  float getCurrentRate() {return _current_rate;}
  float getCurrentAccel() {return _current_accel;}
  bool getMovingStatus() {return _moving;}
  uint16_t getStepsToMove() {return _steps_to_move;}
  uint16_t getMinAngle(){return _min_angle;}
  uint16_t getMaxAngle(){return _max_angle;}
  bool getCalibrationMode(){return _calibration_mode;}
  DoubleBoolCross getCrenFlag() {return _cren_flag;}
  MicroTimer getDelayTimer() {return &_delay_timer;}
  MicroTimer getStepTimer() {return &_step_timer;}
  MicroTimer getRampTimer() {return &_ramp_timer;}

  //--------Setter Functions----------
  void setMaxAngle(uint16_t max_angle);
  void setMinAngle(uint16_t min_angle);
  void setCurrentAngle(uint16_t current_angle);
  void setSetAngle(uint16_t set_angle);
  void setHomeAngle(uint16_t home_angle);
  void setDirection(bool direction) {_direction = direction;}
  void setMaxRate(float max_rate) {_max_rate = max_rate;}
  void setMinRate(float min_rate) {_min_rate = min_rate;}
  void setMaxAccel(float max_accel) {_max_accel = max_accel;}
  void setMinAccel(float min_accel) {_min_accel = min_accel;}
  void setJerk(float jerk) {_jerk = jerk;}
  void setStepsToMove(uint16_t steps_to_move) {_steps_to_move = steps_to_move;}
  void setStepsPerVolt(float steps_per_volt) {_steps_per_volt = steps_per_volt;}
  void setDegsPerVolt(float degs_per_volt) {_degs_per_volt = degs_per_volt;}
  void setConstAngle(float const_angle){_const_angle = const_angle;}
  void setState(State state){_state = state;}
  void setCalibrationMode(bool calibration_mode){_calibration_mode = calibration_mode;}
  void initDelayTimer(uint32_t delay_time) {_delay_timer.init(delay_time);}
  void initRampTimer(uint32_t ramp_time) {_ramp_timer.init(ramp_time);}
  void initDebounceTimer(uint32_t debounce_time){_debounce_timer.init(debounce_time);}

  //------------Helper Functions---------------
  bool readCrenelations();  // Reads the crenelations (every 5 degrees)
  bool readHome();  // Reads the home flag (between 45 and 50 deg)
  bool readCrenOpto(){return digitalRead(_cren_opto_pin);}  // Reads the crenelation opto
  bool checkCrenelations();  // Checks the crenelations for an edge
  void readPotVoltage();  // analog reads the pot pin and converts to an angle
  void calculateStepsToMove(uint16_t angle);  // calculates the number of steps to move 
  void writeDirection(uint16_t angle);  // writes the direction logic
  void calculateRate();  // Calculates the current steps per second to drive stepper
  void coerceRateAndAccel();  // Coerces the rates and acceleration to their min/max values
  void stop();  // Stops the motor
  float toRampTimescale(float v);  // Converts rates, acceleration, and jerk to the ramp timer timescale
  
  // -------------State Functions----------------
  void moveToSetAngle();  // Moves analyser to set angle
  void home();  // Homes analysers (around 45 degrees)
  void calibrate();  // Calibrates the number of steps per degree and volts per degree
  
  //----------------State Machine------------------
  void run();  // Runs the state machine
};

#endif
