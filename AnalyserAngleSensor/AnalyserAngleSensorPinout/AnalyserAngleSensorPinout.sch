EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Module:Arduino_Nano_v3.x A1
U 1 1 627959DD
P 4550 2200
F 0 "A1" H 4550 1111 50  0000 C CNN
F 1 "ArduinoNano" H 4550 1020 50  0000 C CNN
F 2 "Module:Arduino_Nano" H 4550 2200 50  0001 C CIN
F 3 "http://www.mouser.com/pdfdocs/Gravitech_Arduino_Nano3_0.pdf" H 4550 2200 50  0001 C CNN
	1    4550 2200
	1    0    0    -1  
$EndComp
Text GLabel 4050 1900 0    50   Input ~ 0
A2Crenelation
Text GLabel 4050 2100 0    50   Input ~ 0
A2Home
Text GLabel 4050 2400 0    50   Input ~ 0
A2Direction
Text GLabel 4050 2600 0    50   Input ~ 0
A2Step
$Comp
L Device:R_POT A1Potentiometer
U 1 1 6279713F
P 6000 1900
F 0 "A1Potentiometer" H 6650 1700 50  0000 R CNN
F 1 "1kΩ" H 5930 1945 50  0000 R CNN
F 2 "" H 6000 1900 50  0001 C CNN
F 3 "~" H 6000 1900 50  0001 C CNN
	1    6000 1900
	-1   0    0    1   
$EndComp
$Comp
L Device:R_POT A2Potentiometer
U 1 1 627974F2
P 6000 2500
F 0 "A2Potentiometer" H 6700 2400 50  0000 R CNN
F 1 "1kΩ" H 5930 2545 50  0000 R CNN
F 2 "" H 6000 2500 50  0001 C CNN
F 3 "~" H 6000 2500 50  0001 C CNN
	1    6000 2500
	-1   0    0    1   
$EndComp
Text GLabel 4050 1800 0    50   Input ~ 0
A1Crenelation
Text GLabel 4050 2000 0    50   Input ~ 0
A1Home
Text GLabel 4050 2500 0    50   Input ~ 0
A1Step
Text GLabel 4050 2300 0    50   Input ~ 0
A1Direction
Text GLabel 4050 2200 0    50   Input ~ 0
InterruptsRelay
Wire Wire Line
	4750 1200 6000 1200
Wire Wire Line
	6000 1200 6000 1700
Wire Wire Line
	4650 3200 6000 3200
Wire Wire Line
	6000 3200 6000 2700
Wire Wire Line
	6000 1700 6250 1700
Wire Wire Line
	6250 1700 6250 2350
Wire Wire Line
	6250 2350 6000 2350
Connection ~ 6000 1700
Wire Wire Line
	6000 1700 6000 1750
Wire Wire Line
	6000 2700 6300 2700
Wire Wire Line
	6300 2700 6300 2050
Wire Wire Line
	6300 2050 6000 2050
Connection ~ 6000 2700
Wire Wire Line
	6000 2700 6000 2650
Wire Wire Line
	5850 2500 5200 2500
Wire Wire Line
	5200 2500 5200 2400
Wire Wire Line
	5200 2400 5050 2400
Wire Wire Line
	5200 1900 5200 2300
Wire Wire Line
	5200 2300 5050 2300
Wire Wire Line
	5200 1900 5850 1900
$EndSCHEMATC
