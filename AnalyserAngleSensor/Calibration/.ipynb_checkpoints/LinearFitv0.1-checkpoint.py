# A file with functions to read in data and perform a linear fit of the data
# Date created 04/11/2020

import csv as csv

# Read a csv file and input into the arrays x, y and 
# the optional arrays xerr, yerr for the error bars
def readCSV(filename, x, y, xerr = None, yerr = None):
    # Read the file
    with open(filename) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter = ',')
        line_count = 0
        for row in csv_reader:
            # Get axes titles from headers of rows in file
            if line_count == 0:
                xtitle = row[0]
                ytitle = row[1]
                if yerr is not None:
                    yerrtitle = row[2]
                if xerr is not None:
                    xerrtitle = row[3]
                line_count += 1
            # Read in data to input arrays
            else:
                x.append(float(row[0]))
                y.append(float(row[1]))
                if yerr is not None:
                    yerr.append(float(row[2]))
                else:
                    if xerr is not None:
                        xerr.append(float(row[2]))
                if xerr is not None:
                    xerr.append(float(row[3]))
                line_count += 1
