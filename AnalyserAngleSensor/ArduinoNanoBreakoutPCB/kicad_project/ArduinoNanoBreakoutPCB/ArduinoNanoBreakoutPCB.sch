EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Arduino-Nano-Breakout-PCB"
Date "2020-11-10"
Rev "0.1"
Comp "University of Manchester"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Module:Arduino_Nano_v2.x A1
U 1 1 5FA96593
P 5200 3150
F 0 "A1" H 5100 4600 50  0000 C CNN
F 1 "Arduino_Nano_v2.x" H 5150 4500 50  0000 C CNN
F 2 "Module:Arduino_Nano" H 5200 3150 50  0001 C CIN
F 3 "https://www.arduino.cc/en/uploads/Main/ArduinoNanoManual23.pdf" H 5200 3150 50  0001 C CNN
	1    5200 3150
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 5FA989C1
P 2950 1600
F 0 "H1" H 3050 1646 50  0000 L CNN
F 1 "M2 MountingHole" H 3050 1555 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 2950 1600 50  0001 C CNN
F 3 "~" H 2950 1600 50  0001 C CNN
	1    2950 1600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5FA99986
P 2950 1800
F 0 "H2" H 3050 1846 50  0000 L CNN
F 1 "M2 MountingHole" H 3050 1755 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 2950 1800 50  0001 C CNN
F 3 "~" H 2950 1800 50  0001 C CNN
	1    2950 1800
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5FA99D9B
P 2950 2000
F 0 "H3" H 3050 2046 50  0000 L CNN
F 1 "M3 MountingHole" H 3050 1955 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 2950 2000 50  0001 C CNN
F 3 "~" H 2950 2000 50  0001 C CNN
	1    2950 2000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5FA9A068
P 2950 2200
F 0 "H4" H 3050 2246 50  0000 L CNN
F 1 "M3 MountingHole" H 3050 2155 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 2950 2200 50  0001 C CNN
F 3 "~" H 2950 2200 50  0001 C CNN
	1    2950 2200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H5
U 1 1 5FA9A402
P 2950 2400
F 0 "H5" H 3050 2446 50  0000 L CNN
F 1 "M4 MountingHole" H 3050 2355 50  0000 L CNN
F 2 "MountingHole:MountingHole_4.3mm_M4" H 2950 2400 50  0001 C CNN
F 3 "~" H 2950 2400 50  0001 C CNN
	1    2950 2400
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H6
U 1 1 5FA9A600
P 2950 2650
F 0 "H6" H 3050 2696 50  0000 L CNN
F 1 "M4 MountingHole" H 3050 2605 50  0000 L CNN
F 2 "MountingHole:MountingHole_4.3mm_M4" H 2950 2650 50  0001 C CNN
F 3 "~" H 2950 2650 50  0001 C CNN
	1    2950 2650
	1    0    0    -1  
$EndComp
Text GLabel 5700 3550 2    50   Input ~ 0
A4
Text GLabel 5700 3650 2    50   Input ~ 0
A5
$Comp
L Device:CP1 C1
U 1 1 5FAC6730
P 6850 2600
F 0 "C1" H 6965 2646 50  0000 L CNN
F 1 "0.1uF" H 6965 2555 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 6850 2600 50  0001 C CNN
F 3 "~" H 6850 2600 50  0001 C CNN
	1    6850 2600
	1    0    0    -1  
$EndComp
Text GLabel 5700 2550 2    50   Input ~ 0
Reset1
Text GLabel 5700 2650 2    50   Input ~ 0
Reset2
Text GLabel 5200 4350 3    50   Input ~ 0
GND
Text GLabel 6700 2450 0    50   Input ~ 0
Reset1
Wire Wire Line
	6700 2450 6850 2450
Wire Wire Line
	6850 2900 6850 2750
Text GLabel 6950 3400 3    50   Input ~ 0
GND
$Comp
L Switch:SW_SPDT SW1
U 1 1 5FB05225
P 6850 3100
F 0 "SW1" V 6804 3248 50  0000 L CNN
F 1 "SW_SPDT" V 6895 3248 50  0000 L CNN
F 2 "ArduinoNanoBreakoutPCB:SPDTSwitch_THT" H 6850 3100 50  0001 C CNN
F 3 "~" H 6850 3100 50  0001 C CNN
	1    6850 3100
	0    1    1    0   
$EndComp
Wire Wire Line
	6950 3400 6950 3300
Text Notes 6500 2000 0    50   ~ 0
Reset Switch and breakout of reset pins.
Text Notes 4900 1600 0    50   ~ 0
Arduino Nano
Text Notes 3000 1450 0    50   ~ 0
Mounting Holes
Text GLabel 4700 2550 0    50   Input ~ 0
RX
Text GLabel 4700 2650 0    50   Input ~ 0
TX
Text GLabel 4700 2750 0    50   Input ~ 0
D2
Text GLabel 4700 2850 0    50   Input ~ 0
D3
Text GLabel 4700 2950 0    50   Input ~ 0
D4
Text GLabel 4700 3050 0    50   Input ~ 0
D5
Text GLabel 4700 3150 0    50   Input ~ 0
D6
Text GLabel 4700 3250 0    50   Input ~ 0
D7
Text GLabel 4700 3350 0    50   Input ~ 0
D8
Text GLabel 4700 3450 0    50   Input ~ 0
D9
Text GLabel 4700 3550 0    50   Input ~ 0
D10
Text GLabel 4700 3650 0    50   Input ~ 0
D11
Text GLabel 4700 3750 0    50   Input ~ 0
D12
Text GLabel 4700 3850 0    50   Input ~ 0
D13
Wire Wire Line
	5200 4150 5200 4300
Wire Wire Line
	5200 4300 5300 4300
Wire Wire Line
	5300 4300 5300 4150
Wire Wire Line
	5200 4300 5200 4350
Connection ~ 5200 4300
Text GLabel 5350 5250 2    50   Input ~ 0
Vin
Text GLabel 5300 2150 1    50   Input ~ 0
3.3V
Text GLabel 5400 2150 1    50   Input ~ 0
5V
Text GLabel 5700 2950 2    50   Input ~ 0
Aref
Text GLabel 5700 3150 2    50   Input ~ 0
A0
Text GLabel 5700 3250 2    50   Input ~ 0
A1
Text GLabel 5700 3350 2    50   Input ~ 0
A2
Text GLabel 5700 3450 2    50   Input ~ 0
A3
Text GLabel 5700 3750 2    50   Input ~ 0
A6
Text GLabel 5700 3850 2    50   Input ~ 0
A7
Text Notes 6500 2250 0    50   ~ 0
When a capacitor is connected to the\nReset pin and to GND the Arduino \ncannot be reset.
$Comp
L Connector:Conn_01x03_Male J13
U 1 1 5FAE6829
P 7600 2900
F 0 "J13" H 7850 3250 50  0000 C CNN
F 1 "Molex_KK_254_01x03_Male" H 7850 3150 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-03A_1x03_P2.54mm_Vertical" H 7600 2900 50  0001 C CNN
F 3 "~" H 7600 2900 50  0001 C CNN
	1    7600 2900
	1    0    0    -1  
$EndComp
Text GLabel 7800 2800 2    50   Input ~ 0
Reset1
Text GLabel 7800 2900 2    50   Input ~ 0
Reset2
Text GLabel 7800 3000 2    50   Input ~ 0
GND
$Comp
L Connector:Conn_01x03_Male J1
U 1 1 5FAEAD42
P 3200 3200
F 0 "J1" H 3172 3132 50  0000 R CNN
F 1 "Molex_KK_254_01x03_Male" H 3172 3223 50  0000 R CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-03A_1x03_P2.54mm_Vertical" H 3200 3200 50  0001 C CNN
F 3 "~" H 3200 3200 50  0001 C CNN
	1    3200 3200
	-1   0    0    1   
$EndComp
Text GLabel 3000 3100 0    50   Input ~ 0
RX
Text GLabel 3000 3200 0    50   Input ~ 0
TX
Text GLabel 3000 3300 0    50   Input ~ 0
GND
Text Notes 2800 3000 0    50   ~ 0
Serial Line
Text Notes 2700 3550 0    50   ~ 0
Digital I/O Pins
Text GLabel 3000 3650 0    50   Input ~ 0
D2
Text GLabel 3000 3750 0    50   Input ~ 0
D3
Text GLabel 3000 3850 0    50   Input ~ 0
D4
Text GLabel 3000 3950 0    50   Input ~ 0
D5
Text GLabel 3000 4050 0    50   Input ~ 0
D6
Text GLabel 3000 4150 0    50   Input ~ 0
D7
Text GLabel 3000 4250 0    50   Input ~ 0
D8
Text GLabel 3000 4350 0    50   Input ~ 0
D9
Text GLabel 3000 4450 0    50   Input ~ 0
D10
Text GLabel 3100 4900 0    50   Input ~ 0
D11
Text GLabel 3100 5000 0    50   Input ~ 0
D12
Text GLabel 3000 4550 0    50   Input ~ 0
GND
Text Notes 2850 5500 0    50   ~ 0
I2C
Text Notes 2950 4800 0    50   ~ 0
SPI
$Comp
L Connector:Conn_01x10_Male J2
U 1 1 5FB0E07F
P 3200 4150
F 0 "J2" H 3172 4032 50  0000 R CNN
F 1 "Molex_KK_254_01x10_Male" H 3172 4123 50  0000 R CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-10A_1x10_P2.54mm_Vertical" H 3200 4150 50  0001 C CNN
F 3 "~" H 3200 4150 50  0001 C CNN
	1    3200 4150
	-1   0    0    1   
$EndComp
Text GLabel 3100 5200 0    50   Input ~ 0
GND
Text GLabel 3100 5100 0    50   Input ~ 0
D13
$Comp
L Connector:Conn_01x04_Male J4
U 1 1 5FB1664C
P 3300 5100
F 0 "J4" H 3272 4982 50  0000 R CNN
F 1 "Molex_254_KK_01x04_Male" H 3272 5073 50  0000 R CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 3300 5100 50  0001 C CNN
F 3 "~" H 3300 5100 50  0001 C CNN
	1    3300 5100
	-1   0    0    1   
$EndComp
Text Notes 2700 4950 0    50   ~ 0
MOSI
Text Notes 2700 5050 0    50   ~ 0
MISO
Text Notes 2700 5150 0    50   ~ 0
SCK
Text GLabel 3000 5600 0    50   Input ~ 0
A4
Text GLabel 3000 5700 0    50   Input ~ 0
A5
Text GLabel 3000 5800 0    50   Input ~ 0
GND
Text Notes 2700 5600 0    50   ~ 0
SDA
Text Notes 2700 5700 0    50   ~ 0
SCL
$Comp
L Connector:Conn_01x03_Male J3
U 1 1 5FB33A58
P 3200 5700
F 0 "J3" H 3172 5632 50  0000 R CNN
F 1 "Molex_254_KK_01x03_Male" H 3172 5723 50  0000 R CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-03A_1x03_P2.54mm_Vertical" H 3200 5700 50  0001 C CNN
F 3 "~" H 3200 5700 50  0001 C CNN
	1    3200 5700
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Male J6
U 1 1 5FB36668
P 6900 4200
F 0 "J6" H 6872 4082 50  0000 R CNN
F 1 "Molex_KK_254_01x02_Male" H 6872 4173 50  0000 R CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 6900 4200 50  0001 C CNN
F 3 "~" H 6900 4200 50  0001 C CNN
	1    6900 4200
	-1   0    0    1   
$EndComp
Text Notes 6650 4000 0    50   ~ 0
Analog Pins
Text GLabel 5700 2950 2    50   Input ~ 0
Aref
Text GLabel 6700 4100 0    50   Input ~ 0
Aref
Text GLabel 6700 4200 0    50   Input ~ 0
GND
$Comp
L Connector:Conn_01x02_Male J7
U 1 1 5FB3A0F9
P 6900 4400
F 0 "J7" H 6872 4282 50  0000 R CNN
F 1 "Molex_KK_254_01x02_Male" H 6872 4373 50  0000 R CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 6900 4400 50  0001 C CNN
F 3 "~" H 6900 4400 50  0001 C CNN
	1    6900 4400
	-1   0    0    1   
$EndComp
Text GLabel 6700 4300 0    50   Input ~ 0
A0
Text GLabel 6700 4400 0    50   Input ~ 0
GND
$Comp
L Connector:Conn_01x02_Male J8
U 1 1 5FB3EAC9
P 6900 4600
F 0 "J8" H 6872 4482 50  0000 R CNN
F 1 "Molex_KK_254_01x02_Male" H 6872 4573 50  0000 R CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 6900 4600 50  0001 C CNN
F 3 "~" H 6900 4600 50  0001 C CNN
	1    6900 4600
	-1   0    0    1   
$EndComp
Text GLabel 6700 4500 0    50   Input ~ 0
A1
Text GLabel 6700 4600 0    50   Input ~ 0
GND
Text GLabel 6700 4700 0    50   Input ~ 0
A2
Text GLabel 6700 4800 0    50   Input ~ 0
GND
$Comp
L Connector:Conn_01x02_Male J9
U 1 1 5FB3F704
P 6900 4800
F 0 "J9" H 6872 4682 50  0000 R CNN
F 1 "Molex_KK_254_01x02_Male" H 6872 4773 50  0000 R CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 6900 4800 50  0001 C CNN
F 3 "~" H 6900 4800 50  0001 C CNN
	1    6900 4800
	-1   0    0    1   
$EndComp
Text GLabel 6700 4900 0    50   Input ~ 0
A3
Text GLabel 6700 5000 0    50   Input ~ 0
GND
Text GLabel 6700 5100 0    50   Input ~ 0
A6
Text GLabel 6700 5300 0    50   Input ~ 0
A7
Text GLabel 6700 5200 0    50   Input ~ 0
GND
Text GLabel 6700 5400 0    50   Input ~ 0
GND
$Comp
L Connector:Conn_01x02_Male J10
U 1 1 5FB44135
P 6900 5000
F 0 "J10" H 6872 4882 50  0000 R CNN
F 1 "Molex_KK_254_01x02_Male" H 6872 4973 50  0000 R CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 6900 5000 50  0001 C CNN
F 3 "~" H 6900 5000 50  0001 C CNN
	1    6900 5000
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Male J11
U 1 1 5FB4458B
P 6900 5200
F 0 "J11" H 6872 5082 50  0000 R CNN
F 1 "Molex_KK_254_01x02_Male" H 6872 5173 50  0000 R CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 6900 5200 50  0001 C CNN
F 3 "~" H 6900 5200 50  0001 C CNN
	1    6900 5200
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Male J12
U 1 1 5FB448A0
P 6900 5400
F 0 "J12" H 6872 5282 50  0000 R CNN
F 1 "Molex_KK_254_01x02_Male" H 6872 5373 50  0000 R CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 6900 5400 50  0001 C CNN
F 3 "~" H 6900 5400 50  0001 C CNN
	1    6900 5400
	-1   0    0    1   
$EndComp
Text Notes 5050 4950 0    50   ~ 0
Power Pins
$Comp
L Connector:Conn_01x04_Male J5
U 1 1 5FB51B7F
P 5150 5350
F 0 "J5" H 5250 5700 50  0000 C CNN
F 1 "Molex_KK_254_01x04_Male" H 5250 5600 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-04A_1x04_P2.54mm_Vertical" H 5150 5350 50  0001 C CNN
F 3 "~" H 5150 5350 50  0001 C CNN
	1    5150 5350
	1    0    0    -1  
$EndComp
Text GLabel 5350 5350 2    50   Input ~ 0
3.3V
Text GLabel 5350 5450 2    50   Input ~ 0
5V
Text GLabel 5350 5550 2    50   Input ~ 0
GND
Text GLabel 5100 2150 1    50   Input ~ 0
Vin
$EndSCHEMATC
