//Rotary Encoder function definitions

#include "Rotary_Encoder.hpp"
#include <Arduino.h>


//Check the position of the rotary encoder
void RotaryEncoder::CheckPosition()
{
  static unsigned long lastInterruptTime = 0;
  unsigned long interruptTime = millis();  //Gets the number of milliseconds on clock

  //If interrupts faster than set bounce time, assume it's a  bounce and ignore
  if((interruptTime - lastInterruptTime) > 5)
  {
    if(digitalRead(pinB) == LOW)
    {
      virtualPosition--;
    }
    else
    {
      virtualPosition++;
    }
    //Keep virtual position between 0 and 100
    virtualPosition = min(100, max(0, virtualPosition));
    lastInterruptTime = interruptTime;  //To keep track of bounce
  }
}
