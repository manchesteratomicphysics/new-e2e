// This is a program to control a constant current power supply

#include "Rotary_Encoder.hpp"
// Define pins to write to
//int pwmPinCoarse = 9;
//int pwmPinFine = 10;

const int pushButtonPin = 4;
const int ledPin = 13;

uint8_t pinA{3};
uint8_t pinB{5};
uint8_t bounceTime{5};
uint8_t initialVirtualPos{0};
uint8_t lastCount{0};

//Create Rotary Encoder object and initialise pins, bounce time, and virtual position
RotaryEncoder r(pinA, pinB, bounceTime, initialVirtualPos);

/*Interrupt service routine, have to make this a void function without any arguments
to pass into an attach interrupt function*/
void Interrupt()
{
  r.CheckPosition();
}

void setup()
{
  //Whilst debugging read serial monitor
  Serial.begin(9600);

  //Switch is floating so use the in-built pull-up resistor
  pinMode(pushButtonPin, INPUT_PULLUP);  //Pulls up the switch to high
  pinMode(ledPin, OUTPUT);

   //Setup the rotary encoder as an interrupt
   r.setup();
   attachInterrupt(digitalPinToInterrupt(r.getpinA()), Interrupt, LOW); //Attach the interrupt to pinA when pinA goes low

  //Pulse-width modulation pins set to output
  //pinMode(pwmPinCoarse, OUTPUT);
  //pinMode(pwmPinFine, OUTPUT);
  /*Change the clock-select bits in Timer/Counter 1 Control Register B to a
  prescalar. This sets pins 9 & 10 to have a set PWM frequency:
    Bit-to-add | freq (Hz)
    B00000001    31250
    B00000010    3906.25
    B00000011    488.28125 <-- Default
    B00000100    122.0703125
    B00000101    30.517578125
  */
  TCCR1B = TCCR1B & B11111000 | B00000001;  //Set to 31 kHz PWM frequency
  //analogWrite(pwmPinCoarse, 255); //Set to 50% duty cycle for course pin

  Serial.println("Start:");
}

void loop()
{
  // for(uint8_t i = 0; i < 256; i++)
  // {
  //    analogWrite(pwmPinFine, 255 - 255*i);
  //    delay(5);
  //
  // }

  //Test for Push Button
  if(digitalRead(pushButtonPin) == 0)
  {
    digitalWrite(ledPin, HIGH);
    Serial.println("Button Pushed!" );
    delay(200);
  }
  else
  {
    digitalWrite(ledPin, LOW);
  }

  //if the current rotary position has changed then print to monitor
  if(r.getvirtualPosition()!=lastCount)
  {
    //Write out to serial monitor the value and direction
    Serial.print(r.getvirtualPosition() > lastCount ? "Up:" : "Down:");
    Serial.println(r.getvirtualPosition());

    //Keep track of new value
    lastCount = r.getvirtualPosition();
  }


}
