//Rotary Encoder Library

#ifndef ROTARY_ENCODER_H
#define ROTARY_ENCODER_H

#include <Arduino.h>

class RotaryEncoder
{
private:
  /*pinA = active pin is wired to rotary encoder (not ground)
    pinB = 2nd active pin wired to rotary encoder (not ground)
    bounceTime = time in ms which bounce could occur in
    virtualPosition = number from 0-100 to output to serial when encoder turns*/
  uint8_t pinA;
  uint8_t pinB;
  uint8_t bounceTime;
  volatile uint8_t virtualPosition;
public:
  //Default constructor
  RotaryEncoder(){}  //Initialise to a start of zero

  //Parameterised constructor
  RotaryEncoder(uint8_t pA, uint8_t pB, uint8_t bT, uint8_t virtualPos)
  {
    pinA = pA;
    pinB = pB;
    bounceTime = bT;
    virtualPosition = virtualPos;
  }

  //Check Position
  void CheckPosition();

  //Setup the rotary encoder pins
  void setup(uint8_t pA, uint8_t pB, uint8_t bT, uint8_t virtualPos)
  {
    pinA = pA;
    pinB = pB;
    bounceTime = bT;
    virtualPosition = virtualPos;
    //Initiate pins A & B so they are inputs and are initially HIGH
    pinMode(pinA, INPUT_PULLUP);
    pinMode(pinB, INPUT_PULLUP);
  }

  //Setup the rotary encoder pins
  void setup()
  {
    //Initiate pins A & B so they are inputs and are initially HIGH
    pinMode(pinA, INPUT_PULLUP);
    pinMode(pinB, INPUT_PULLUP);
  }

  //Getter and setter functions
  uint8_t getpinA(){return pinA;}
  uint8_t getpinB(){return pinB;}
  uint8_t getbouceTime(){return bounceTime;}
  uint8_t getvirtualPosition(){return virtualPosition;}
  void setbounceTime(uint8_t bT){bounceTime = bT;}
  void setvirtualPos(uint8_t vPos){virtualPosition = vPos;}

};


#endif /*ROTARY_ENCODER_HPP */
