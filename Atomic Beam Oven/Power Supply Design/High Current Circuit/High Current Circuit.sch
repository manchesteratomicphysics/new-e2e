EESchema Schematic File Version 4
LIBS:High Current Circuit-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "High Current Circuit"
Date "2019-06-19"
Rev "1"
Comp "University of Manchester"
Comment1 "For atomic beam oven constant current power supply"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Operational:TL081 OPA1
U 1 1 5D0A3A2B
P 3050 3150
F 0 "OPA1" H 3200 3350 50  0000 L CNN
F 1 "TL081" H 3200 3250 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 3100 3200 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl081.pdf" H 3200 3300 50  0001 C CNN
	1    3050 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C-OPA+1
U 1 1 5D0A6C8D
P 2700 2600
F 0 "C-OPA+1" H 3100 2600 50  0000 R CNN
F 1 "1uF" H 3050 2700 50  0000 R CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 2738 2450 50  0001 C CNN
F 3 "~" H 2700 2600 50  0001 C CNN
	1    2700 2600
	-1   0    0    1   
$EndComp
$Comp
L Device:C C-OPA-1
U 1 1 5D0A6D01
P 2950 3700
F 0 "C-OPA-1" H 3065 3746 50  0000 L CNN
F 1 "1uF" H 3065 3655 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 2988 3550 50  0001 C CNN
F 3 "~" H 2950 3700 50  0001 C CNN
	1    2950 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5D0A6D2B
P 3700 3600
F 0 "C2" H 3815 3646 50  0000 L CNN
F 1 "100nF" H 3815 3555 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 3738 3450 50  0001 C CNN
F 3 "~" H 3700 3600 50  0001 C CNN
	1    3700 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS MOSFET1
U 1 1 5D0A707D
P 4300 3150
F 0 "MOSFET1" H 4505 3196 50  0000 L CNN
F 1 "FQP9N90C" H 4505 3105 50  0000 L CNN
F 2 "Connector_JST:JST_EH_B03B-EH-A_1x03_P2.50mm_Vertical" H 4500 3250 50  0001 C CNN
F 3 "~" H 4300 3150 50  0001 C CNN
	1    4300 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:R PowerResistor1
U 1 1 5D0A70E7
P 4400 4450
F 0 "PowerResistor1" H 4470 4496 50  0000 L CNN
F 1 "2R" H 4470 4405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 4330 4450 50  0001 C CNN
F 3 "~" H 4400 4450 50  0001 C CNN
	1    4400 4450
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male Vcontrol1
U 1 1 5D0A7256
P 3250 1450
F 0 "Vcontrol1" H 3356 1628 50  0000 C CNN
F 1 "Conn_01x02_Male" H 3356 1537 50  0000 C CNN
F 2 "Connector_JST:JST_EH_B02B-EH-A_1x02_P2.50mm_Vertical" H 3250 1450 50  0001 C CNN
F 3 "~" H 3250 1450 50  0001 C CNN
	1    3250 1450
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male 80VSupply1
U 1 1 5D0A72DC
P 3250 1800
F 0 "80VSupply1" H 3356 1978 50  0000 C CNN
F 1 "Conn_01x02_Male" H 3356 1887 50  0000 C CNN
F 2 "Connector_JST:JST_EH_B02B-EH-A_1x02_P2.50mm_Vertical" H 3250 1800 50  0001 C CNN
F 3 "~" H 3250 1800 50  0001 C CNN
	1    3250 1800
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male 12VSupply1
U 1 1 5D0A73C3
P 3250 2300
F 0 "12VSupply1" H 3356 2578 50  0000 C CNN
F 1 "Conn_01x03_Male" H 3356 2487 50  0000 C CNN
F 2 "Connector_JST:JST_EH_B03B-EH-A_1x03_P2.50mm_Vertical" H 3250 2300 50  0001 C CNN
F 3 "~" H 3250 2300 50  0001 C CNN
	1    3250 2300
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male RLoad1
U 1 1 5D0A7B2D
P 4600 2800
F 0 "RLoad1" H 4573 2680 50  0000 R CNN
F 1 "Conn_01x02_Male" H 4573 2771 50  0000 R CNN
F 2 "Connector_JST:JST_EH_B02B-EH-A_1x02_P2.50mm_Vertical" H 4600 2800 50  0001 C CNN
F 3 "~" H 4600 2800 50  0001 C CNN
	1    4600 2800
	-1   0    0    1   
$EndComp
Text GLabel 3450 1450 2    50   Input ~ 0
Vcontrol
Text GLabel 3450 1550 2    50   Input ~ 0
0V
Text GLabel 3450 2200 2    50   Input ~ 0
+12V
Text GLabel 3450 2300 2    50   Input ~ 0
0V
Text GLabel 3450 2400 2    50   Input ~ 0
-12V
Text GLabel 3450 1800 2    50   Input ~ 0
+80V
Text GLabel 3450 1900 2    50   Input ~ 0
0V
Text GLabel 2700 2750 3    50   Input ~ 0
0V
Text GLabel 2950 3850 3    50   Input ~ 0
0V
Text GLabel 2100 3050 0    50   Input ~ 0
Vcontrol
Text GLabel 2850 3500 0    50   Input ~ 0
-12V
Wire Wire Line
	2950 3450 2950 3500
Wire Wire Line
	2850 3500 2950 3500
Connection ~ 2950 3500
Wire Wire Line
	2950 3500 2950 3550
Wire Wire Line
	2950 2850 2950 2700
Wire Wire Line
	2950 2450 2700 2450
Text GLabel 3050 2700 2    50   Input ~ 0
+12V
Wire Wire Line
	3050 2700 2950 2700
Connection ~ 2950 2700
Wire Wire Line
	2950 2700 2950 2450
Wire Wire Line
	2750 3250 2500 3250
Wire Wire Line
	2500 3250 2500 4100
Wire Wire Line
	2500 4100 3700 4100
Wire Wire Line
	3700 3150 3700 3450
Wire Wire Line
	3700 3750 3700 4100
Wire Wire Line
	3700 4100 4400 4100
Wire Wire Line
	4400 4100 4400 3350
Connection ~ 3700 4100
Wire Wire Line
	4400 4100 4400 4300
Connection ~ 4400 4100
Text GLabel 4400 4600 3    50   Input ~ 0
0V
Wire Wire Line
	4400 2950 4400 2800
Text GLabel 4400 2450 1    50   Input ~ 0
+80V
Wire Wire Line
	4400 2700 4400 2450
$Comp
L Device:C C1
U 1 1 5D0A98F0
P 2400 3200
F 0 "C1" H 2150 3200 50  0000 L CNN
F 1 "100nF" H 2100 3100 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W2.0mm_P2.50mm" H 2438 3050 50  0001 C CNN
F 3 "~" H 2400 3200 50  0001 C CNN
	1    2400 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 3050 2400 3050
Wire Wire Line
	2400 3050 2750 3050
Connection ~ 2400 3050
Text GLabel 2400 3350 3    50   Input ~ 0
0V
$Comp
L Device:R R_MOSFET1
U 1 1 5D1236D9
P 3950 3150
F 0 "R_MOSFET1" V 3743 3150 50  0000 C CNN
F 1 "100R" V 3834 3150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3880 3150 50  0001 C CNN
F 3 "~" H 3950 3150 50  0001 C CNN
	1    3950 3150
	0    1    1    0   
$EndComp
Wire Wire Line
	3350 3150 3700 3150
Connection ~ 3700 3150
Wire Wire Line
	3700 3150 3800 3150
Text Notes 1350 2950 0    50   ~ 0
Varies from 0-4V\nabove the reference voltage
$EndSCHEMATC
