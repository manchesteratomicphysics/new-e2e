EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Transformer for High Current Circuit"
Date "2019-06-20"
Rev "1"
Comp "University of Manchester"
Comment1 "For atomic beam oven constant current power supply"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L pspice:C C1
U 1 1 5D0B66FF
P 6350 4600
F 0 "C1" H 6528 4646 50  0000 L CNN
F 1 "10,000uF" H 6528 4555 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D40.0mm_P10.00mm_SnapIn" H 6350 4600 50  0001 C CNN
F 3 "~" H 6350 4600 50  0001 C CNN
	1    6350 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 4950 4700 4950
Wire Wire Line
	4700 4950 4700 4450
Wire Wire Line
	4700 4450 4600 4450
Wire Wire Line
	4600 4750 4750 4750
Wire Wire Line
	4750 4750 4750 5250
Wire Wire Line
	4750 5250 4600 5250
Wire Wire Line
	4700 4450 5000 4450
Wire Wire Line
	5000 4450 5000 4250
Wire Wire Line
	5000 4250 5500 4250
Connection ~ 4700 4450
Wire Wire Line
	4750 4750 5000 4750
Wire Wire Line
	5000 4750 5000 4850
Wire Wire Line
	5000 4850 5500 4850
Connection ~ 4750 4750
Wire Wire Line
	5800 4550 6100 4550
Wire Wire Line
	6100 4550 6100 4350
Wire Wire Line
	6350 4900 6350 4850
Text Notes 3850 5400 0    50   ~ 0
160 VA, +55 Vrms, 2.9A
Text Notes 7900 4550 0    50   ~ 0
+80V
Text Notes 7950 4700 0    50   ~ 0
GND\n
Connection ~ 6350 4350
Wire Wire Line
	6100 4350 6350 4350
$Comp
L pspice:C C2
U 1 1 5D0CCAF5
P 7100 4600
F 0 "C2" H 7278 4646 50  0000 L CNN
F 1 "100nF" H 7278 4555 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.4mm_W2.1mm_P2.50mm" H 7100 4600 50  0001 C CNN
F 3 "~" H 7100 4600 50  0001 C CNN
	1    7100 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 4350 7100 4350
Wire Wire Line
	7100 4900 7100 4850
Wire Wire Line
	7650 4900 7650 4650
Wire Wire Line
	7100 4350 7650 4350
Wire Wire Line
	7650 4350 7650 4550
Connection ~ 7100 4350
$Comp
L Diode_Bridge:GBU4G D1
U 1 1 5D0B62B4
P 5500 4550
F 0 "D1" H 5841 4596 50  0000 L CNN
F 1 "GBU2506F" H 5841 4505 50  0000 L CNN
F 2 "Diode_THT:Diode_Bridge_Vishay_GBU" H 5650 4675 50  0001 L CNN
F 3 "http://www.vishay.com/docs/88656/gbu4a.pdf" H 5500 4550 50  0001 C CNN
	1    5500 4550
	1    0    0    -1  
$EndComp
Text GLabel 7650 5000 3    50   Input ~ 0
GND
Wire Wire Line
	7100 4900 7650 4900
Connection ~ 7100 4900
Wire Wire Line
	6350 4900 7100 4900
Connection ~ 6350 4900
Wire Wire Line
	5200 4900 6350 4900
Wire Wire Line
	5200 4550 5200 4900
Wire Wire Line
	7650 4900 7650 5000
Connection ~ 7650 4900
$Comp
L Connector:Conn_01x01_Male J80V1
U 1 1 5D1C78C3
P 7850 4550
F 0 "J80V1" H 7900 4400 50  0000 R CNN
F 1 "Conn_01x01_Male" H 7650 4400 50  0000 R CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x01_P2.00mm_Vertical" H 7850 4550 50  0001 C CNN
F 3 "~" H 7850 4550 50  0001 C CNN
	1    7850 4550
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Male JGND1
U 1 1 5D1C7927
P 7850 4650
F 0 "JGND1" H 7800 4750 50  0000 C CNN
F 1 "Conn_01x01_Male" H 7350 4750 50  0000 C CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x01_P2.00mm_Vertical" H 7850 4650 50  0001 C CNN
F 3 "~" H 7850 4650 50  0001 C CNN
	1    7850 4650
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5D1C870F
P 3900 3450
F 0 "H2" H 4000 3496 50  0000 L CNN
F 1 "MountingHole" H 4000 3405 50  0000 L CNN
F 2 "MountingHole:MountingHole_4mm" H 3900 3450 50  0001 C CNN
F 3 "~" H 3900 3450 50  0001 C CNN
	1    3900 3450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5D1C879F
P 3900 3650
F 0 "H3" H 4000 3696 50  0000 L CNN
F 1 "MountingHole" H 4000 3605 50  0000 L CNN
F 2 "MountingHole:MountingHole_4mm" H 3900 3650 50  0001 C CNN
F 3 "~" H 3900 3650 50  0001 C CNN
	1    3900 3650
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5D1C87F7
P 3900 3850
F 0 "H4" H 4000 3896 50  0000 L CNN
F 1 "MountingHole" H 4000 3805 50  0000 L CNN
F 2 "MountingHole:MountingHole_4mm" H 3900 3850 50  0001 C CNN
F 3 "~" H 3900 3850 50  0001 C CNN
	1    3900 3850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 5D1C8823
P 3900 3250
F 0 "H1" H 4000 3296 50  0000 L CNN
F 1 "MountingHole" H 4000 3205 50  0000 L CNN
F 2 "MountingHole:MountingHole_4mm" H 3900 3250 50  0001 C CNN
F 3 "~" H 3900 3250 50  0001 C CNN
	1    3900 3250
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male JGND2
U 1 1 5D1CBCAB
P 5000 3650
F 0 "JGND2" H 5100 3850 50  0000 C CNN
F 1 "Conn_01x01_Male" H 5100 3750 50  0000 C CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x01_P2.00mm_Vertical" H 5000 3650 50  0001 C CNN
F 3 "~" H 5000 3650 50  0001 C CNN
	1    5000 3650
	1    0    0    -1  
$EndComp
Text GLabel 5200 3650 2    50   Input ~ 0
GND
$Comp
L TransformerBoard-rescue:VTX-146-160-155-Custom_Library U1
U 1 1 5D1CD0E8
P 4500 4900
F 0 "U1" H 4431 5575 50  0000 C CNN
F 1 "VTX-146-160-155" H 4431 5484 50  0000 C CNN
F 2 "Custom_Library:VTX-146-169-155" H 4450 5200 50  0001 C CNN
F 3 "" H 4450 5200 50  0001 C CNN
	1    4500 4900
	1    0    0    -1  
$EndComp
$EndSCHEMATC
