EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Fan Transformer"
Date "2019-06-20"
Rev "1"
Comp "University of Manchester"
Comment1 "Transformer board to supply 12V to a fan"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:D_Bridge_-AA+ D1
U 1 1 5D0BCDB5
P 5700 4850
F 0 "D1" H 6041 4896 50  0000 L CNN
F 1 "W08G" H 6041 4805 50  0000 L CNN
F 2 "Custom_Library:Vishay W08G Diode Bridge" H 5700 4850 50  0001 C CNN
F 3 "~" H 5700 4850 50  0001 C CNN
	1    5700 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5D0BCE0C
P 6500 4850
F 0 "C1" H 6615 4896 50  0000 L CNN
F 1 "10,000uF" H 6615 4805 50  0000 L CNN
F 2 "Capacitor_THT:C_Radial_D18.0mm_H35.5mm_P7.50mm" H 6538 4700 50  0001 C CNN
F 3 "~" H 6500 4850 50  0001 C CNN
	1    6500 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5D0BCE38
P 7050 4850
F 0 "C2" H 7165 4896 50  0000 L CNN
F 1 "100nF" H 7165 4805 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.8mm_W2.6mm_P2.50mm" H 7088 4700 50  0001 C CNN
F 3 "~" H 7050 4850 50  0001 C CNN
	1    7050 4850
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male JFan1
U 1 1 5D0BCEA1
P 7950 4900
F 0 "JFan1" H 7650 5000 50  0000 L CNN
F 1 "Conn_01x02_Male" H 7150 4900 50  0000 L CNN
F 2 "Connector_JST:JST_XH_B02B-XH-A_1x02_P2.50mm_Vertical" H 7950 4900 50  0001 C CNN
F 3 "~" H 7950 4900 50  0001 C CNN
	1    7950 4900
	-1   0    0    1   
$EndComp
$Comp
L FanTransformer-rescue:Transformer-12625B-Custom_Library T9V1
U 1 1 5D0BD11C
P 4350 4950
F 0 "T9V1" H 4350 5565 50  0000 C CNN
F 1 "Transformer-12625B" H 4350 5474 50  0000 C CNN
F 2 "Custom_Library:Transformer_Farnell_12625B_9V" H 4350 4950 50  0001 C CNN
F 3 "" H 4350 4950 50  0001 C CNN
	1    4350 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 4850 3800 5050
Wire Wire Line
	4900 4550 5000 4550
Wire Wire Line
	4900 5050 5000 5050
Wire Wire Line
	5000 5050 5000 4550
Connection ~ 5000 4550
Wire Wire Line
	5000 4550 5700 4550
Wire Wire Line
	4900 4850 5100 4850
Wire Wire Line
	5300 4850 5300 5150
Wire Wire Line
	5300 5150 5700 5150
Wire Wire Line
	4900 5350 5100 5350
Wire Wire Line
	5100 5350 5100 4850
Connection ~ 5100 4850
Wire Wire Line
	5100 4850 5300 4850
Wire Wire Line
	5400 4850 5400 5200
Wire Wire Line
	5400 5200 6500 5200
Wire Wire Line
	6500 5200 6500 5000
Wire Wire Line
	6000 4850 6000 4550
Wire Wire Line
	6000 4550 6500 4550
Wire Wire Line
	6500 4550 6500 4700
Wire Wire Line
	6500 4550 7050 4550
Wire Wire Line
	7050 4550 7050 4700
Connection ~ 6500 4550
Wire Wire Line
	6500 5200 7050 5200
Wire Wire Line
	7050 5200 7050 5000
Connection ~ 6500 5200
Wire Wire Line
	7050 4550 7750 4550
Wire Wire Line
	7750 4550 7750 4800
Connection ~ 7050 4550
Wire Wire Line
	7050 5200 7750 5200
Wire Wire Line
	7750 5200 7750 4900
Connection ~ 7050 5200
Text Notes 4250 5500 0    50   ~ 0
9Vrms
Text Notes 7900 4750 0    50   ~ 0
+12V
Text Notes 7900 5000 0    50   ~ 0
0V
Text GLabel 3800 5350 0    50   Input ~ 0
Live
Text GLabel 3800 4550 0    50   Input ~ 0
Neutral
Text GLabel 7050 5350 3    50   Input ~ 0
GND
Wire Wire Line
	7050 5200 7050 5350
$Comp
L Mechanical:MountingHole H1
U 1 1 5D1CDFE9
P 3050 3750
F 0 "H1" H 3150 3796 50  0000 L CNN
F 1 "MountingHole" H 3150 3705 50  0000 L CNN
F 2 "MountingHole:MountingHole_4mm" H 3050 3750 50  0001 C CNN
F 3 "~" H 3050 3750 50  0001 C CNN
	1    3050 3750
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5D1CE027
P 3050 4000
F 0 "H2" H 3150 4046 50  0000 L CNN
F 1 "MountingHole" H 3150 3955 50  0000 L CNN
F 2 "MountingHole:MountingHole_4mm" H 3050 4000 50  0001 C CNN
F 3 "~" H 3050 4000 50  0001 C CNN
	1    3050 4000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male JLive1
U 1 1 5D1DC50F
P 2600 4650
F 0 "JLive1" H 2706 4828 50  0000 C CNN
F 1 "Conn_01x01_Male" H 2706 4737 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 2600 4650 50  0001 C CNN
F 3 "~" H 2600 4650 50  0001 C CNN
	1    2600 4650
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male JGND1
U 1 1 5D1DC5EB
P 2600 4900
F 0 "JGND1" H 2706 5078 50  0000 C CNN
F 1 "Conn_01x01_Male" H 2706 4987 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 2600 4900 50  0001 C CNN
F 3 "~" H 2600 4900 50  0001 C CNN
	1    2600 4900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male JNeutral1
U 1 1 5D1DC617
P 2600 5200
F 0 "JNeutral1" H 2706 5378 50  0000 C CNN
F 1 "Conn_01x01_Male" H 2706 5287 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 2600 5200 50  0001 C CNN
F 3 "~" H 2600 5200 50  0001 C CNN
	1    2600 5200
	1    0    0    -1  
$EndComp
Text GLabel 2800 5200 2    50   Input ~ 0
Neutral
Text GLabel 2800 4650 2    50   Input ~ 0
Live
Text GLabel 2800 4900 2    50   Input ~ 0
GND
$Comp
L Mechanical:MountingHole H3
U 1 1 5D1E001B
P 3900 3750
F 0 "H3" H 4000 3796 50  0000 L CNN
F 1 "MountingHole" H 4000 3705 50  0000 L CNN
F 2 "MountingHole:MountingHole_4mm" H 3900 3750 50  0001 C CNN
F 3 "~" H 3900 3750 50  0001 C CNN
	1    3900 3750
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5D1E00BC
P 3900 4000
F 0 "H4" H 4000 4046 50  0000 L CNN
F 1 "MountingHole" H 4000 3955 50  0000 L CNN
F 2 "MountingHole:MountingHole_4mm" H 3900 4000 50  0001 C CNN
F 3 "~" H 3900 4000 50  0001 C CNN
	1    3900 4000
	1    0    0    -1  
$EndComp
$EndSCHEMATC
