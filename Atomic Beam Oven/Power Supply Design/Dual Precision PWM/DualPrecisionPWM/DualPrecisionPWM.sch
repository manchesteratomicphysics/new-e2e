EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Double Precision PWM"
Date "2019-06-18"
Rev "2"
Comp "University of Manchester"
Comment1 "For atomic beam oven constant current power supply"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L DualPrecisionPWM-rescue:Arduino_Nano_v2.x-MCU_Module A1
U 1 1 5C829738
P 3550 2300
F 0 "A1" H 3400 1300 50  0000 C CNN
F 1 "Arduino_Nano_v2.x" H 4350 1450 50  0000 C CNN
F 2 "Module:Arduino_Nano_WithMountingHoles" H 3700 1350 50  0001 L CNN
F 3 "https://www.arduino.cc/en/uploads/Main/ArduinoNanoManual23.pdf" H 3550 1300 50  0001 C CNN
	1    3550 2300
	1    0    0    -1  
$EndComp
$Comp
L DualPrecisionPWM-rescue:HCPL-263A-Isolator OC1
U 1 1 5C829B20
P 2250 4600
F 0 "OC1" H 2250 5278 50  0000 C CNN
F 1 "HCPL-263A" H 2250 5187 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 2350 3880 50  0001 C CNN
F 3 "http://docs.avagotech.com/docs/AV02-0391EN" H 1850 4950 50  0001 C CNN
	1    2250 4600
	1    0    0    -1  
$EndComp
$Comp
L DualPrecisionPWM-rescue:Quad-DualPrecisionPWM OPA4277
U 1 1 5C867B82
P 5450 4850
F 0 "OPA4277" H 5400 5765 50  0000 C CNN
F 1 "Quad1" H 5400 5674 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket" H 5250 5250 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2902-n.pdf" H 4850 5750 50  0001 C CNN
	1    5450 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT RV_Fine1
U 1 1 5C868754
P 5400 3500
F 0 "RV_Fine1" V 5286 3500 50  0000 C CNN
F 1 "5k" V 5400 3500 50  0000 C CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3296W_Vertical" H 5400 3500 50  0001 C CNN
F 3 "~" H 5400 3500 50  0001 C CNN
	1    5400 3500
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R_LPF1-1
U 1 1 5C86889C
P 3350 4400
F 0 "R_LPF1-1" V 3250 4300 50  0000 C CNN
F 1 "10k" V 3350 4400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 3280 4400 50  0001 C CNN
F 3 "~" H 3350 4400 50  0001 C CNN
	1    3350 4400
	0    1    1    0   
$EndComp
$Comp
L Device:C C_LPF1-1
U 1 1 5C868953
P 3500 4550
F 0 "C_LPF1-1" H 3615 4596 50  0000 L CNN
F 1 "100nF" H 3615 4505 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 3538 4400 50  0001 C CNN
F 3 "~" H 3500 4550 50  0001 C CNN
	1    3500 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R_LPF1-2
U 1 1 5C868DEE
P 3850 4400
F 0 "R_LPF1-2" V 3750 4400 50  0000 C CNN
F 1 "10k" V 3850 4400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 3780 4400 50  0001 C CNN
F 3 "~" H 3850 4400 50  0001 C CNN
	1    3850 4400
	0    1    1    0   
$EndComp
$Comp
L Device:C C_LPF1-2
U 1 1 5C868DF5
P 4000 4550
F 0 "C_LPF1-2" H 4115 4596 50  0000 L CNN
F 1 "100nF" H 4115 4505 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 4038 4400 50  0001 C CNN
F 3 "~" H 4000 4550 50  0001 C CNN
	1    4000 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 4400 3500 4400
Connection ~ 3500 4400
Wire Wire Line
	4000 4700 3500 4700
$Comp
L Device:R R_LPF1-3
U 1 1 5C868F97
P 3350 5000
F 0 "R_LPF1-3" V 3250 4900 50  0000 C CNN
F 1 "10k" V 3350 5000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 3280 5000 50  0001 C CNN
F 3 "~" H 3350 5000 50  0001 C CNN
	1    3350 5000
	0    1    1    0   
$EndComp
$Comp
L Device:C C_LPF1-3
U 1 1 5C868F9E
P 3500 5150
F 0 "C_LPF1-3" H 3000 5050 50  0000 L CNN
F 1 "100nF" H 3150 4950 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 3538 5000 50  0001 C CNN
F 3 "~" H 3500 5150 50  0001 C CNN
	1    3500 5150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R_LPF1-4
U 1 1 5C868FAB
P 3850 5000
F 0 "R_LPF1-4" V 3750 4850 50  0000 C CNN
F 1 "10k" V 3850 5000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 3780 5000 50  0001 C CNN
F 3 "~" H 3850 5000 50  0001 C CNN
	1    3850 5000
	0    1    1    0   
$EndComp
$Comp
L Device:C C_LPF1-4
U 1 1 5C868FB2
P 4000 5150
F 0 "C_LPF1-4" H 4000 5000 50  0000 L CNN
F 1 "100nF" H 4000 4900 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 4038 5000 50  0001 C CNN
F 3 "~" H 4000 5150 50  0001 C CNN
	1    4000 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 5000 3500 5000
Connection ~ 3500 5000
Wire Wire Line
	4000 5300 3500 5300
$Comp
L Device:R R_OCO1-1
U 1 1 5C869E74
P 2650 4250
F 0 "R_OCO1-1" H 2720 4296 50  0000 L CNN
F 1 "1k " H 2600 4250 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2580 4250 50  0001 C CNN
F 3 "~" H 2650 4250 50  0001 C CNN
	1    2650 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R_OCO1-2
U 1 1 5C869FA2
P 2750 4600
F 0 "R_OCO1-2" H 2820 4646 50  0000 L CNN
F 1 "1k" H 2700 4600 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2680 4600 50  0001 C CNN
F 3 "~" H 2750 4600 50  0001 C CNN
	1    2750 4600
	1    0    0    -1  
$EndComp
Connection ~ 2650 4100
Wire Wire Line
	2650 4100 2750 4100
Wire Wire Line
	2750 4100 2750 4450
Connection ~ 2750 4100
Wire Wire Line
	2750 4750 2750 4800
Wire Wire Line
	2250 4100 2650 4100
Wire Wire Line
	2550 4400 2650 4400
Connection ~ 2650 4400
Wire Wire Line
	2550 4800 2750 4800
Wire Wire Line
	2750 4100 2850 4100
$Comp
L Device:C C_OC1
U 1 1 5C86B8EA
P 2850 5050
F 0 "C_OC1" H 2965 5096 50  0000 L CNN
F 1 "100nF" H 2965 5005 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 2888 4900 50  0001 C CNN
F 3 "~" H 2850 5050 50  0001 C CNN
	1    2850 5050
	1    0    0    -1  
$EndComp
Connection ~ 2750 4800
Wire Wire Line
	2250 5100 2250 5200
Wire Wire Line
	2850 5200 2250 5200
Wire Wire Line
	2650 4400 3200 4400
Wire Wire Line
	3200 4800 3200 5000
Wire Wire Line
	2750 4800 3200 4800
$Comp
L Device:R R_OCI1-2
U 1 1 5C86DED4
P 1700 5150
F 0 "R_OCI1-2" H 1770 5196 50  0000 L CNN
F 1 "220" V 1700 5100 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 1630 5150 50  0001 C CNN
F 3 "~" H 1700 5150 50  0001 C CNN
	1    1700 5150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R_OCI1-1
U 1 1 5C86E3BB
P 1400 5150
F 0 "R_OCI1-1" H 950 5150 50  0000 L CNN
F 1 "220" V 1400 5100 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 1330 5150 50  0001 C CNN
F 3 "~" H 1400 5150 50  0001 C CNN
	1    1400 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 4500 1700 4500
Wire Wire Line
	1700 4500 1700 5000
Wire Wire Line
	1950 4700 1400 4700
Wire Wire Line
	1400 4700 1400 5000
Wire Wire Line
	1400 5300 1400 5400
Wire Wire Line
	1400 5400 1700 5400
Wire Wire Line
	1700 5300 1700 5400
Wire Wire Line
	1700 5400 1700 5500
Connection ~ 1700 5400
Wire Wire Line
	4000 4400 4450 4400
Wire Wire Line
	4450 4400 4450 4600
Wire Wire Line
	4450 4600 4600 4600
Connection ~ 4000 4400
$Comp
L Device:R R_PDFine1-2
U 1 1 5C873B7F
P 5100 3500
F 0 "R_PDFine1-2" V 5200 3550 50  0000 C CNN
F 1 "33k" V 5100 3500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 5030 3500 50  0001 C CNN
F 3 "~" H 5100 3500 50  0001 C CNN
	1    5100 3500
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R_PDFine1-1
U 1 1 5C873C07
P 4800 3500
F 0 "R_PDFine1-1" V 4850 3850 50  0000 C CNN
F 1 "220k" V 4800 3500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 4730 3500 50  0001 C CNN
F 3 "~" H 4800 3500 50  0001 C CNN
	1    4800 3500
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R_PDCoarse1
U 1 1 5C873C96
P 7000 4700
F 0 "R_PDCoarse1" H 7070 4746 50  0000 L CNN
F 1 "1k" H 6950 4700 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6930 4700 50  0001 C CNN
F 3 "~" H 7000 4700 50  0001 C CNN
	1    7000 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 5700 7000 5700
Wire Wire Line
	7000 4850 7000 5700
$Comp
L Device:R R_Inv1-2
U 1 1 5C879019
P 6550 5300
F 0 "R_Inv1-2" V 6650 5050 50  0000 C CNN
F 1 "1k" V 6550 5300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6480 5300 50  0001 C CNN
F 3 "~" H 6550 5300 50  0001 C CNN
	1    6550 5300
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R_Inv1-3
U 1 1 5C8791C3
P 6350 5500
F 0 "R_Inv1-3" H 6400 5500 50  0000 L CNN
F 1 "1k" H 6300 5500 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6280 5500 50  0001 C CNN
F 3 "~" H 6350 5500 50  0001 C CNN
	1    6350 5500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R_Inv1-1
U 1 1 5C8798E3
P 6450 4300
F 0 "R_Inv1-1" H 6520 4346 50  0000 L CNN
F 1 "1k" H 6520 4255 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6380 4300 50  0001 C CNN
F 3 "~" H 6450 4300 50  0001 C CNN
	1    6450 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 4200 6200 4150
Wire Wire Line
	6200 4450 6450 4450
Wire Wire Line
	6700 4150 6700 5300
Wire Wire Line
	6200 4150 6450 4150
Connection ~ 6450 4150
Connection ~ 6450 4450
Wire Wire Line
	6450 4450 7000 4450
Wire Wire Line
	6200 4650 6400 4650
Wire Wire Line
	6400 4650 6400 4700
Wire Wire Line
	6450 4150 6700 4150
Wire Wire Line
	6200 5300 6350 5300
Wire Wire Line
	6350 5300 6350 5350
Connection ~ 6350 5300
Wire Wire Line
	6350 5300 6400 5300
Wire Wire Line
	6350 5650 6200 5650
Wire Wire Line
	6200 5650 6200 5500
Wire Wire Line
	6200 5650 6200 5800
Wire Wire Line
	6200 5800 7150 5800
Wire Wire Line
	7150 5800 7150 4950
Wire Wire Line
	7150 4950 7450 4950
Connection ~ 6200 5650
Wire Wire Line
	2850 4100 2850 4900
Wire Wire Line
	4600 3500 4650 3500
Wire Wire Line
	5550 3500 5700 3500
Wire Wire Line
	5700 3500 5700 3600
Wire Wire Line
	5400 3350 5900 3350
Wire Wire Line
	5900 3350 5900 3700
Wire Wire Line
	5900 3700 7000 3700
Wire Wire Line
	7000 3700 7000 4450
Connection ~ 7000 4450
Wire Wire Line
	7000 4450 7000 4550
Text GLabel 3050 2600 0    50   Input ~ 0
PWM_D9
Text GLabel 3050 2700 0    50   Input ~ 0
PWM_D10
Text GLabel 1950 4300 0    50   Input ~ 0
PWM_D9
Text GLabel 1950 4900 0    50   Input ~ 0
PWM_D10
Text GLabel 8300 4850 2    50   Input ~ 0
Vcontrol
Wire Wire Line
	2250 5200 2250 5250
Connection ~ 2250 5200
Wire Wire Line
	3500 5300 3500 5350
Connection ~ 3500 5300
Wire Wire Line
	3500 4700 3500 4750
Connection ~ 3500 4700
Connection ~ 4600 4200
Connection ~ 4600 5500
Wire Wire Line
	4600 5500 4600 5700
Wire Wire Line
	4600 4400 4600 4200
Wire Wire Line
	4600 4200 4600 3500
Text GLabel 7450 4950 2    50   Input ~ 0
Vcontrol
$Comp
L Regulator_Linear:L7805 L1
U 1 1 5C87B661
P 2050 3200
F 0 "L1" H 2050 3442 50  0000 C CNN
F 1 "L7805" H 2050 3351 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 2075 3050 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 2050 3150 50  0001 C CNN
	1    2050 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C_LO1
U 1 1 5C87B7D8
P 2400 3350
F 0 "C_LO1" H 2515 3396 50  0000 L CNN
F 1 "1uF" H 2515 3305 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 2438 3200 50  0001 C CNN
F 3 "~" H 2400 3350 50  0001 C CNN
	1    2400 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 3200 2400 3200
Wire Wire Line
	2050 3500 2050 3700
Wire Wire Line
	2050 3700 1600 3700
$Comp
L Device:C C_LI1
U 1 1 5C8980DB
P 1600 3550
F 0 "C_LI1" H 1715 3596 50  0000 L CNN
F 1 "1uF" H 1715 3505 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 1638 3400 50  0001 C CNN
F 3 "~" H 1600 3550 50  0001 C CNN
	1    1600 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 3400 1600 3200
Wire Wire Line
	1600 3200 1750 3200
Wire Wire Line
	2400 3500 2050 3500
Connection ~ 2050 3500
Wire Wire Line
	2050 3700 2050 3750
Connection ~ 2400 3200
Wire Wire Line
	1600 3200 1550 3200
Connection ~ 1600 3200
$Comp
L Connector:Conn_01x03_Male Conn_3x_Male_In1
U 1 1 5C8AE05D
P 6100 1500
F 0 "Conn_3x_Male_In1" H 6206 1778 50  0000 C CNN
F 1 "+12V, GND, -12V" H 6200 1700 50  0000 C CNN
F 2 "Connector:FanPinHeader_1x03_P2.54mm_Vertical" H 6100 1500 50  0001 C CNN
F 3 "~" H 6100 1500 50  0001 C CNN
	1    6100 1500
	1    0    0    -1  
$EndComp
Text GLabel 6300 1400 2    50   Input ~ 0
+12V_RealEarth
Text GLabel 6300 1600 2    50   Input ~ 0
-12V_RealEarth
Text GLabel 6300 1500 2    50   Input ~ 0
GND
$Comp
L Device:C C_OPA+1
U 1 1 5C8BCAC7
P 4500 5000
F 0 "C_OPA+1" H 4050 5050 50  0000 L CNN
F 1 "1uF" H 4100 4950 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 4538 4850 50  0001 C CNN
F 3 "~" H 4500 5000 50  0001 C CNN
	1    4500 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 4850 4500 4850
Connection ~ 4500 4850
Wire Wire Line
	4500 4850 4600 4850
Connection ~ 4000 5000
Wire Wire Line
	4500 5150 4450 5150
$Comp
L Device:C C_OPA-1
U 1 1 5C8EA66E
P 6350 5000
F 0 "C_OPA-1" H 6450 5050 50  0000 L CNN
F 1 "1uF" H 6450 4950 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 6388 4850 50  0001 C CNN
F 3 "~" H 6350 5000 50  0001 C CNN
	1    6350 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 4850 6350 4850
Connection ~ 6350 4850
Wire Wire Line
	6350 4850 6450 4850
Wire Wire Line
	6200 5100 6300 5100
Wire Wire Line
	6300 5100 6300 5150
Wire Wire Line
	6300 5150 6350 5150
Connection ~ 6350 5150
Wire Wire Line
	6350 5150 6400 5150
Wire Wire Line
	4600 5200 4600 5500
Wire Wire Line
	4000 5000 4600 5000
$Comp
L Mechanical:MountingHole H1
U 1 1 5C8FCE68
P 7150 1100
F 0 "H1" H 7250 1146 50  0000 L CNN
F 1 "MountingHole" H 7250 1055 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 7150 1100 50  0001 C CNN
F 3 "~" H 7150 1100 50  0001 C CNN
	1    7150 1100
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5C8FD0BC
P 7150 1300
F 0 "H2" H 7250 1346 50  0000 L CNN
F 1 "MountingHole" H 7250 1255 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 7150 1300 50  0001 C CNN
F 3 "~" H 7150 1300 50  0001 C CNN
	1    7150 1300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5C8FECC3
P 7150 1500
F 0 "H3" H 7250 1546 50  0000 L CNN
F 1 "MountingHole" H 7250 1455 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 7150 1500 50  0001 C CNN
F 3 "~" H 7150 1500 50  0001 C CNN
	1    7150 1500
	1    0    0    -1  
$EndComp
Text GLabel 4050 2800 2    50   Input ~ 0
SCL
Text GLabel 4050 2700 2    50   Input ~ 0
SDA
$Comp
L Connector:Conn_01x02_Male I2C1
U 1 1 5CADCECB
P 4600 1250
F 0 "I2C1" H 4750 1050 50  0000 C CNN
F 1 "SCL, SDA" H 4400 1200 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4600 1250 50  0001 C CNN
F 3 "~" H 4600 1250 50  0001 C CNN
	1    4600 1250
	1    0    0    -1  
$EndComp
Text GLabel 4800 1250 2    50   Input ~ 0
SDA
Text GLabel 4050 2800 2    50   Input ~ 0
SCL
Text GLabel 4800 1350 2    50   Input ~ 0
SCL
$Comp
L Device:Rotary_Encoder_Switch ROT1
U 1 1 5CADE577
P 5100 1950
F 0 "ROT1" H 5100 2317 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 5100 2226 50  0000 C CNN
F 2 "SamacSys_Parts:PEC12R-2225F-S0024" H 4950 2110 50  0001 C CNN
F 3 "~" H 5100 2210 50  0001 C CNN
	1    5100 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C_reset1
U 1 1 5CADEADF
P 2150 1550
F 0 "C_reset1" V 2402 1550 50  0000 C CNN
F 1 "10uF" V 2311 1550 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 2150 1550 50  0001 C CNN
F 3 "~" H 2150 1550 50  0001 C CNN
	1    2150 1550
	0    -1   -1   0   
$EndComp
$Comp
L Analog_Switch:ADG419BN SW_reset1
U 1 1 5CAF02A3
P 1700 1550
F 0 "SW_reset1" H 1700 1882 50  0000 C CNN
F 1 "ADG419BN" H 1700 1791 50  0000 C CNN
F 2 "SamacSys_Parts:2AS1T2A1M6RES" H 1700 1250 50  0001 C CNN
F 3 "" H 1700 1350 50  0001 C CNN
F 4 "401-686" H 1700 1700 50  0000 C CNN "RS Stock no"
	1    1700 1550
	1    0    0    -1  
$EndComp
Text GLabel 1700 5500 3    50   Input ~ 0
GND
Text GLabel 2250 5250 3    50   Input ~ 0
0V
Text GLabel 3500 5350 3    50   Input ~ 0
0V
Text GLabel 3500 4750 3    50   Input ~ 0
0V
Text GLabel 4450 5150 0    50   Input ~ 0
0V
Text GLabel 6400 5150 2    50   Input ~ 0
0V
Text GLabel 6400 4700 2    50   Input ~ 0
0V
Text GLabel 8300 4950 2    50   Input ~ 0
0V
Text GLabel 5700 3600 3    50   Input ~ 0
0V
$Comp
L Analog_Switch:ADG419BN SW_GND1
U 1 1 5CAF3466
P 1700 2500
F 0 "SW_GND1" H 1700 2832 50  0000 C CNN
F 1 "ADG419BN" H 1700 2741 50  0000 C CNN
F 2 "SamacSys_Parts:2AS1T2A1M6RES" H 1700 2200 50  0001 C CNN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/ADG419.pdf" H 1700 2300 50  0001 C CNN
F 4 "401-686" H 1700 2650 50  0000 C CNN "RS Stock no"
	1    1700 2500
	1    0    0    -1  
$EndComp
Text GLabel 3550 3300 3    50   Input ~ 0
GND
Text GLabel 1400 2600 0    50   Input ~ 0
GND
Text GLabel 2000 2500 2    50   Input ~ 0
0V
Text GLabel 4050 1700 2    50   Input ~ 0
Reset
Text GLabel 1400 1650 0    50   Input ~ 0
Reset
Text GLabel 2300 1550 2    50   Input ~ 0
GND
Text GLabel 4800 1950 0    50   Input ~ 0
GND
Text GLabel 3050 1900 0    50   Input ~ 0
RotA
Text GLabel 3050 2000 0    50   Input ~ 0
RotB
Text GLabel 3050 2100 0    50   Input ~ 0
Button
Text GLabel 5400 1850 2    50   Input ~ 0
GND
Text GLabel 5400 2050 2    50   Input ~ 0
Button
Text GLabel 4800 1850 0    50   Input ~ 0
RotA
Text GLabel 4800 2050 0    50   Input ~ 0
RotB
$Comp
L Connector:Conn_01x02_Male GND,+5V1
U 1 1 5CB72BCD
P 4600 1050
F 0 "GND,+5V1" H 4706 1228 50  0000 C CNN
F 1 "Conn_01x02_Male" H 4700 1150 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4600 1050 50  0001 C CNN
F 3 "~" H 4600 1050 50  0001 C CNN
	1    4600 1050
	1    0    0    -1  
$EndComp
Text GLabel 4800 1050 2    50   Input ~ 0
GND
Text GLabel 9450 3350 2    50   Input ~ 0
CLK
Text GLabel 9450 3450 2    50   Input ~ 0
MISO
Text GLabel 9450 3550 2    50   Input ~ 0
MOSI
Text GLabel 7850 3550 0    50   Input ~ 0
GND
$Comp
L Connector:Conn_01x02_Male AnalogInput1
U 1 1 5CFEA937
P 7200 2850
F 0 "AnalogInput1" H 7306 3028 50  0000 L CNN
F 1 "Conn_01x02_Male" H 7306 2937 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 7200 2850 50  0001 C CNN
F 3 "~" H 7200 2850 50  0001 C CNN
	1    7200 2850
	1    0    0    -1  
$EndComp
Text GLabel 7850 3350 0    50   Input ~ 0
AnalogIN1
Text GLabel 7850 3450 0    50   Input ~ 0
AnalogIN2
Text GLabel 3050 3000 0    50   Input ~ 0
CLK
Text GLabel 3050 2900 0    50   Input ~ 0
MISO
Text GLabel 3050 2800 0    50   Input ~ 0
MOSI
Text GLabel 7400 2850 2    50   Input ~ 0
AnalogIN1
Text GLabel 7400 2950 2    50   Input ~ 0
AnalogIN2
Text GLabel 7850 3250 0    50   Input ~ 0
CS1
Text GLabel 3050 2500 0    50   Input ~ 0
CS1
$Comp
L DualPrecisionPWM-rescue:MCP3202-CI_P-SamacSys_Parts ADC1
U 1 1 5CFF901A
P 7850 3250
F 0 "ADC1" H 8650 3515 50  0000 C CNN
F 1 "MCP3202-CI_P" H 8650 3424 50  0000 C CNN
F 2 "SamacSys_Parts:DIP940W56P254L946H432Q8N" H 9300 3350 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/MCP3202-CI_P.pdf" H 9300 3250 50  0001 L CNN
F 4 "Microchip MCP3202-CI/P, 12 bit Serial ADC, Differential Input, 8-Pin PDIP" H 9300 3150 50  0001 L CNN "Description"
F 5 "4.32" H 9300 3050 50  0001 L CNN "Height"
F 6 "Microchip" H 9300 2950 50  0001 L CNN "Manufacturer_Name"
F 7 "MCP3202-CI/P" H 9300 2850 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "579-MCP3202-CI/P" H 9300 2750 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=579-MCP3202-CI%2FP" H 9300 2650 50  0001 L CNN "Mouser Price/Stock"
F 10 "1449085" H 9300 2550 50  0001 L CNN "RS Part Number"
F 11 "https://uk.rs-online.com/web/p/products/1449085" H 9300 2450 50  0001 L CNN "RS Price/Stock"
	1    7850 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9450 3250 9550 3250
Text GLabel 4800 1150 2    50   Input ~ 0
+5V
Text GLabel 3750 1300 1    50   Input ~ 0
+5V
$Comp
L Connector:Conn_01x02_Male AnalogOutput1
U 1 1 5D08E239
P 8100 4850
F 0 "AnalogOutput1" H 8000 5050 50  0000 L CNN
F 1 "Conn_01x02_Male" H 8206 4937 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 8100 4850 50  0001 C CNN
F 3 "~" H 8100 4850 50  0001 C CNN
	1    8100 4850
	1    0    0    -1  
$EndComp
Text GLabel 3450 1200 1    50   Input ~ 0
+12V_RealEarth
$Comp
L Connector:Conn_01x03_Male Conn_3x_Male_In2
U 1 1 5D09FD3D
P 6100 2100
F 0 "Conn_3x_Male_In2" H 6206 2378 50  0000 C CNN
F 1 "+12V, 0V, -12V" H 6200 2300 50  0000 C CNN
F 2 "Connector:FanPinHeader_1x03_P2.54mm_Vertical" H 6100 2100 50  0001 C CNN
F 3 "~" H 6100 2100 50  0001 C CNN
	1    6100 2100
	1    0    0    -1  
$EndComp
Text GLabel 6300 2000 2    50   Input ~ 0
+12V_Floating
Text GLabel 6300 2100 2    50   Input ~ 0
0V
Text GLabel 6300 2200 2    50   Input ~ 0
-12V_Floating
Text GLabel 4450 4850 0    50   Input ~ 0
+12V_Floating
Text GLabel 6450 4850 2    50   Input ~ 0
-12V_Floating
Wire Wire Line
	2750 3200 2750 4100
Wire Wire Line
	2400 3200 2750 3200
Text GLabel 2050 3750 3    50   Input ~ 0
0V
Text GLabel 1550 3200 0    50   Input ~ 0
+12V_Floating
Connection ~ 2050 3700
$Comp
L Regulator_Linear:L7805 L2
U 1 1 5D0C7AC8
P 8800 2150
F 0 "L2" H 8800 2392 50  0000 C CNN
F 1 "L7805" H 8800 2301 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 8825 2000 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 8800 2100 50  0001 C CNN
	1    8800 2150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C_LO2
U 1 1 5D0C7C4A
P 9200 2300
F 0 "C_LO2" H 9315 2346 50  0000 L CNN
F 1 "1uF" H 9315 2255 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 9238 2150 50  0001 C CNN
F 3 "~" H 9200 2300 50  0001 C CNN
	1    9200 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C_LI2
U 1 1 5D0C7E67
P 8200 2500
F 0 "C_LI2" H 8315 2546 50  0000 L CNN
F 1 "1uF" H 8315 2455 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 8238 2350 50  0001 C CNN
F 3 "~" H 8200 2500 50  0001 C CNN
	1    8200 2500
	1    0    0    -1  
$EndComp
Text GLabel 8150 2150 0    50   Input ~ 0
+12V_RealEarth
Text GLabel 8800 2700 3    50   Input ~ 0
GND
Wire Wire Line
	8150 2150 8200 2150
Wire Wire Line
	8200 2350 8200 2150
Connection ~ 8200 2150
Wire Wire Line
	8200 2150 8500 2150
Wire Wire Line
	8200 2650 8800 2650
Wire Wire Line
	8800 2650 8800 2450
Wire Wire Line
	8800 2650 8800 2700
Connection ~ 8800 2650
Wire Wire Line
	8800 2450 9200 2450
Connection ~ 8800 2450
Wire Wire Line
	9100 2150 9200 2150
Wire Wire Line
	9550 3250 9550 2150
Wire Wire Line
	9550 2150 9200 2150
Connection ~ 9200 2150
$Comp
L Device:C C_Arduino1
U 1 1 5D0FFC2C
P 2850 1450
F 0 "C_Arduino1" H 2450 1700 50  0000 L CNN
F 1 "1uF" H 2600 1600 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 2888 1300 50  0001 C CNN
F 3 "~" H 2850 1450 50  0001 C CNN
	1    2850 1450
	1    0    0    -1  
$EndComp
Text GLabel 2850 1600 3    50   Input ~ 0
GND
Wire Wire Line
	3450 1300 3450 1250
Wire Wire Line
	3450 1250 2850 1250
Wire Wire Line
	2850 1250 2850 1300
Connection ~ 3450 1250
Wire Wire Line
	3450 1250 3450 1200
Text Notes 1200 4200 0    50   ~ 0
Fine control, \n80uV resolution
Text Notes 1050 4650 0    50   ~ 0
Coarse Control,\n20mV resolution
Text Notes 4400 4800 0    50   ~ 0
Summing Amplifiers
Text Notes 5650 4800 0    50   ~ 0
Buffer Amplifiers
Text Notes 8100 3800 0    50   ~ 0
Analog-to-Digital-Converter
Text Notes 2300 5150 0    50   ~ 0
Optocoupler
Text Notes 1750 1100 0    0    ~ 0
Reset Capacitor for Arduino
Text Notes 1600 1050 0    0    ~ 0
Reset capacoitor for arduino
Text Notes 6650 3200 0    0    ~ 0
Switch to connect\nreal earth &\nfloating reference
$EndSCHEMATC
