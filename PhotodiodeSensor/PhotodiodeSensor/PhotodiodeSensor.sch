EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:CP1 C1
U 1 1 60142D10
P 3550 3250
F 0 "C1" H 3665 3296 50  0000 L CNN
F 1 "1uF" H 3665 3205 50  0000 L CNN
F 2 "" H 3550 3250 50  0001 C CNN
F 3 "~" H 3550 3250 50  0001 C CNN
	1    3550 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C2
U 1 1 60142FC8
P 4550 3250
F 0 "C2" H 4665 3296 50  0000 L CNN
F 1 "100nF" H 4665 3205 50  0000 L CNN
F 2 "" H 4550 3250 50  0001 C CNN
F 3 "~" H 4550 3250 50  0001 C CNN
	1    4550 3250
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:KA78M05_TO252 U1
U 1 1 6014344F
P 4050 2750
F 0 "U1" H 4050 2992 50  0000 C CNN
F 1 "KA7809" H 4050 2901 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 4050 2975 50  0001 C CIN
F 3 "http://www.fairchildsemi.com/ds/LM/LM78M05.pdf" H 4050 2700 50  0001 C CNN
	1    4050 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R1
U 1 1 60144290
P 5550 3400
F 0 "R1" H 5618 3446 50  0000 L CNN
F 1 "1k" H 5618 3355 50  0000 L CNN
F 2 "" V 5590 3390 50  0001 C CNN
F 3 "~" H 5550 3400 50  0001 C CNN
	1    5550 3400
	1    0    0    -1  
$EndComp
Text GLabel 3550 3800 3    50   Input ~ 0
0V
Text GLabel 3400 2750 0    50   Input ~ 0
+12V
Wire Wire Line
	3400 2750 3550 2750
Wire Wire Line
	3550 2750 3550 3100
Connection ~ 3550 2750
Wire Wire Line
	3550 2750 3750 2750
Wire Wire Line
	3550 3400 3550 3700
Wire Wire Line
	4350 2750 4550 2750
Wire Wire Line
	4550 2750 4550 3100
Wire Wire Line
	4550 3400 4550 3700
Wire Wire Line
	4550 3700 4050 3700
Connection ~ 3550 3700
Wire Wire Line
	3550 3700 3550 3800
Wire Wire Line
	5550 3050 5550 3200
Wire Wire Line
	5550 3550 5550 3700
Wire Wire Line
	5550 3700 4550 3700
Connection ~ 4550 3700
$Comp
L Device:D_Photo D1
U 1 1 60143A88
P 6100 2950
F 0 "D1" V 6004 3108 50  0000 L CNN
F 1 "OSD15-5T" V 6095 3108 50  0000 L CNN
F 2 "" H 6050 2950 50  0001 C CNN
F 3 "~" H 6050 2950 50  0001 C CNN
	1    6100 2950
	0    1    1    0   
$EndComp
Connection ~ 4550 2750
Connection ~ 5550 3200
Wire Wire Line
	5550 3200 5550 3250
Wire Wire Line
	4550 2750 6100 2750
Wire Wire Line
	5550 3050 6100 3050
Text GLabel 6200 3200 2    50   Input ~ 0
Vout
Wire Wire Line
	5550 3200 6200 3200
Wire Notes Line
	5800 2650 5800 3100
Wire Notes Line
	5800 3100 6750 3100
Wire Notes Line
	6750 3100 6750 2650
Wire Notes Line
	6750 2650 5800 2650
Text Notes 6800 2850 0    50   ~ 0
Inside the \nspectrometer
Text Notes 5350 2700 0    50   ~ 0
---> I_d
Text Notes 6450 3250 0    50   ~ 0
Vout = 1k x I_d
Text Notes 2900 2650 0    50   ~ 0
From power pack
Text Notes 6450 3450 0    50   ~ 0
Measurement taken\nw.r.t 0V
Wire Wire Line
	4050 3050 4050 3700
Connection ~ 4050 3700
Wire Wire Line
	4050 3700 3550 3700
$EndSCHEMATC
