"""
This is a python script to read in a .json save file from the (e,2e) and output a summary csv
Date crated: 15/10/2021
Author: Manish Patel
"""

import csv
from itertools import zip_longest
import json
import sys

#-----------------------------------------HELPER FUNCTIONS----------------------------------------
def openSaveFile(filePath, header = None):
    """Opens a json file and returns the containing data.

    Parameters:
        filepath - string name of the file.
        header - string of local path of file on device.
    Returns:
        dictionary of data contained in the JSON file
    """
    if header != None:
        filePath = filePath / (header + '.json')
    with open(filePath, encoding = 'utf-8-sig') as f:
        data = json.load(f)
    return data


def fromNestedDict(data, fields):
    """Recursive return from dictionary.

    Feed it a dictionary (JSON) and a list of keystrings and it will return the value.

    Parameters:
        data - dictionary of the data
        fields - list of strings listing the headings of the wanted data
    Returns:
        list of data
    Examples:
        energies = fromNestedDict(data, ['Summary', 'Gun', 'Energies'])
    """
    firstDict = fields[0]
    remainingDicts = fields[1:]

    if remainingDicts:
        return fromNestedDict(data[firstDict], remainingDicts)
    else:
        return data[firstDict]
    
def writeCSV(filename, headers, data):
    """Creates a csv file using pandas dataframe.
    Parameters: 
        headers - array of strings used as column headers
        data - array of arrays of data used for the columns, must be same size as headers
        filename - string of the name of the csv file output
    """
    # Check for errors
    if len(headers) != len(data):
        print("Must have the same number of headers as data columns!")
        return
    # Create csv by using zip_longest
    with open(filename, 'w') as f:
        writer = csv.writer(f)
        writer.writerow(headers)
        rows = zip_longest(*data, fillvalue = '')
        for row in rows:
            writer.writerow(row)
            
def formatDateTime(input_datetime):
    """ Formats the date and time in the (e,2e) data files into the correct format for Plotly.
    
    The correct datetime format is "yyyy-mm-dd hh:mm:ss" in string format.
    
    Parameters:
        input_datetime (str): Datetime in datafile in format yymmdd_hh
        
    Returns:
        str: A datetime equivalent to the `input_datetime` in the format "yyyy-mm-dd hh:mm:ss".
    """
    year = "20"+input_datetime[:2]
    month = input_datetime[2:4]
    day = input_datetime[4:6]
    hour = input_datetime[7:9]
    minute = input_datetime[9:11]
    output_datetime = year+"-"+month+"-"+day+" "+hour+":"+minute
    return output_datetime
            
#----------------------------------SAVE TO CSV---------------------------------
def saveSummaryCSV(filename):
    """Save json data to a csv
    
    Parameters: 
        filename (str): Name of the file to be analysed and plotted in .json format
    """
    # Open datafile and read in data
    data = openSaveFile(filename)
    # Coincidence Counts and ratios
    counts = fromNestedDict(data, ["Summary", "CountArray", 0])
    # Gun Energies
    # Different gun energies for different file types
    experiment_type = fromNestedDict(data, ['Summary', 'ExperimentType'])
    # Gun energy scan
    if(experiment_type == "Gun Energy Scan"):
        gun_fine_energies = fromNestedDict(data, ['Summary', 'Gun', 'Energies'])
        gun_course_energy = fromNestedDict(data, ['Runs', 0, 'Settings', 'Gun', 'Set', 'GUNC'])
        gun_energies = [e+gun_course_energy for e in gun_fine_energies]
    else: # For Angular Scans and Tracking Energy Scans
        gun_energies = fromNestedDict(data, ['Summary', 'Gun', 'Energies'])
    # Angles
    a1_angles = fromNestedDict(data, ['Summary', 'A1', 'Angles'])
    a2_angles = fromNestedDict(data, ['Summary', 'A2', 'Angles'])
    hwp_angles = fromNestedDict(data, ['Summary', 'HWP Angles (°)'])
    # Residula energies
    a1_rse = fromNestedDict(data, ['Summary', 'A1', 'RSE'])
    a2_rse = fromNestedDict(data, ['Summary', 'A2', 'RSE'])
    # Step Time List
    step_times = fromNestedDict(data, ['Summary', 'Step Time List (s)'])
    # Cup Current and pressure
    # Make arrays to put data in
    cup_current = []
    pressure = []
    datetime = []
    a1_counts = []
    a1_std_dev = []
    a2_counts = []
    a2_std_dev = []
    # Open the datafile
    data = openSaveFile(filename)
    # Read in the data
    run_number = [i+1 for i in range(len(fromNestedDict(data, ['Runs'])))]
    for i in range(len(run_number)):
        cup_current.append(fromNestedDict(data, ['Runs', i, 'FCupCurrent (µA)']))
        pressure.append(fromNestedDict(data, ['Runs', i, 'Pressure (Torr)']))
        datetime.append(formatDateTime(fromNestedDict(data, ['Runs', i, 'StartTime'])))
        a1_counts.append(fromNestedDict(data, ['Runs', i, 'CountRates', 'A1']))
        a1_std_dev.append(fromNestedDict(data, ['Runs', i, 'CountRatesStDev', 'A1']))
        a2_std_dev.append(fromNestedDict(data, ['Runs', i, 'CountRatesStDev', 'A2']))
        a2_counts.append(fromNestedDict(data, ['Runs', i, 'CountRates', 'A2']))

    
    # Save to csv
    headers = ["Run Number", "StartDateTime", "Counts",
                "GunEnergy(eV)", "Pressure(Torr)", "FaradayCupCurrent(uA)",
                "A1Angle(deg)", "A2 Angle(deg)", "A1 Counts", "A1 Std Dev",
                "A2 Counts", "A2 Std Dev", "A1ResidualEnergy(eV)",
                "A2ResidualEnergy(eV)", "StepTime"]
    write_data = [run_number, datetime, counts,
                 gun_energies, pressure, cup_current,
                 a1_angles, a2_angles, a1_counts,
                 a1_std_dev, a2_counts, a2_std_dev,
                 a1_rse, a2_rse, step_times]
    csv_filename = csv_filename = filename[0:-5]+".Summary.csv"
    writeCSV(csv_filename, headers, write_data)

#-------------------------------CALL FROM COMMAND LINE-----------------------------
if __name__ == "__main__":
    """In the command line can call this file and the .json filename to output a summary csv.
    
    Example:
        python saveSummaryCSV.py "He&Xe_GES_211013_1013.json"
    """
    filename = str(sys.argv[1]) # The 1st argument is the filename
    saveSummaryCSV(filename)