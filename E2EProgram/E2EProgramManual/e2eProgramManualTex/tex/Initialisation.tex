\chapter{Initialisation}

The main program's structure is based on a flat sequence structure \cite{labVIEWSequenceStruct}. All subvis and operations are within this sequence. The first four sequences runs at the start up of the program and initialises the program. The next sequence runs all the loops. The last sequence closes the program safely upon the user pressing the exit button.  

\section{References}

To interface with all the front panel controls and indicators references are used into the subvi called SetGetReferences. This subvi is use to hold all of these references and update or get information from any of them. These controls need to be passed by reference  in a cluster of \href{https://zone.ni.com/reference/en-XX/help/371361R-01/glang/control_reference/}{VI Server References} \cite{labVIEWServerRef}. Upon initialisation all of these references need to be set as shown in figure \ref{fig:SetGetReferences}. To add to the list of references:
\begin{enumerate}
     \item Add the reference to the list
    \item Create a Cluster Indicator
    \item Cut it and paste it into the SetGetReferences subvi
    \item Copy the new reference into the subvi's control and indicator
\end{enumerate}

\begin{figure}[H]
    \centering
    \includegraphics[scale = 0.5]{img/SetGetReferences.PNG}
    \caption{SetGetReferences subvi}
    \label{fig:SetGetReferences}
\end{figure}

\section{Functional Global Variables}

Once the reference setter/getter is initialised the sequence is incremented to the next section where initialisation of hardware and saved settings are applied. 

The functionality of subvis are not limited to one function. A \href{https://learn-cf.ni.com/teach/riodevguide/code/rt_functional-global-variable.html}{Functional Global Variable} (FGV) is a subVI which has different properties from a local or global variable \cite{labVIEWFGV}. The FGV has multiple functions or states which are set by an enum variable with a \href{https://zone.ni.com/reference/en-XX/help/371361R-01/glang/case_structure/}{case structure} \cite{labVIEWCaseStruct}. FGVs contain a \href{http://www.ni.com/getting-started/labview-basics/shift-registers}{shift register} \cite{labVIEWShiftReg} in a while loop to store data. \textbf{N.B. An FGV must be set to "non-reetrant" in "VI Properties"}. This can be achieved by pressing Ctrl+I to get into VI properties, select Execution category, and choose "non-reetrant execution". The other two execution modes create copies of the subvi in memory if called at runtime. This copy of the FGV is in an indeterminate state compared to the original subVI as it has not been initialised or run before.

\begin{figure}[H]
    \centering
    \includegraphics[scale = 1.2]{img/SaveLoadSettings.PNG}
    \caption{FGV SaveLoadSettings block.}
    \label{fig:SaveLoadSettings}
\end{figure}

The FGVSaveLoadSettings is an example of a Functional Gloabal Variable. This is used in the second sequence in the main program to load or save settings as shown in Figure \ref{fig:SaveLoadSettings}. This has four states which are controlled by passing in enums:
\begin{itemize}
    \item \textbf{Load File}. The JSON file which the user inputs is opened and read from. The \href{https://www.vipm.io/package/jki_lib_json_serialization/}{JKI JSON library} is used to read data from a JSON file \cite{JKIjson}. The "Unflatten From JSON String" subVI is used from the JKI JSON library to convert a JSON string to a \href{https://zone.ni.com/reference/en-XX/help/371361R-01/lvhowto/variants/}{Variant} type \cite{variant}. A cluster containing the file format of the JSON, called SettingsSaveFormat, is used as an input to the subVI to parse all data correctly. The file is then closed and the Settings cluster is output with all the data extracted from the JSON file. 
    \item \textbf{Save As}. This state creates a file and uses the "Flatten to JSON String" VI in the JKI JSON library to write the variant data into a file. The save settings are retrieved from the GetSaveSettings subVI. This subVI uses the SetGetReferences subVI to retrieve all data wanted for the settings and dereference them with a \href{http://zone.ni.com/reference/en-XX/help/371361R-01/glang/property_node/}{property node} \cite{PropertyNode}. 
    \item \textbf{Load Last Session}. The file called "LastSessionSettings.json" is located in the folder "Settings" in the local path of MainProgram.vi and the settings are loaded using this file.
    \item \textbf{Save Last Session}. All settings are saved to "LastSessionSettings.json"
\end{itemize}

Once the Save Settings cluster has been filled with all data from the save file then all values in the cluster are written to their respective references in the SVI\_UpdateSettingsValues.

\section{Hardware \& Front Panel Controls}

Figure \ref{fig:InitHardware} shows the initialisation of the state machine which is used to set up a state queue for the execution of multiple FGVs wanting to execute at the same time. The FGVLoopRunner is also initialised in this sequence. These will be explained in further sections. 

The optimisation indicators for the analysers are turned off. This is so that upon start up the program knows what state they are in.

The MCA and analyser rate counters on the DAQ card are initialised using FGVs in this sequence as well using DAQmx: FGV\_MCA and FGV\_CountRates. In both initialisations tasks are created, channels are created, and tasks are output to a shift register so that the FGV can interface with that task again. 

\begin{figure}[H]
    \centering
    \includegraphics[scale = 1]{img/InitHardware.PNG}
    \includegraphics[scale = 1]{img/ConnectArduinos.PNG}
    \caption{Initialise Hardware \& Connect Arduinos}
    \label{fig:InitHardware}
\end{figure}

\section{Connecting to Devices}

In the next sequence, the Arduinos are connected to using the FGVArduinoCommsHandler. This is shown on the right-hand side of figure \ref{fig:InitHardware}. This FGV connects to Arduin-controlled hardware using their application programming interface (API) via each hardware's specific FGV e.g. the FGVAnalyserControl for controlling the analysers. 

When designing an API for a new piece of hardware there must be an "ID" for the device: if the LabVIEW program sends "ID" over serial to the Arduino (or microcontroller) then there must be a response with a unique two-letter code for example the electron energy analyser's power supply's ID is "AN". This ID is used to find, identify and connect the Arduinos to the main program so that serial messages and actions get sent to the corresponding device. The SVI\_FindCommPort finds all the communication ports attached to the PC and checks if there is a response when sending "ID" (or "\$ID\&" - this is due to a legacy API requiring "\$" at the start of a serial command and "\&" at the end of a serial command). If the response corresponds to the correct ID then the device is connected to. N.B. the end character for the current API is a newline ("\textbackslash n").

\section{Unique Spectrometer Hardware}

Not all electron spectrometers under the control system have the same hardware and APIs to interface with. The Old (e,2e) and New (e,2e) spectrometers have different stepper controllers to move the electron energy analysers. Hardware capabilities for each spectrometer will change in the future and may not be the same. Using a Case Structure with an Apparatus enum allows for different hardware to be used in the same program.

In figure \ref{fig:StepperControl} the Old (e,2e) and New (e,2e) initialise the stepper controllers differently using this enum. Controls and Front Panel buttons can be hidden or not available (grayed out) when using a specific spectrometer. 

\begin{figure}[H]
    \centering
    \includegraphics[scale = 1]{img/InitStepper.PNG}
    \caption{Initialising hardware unique to the spectrometer.}
    \label{fig:StepperControl}
\end{figure}

Lastly, all graphs are initialised and what can be seen in each tab of the main program is set by the Tab Control enum. The Dual Axis control is set in initialisation as well. 