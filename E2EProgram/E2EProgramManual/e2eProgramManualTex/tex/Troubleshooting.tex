\chapter{Troubleshooting}

Below are some common issues are explained when the main program crashes.

\section{Device not found}

If the main program does not start and an error message says that a device cannot be found then a piece of critical hardware is not communicating correctly with the program. 

This could be due to a device not plugged into the control PC. You must plug in all devices over USB so that the program can communicate via serial to these devices, find their ID, and control them. If all devices are plugged in then an error may have occurred with the serial and Windows can disconnect the serial port. 

To fix this error we need to identify which of the connected devices is not communicating first. To do this exit the program by pressing the "Exit" button and follow the steps outlined below.

1. Go to the back panel of the main program. This can be done by double-clicking on an element or pressing "Ctrl+E". Then, identify where the "FGVArduinoCommsHandler" VI is. This should be at the start of the program after declaration of the references as seen in figure \ref{fig:ArduinCommsHandler}. 

\begin{figure}[H]
    \centering
    \includegraphics[scale = 0.4]{img/FGVArduinoCommsHandler.PNG}
    \caption{The back panel of the main program showing the "FGVArduinoCommsHandler" VI as highlighted.}
    \label{fig:ArduinCommsHandler}
\end{figure}

2. Then, double click on this VI to bring up its back panel as shown in figure \ref{fig:ArduinCommsHandlerBackPanel}. Double click on any of the FGVs with the "Find \& Connect" command wired to them.

\begin{figure}[H]
    \centering
    \includegraphics[scale = 0.45]{img/ArduinoCommsHandlerBackPanel.PNG}
    \caption{The back panel of the "FGVArduinoCommsHandler" VI showing the FGVs which control various devices.}
    \label{fig:ArduinCommsHandlerBackPanel}
\end{figure}

3. Then, navigate to the back panel of the FGV just opened. There should be an "SVIFindCommPort" VI if under the "Find COM Port" tab of the case structure as shown in figure \ref{fig:SVIFindCommPort}. 

\begin{figure}[H]
    \centering
    \includegraphics[scale = 0.5]{img/SVIFindCommPort.PNG}
    \caption{The back panel of the "FGVAnalyserCtrl" VI showing the "SVIFindCommPort" VI.}
    \label{fig:SVIFindCommPort}
\end{figure}

4. Double click on this VI to bring up the back panel as shown in figure \ref{fig:SVIFindCommPortBackPanel}. Type into the "ID Request to Send" box on the left-hand side of this VI any character - let's say a "." or "," character thenset the "Checksum Type" to either "ReadableChars" for the New (e, 2e) spectrometer or "Spellman MPS" for the Old (e, 2e). Then, run the VI. 

\begin{figure}[H]
    \centering
    \includegraphics[scale = 0.5]{img/SVICommPortBackPanel.PNG}
    \caption{The back panel of the "SVIFindCommPort" VI. The boxes highlighted are the "ID Request to Send" box containing a "." character on the left and the "Checksum Type" box on the right.}
    \label{fig:SVIFindCommPortBackPanel}
\end{figure}

Responses should come up in the "Response" box containing the "ID"s of the devices which have a serial connection to the PC. A table of the device IDs and the corresponding device can be found in table \ref{table:IDTable}. If a device is not found by this VI and it is connected to the PC via USB then this COM port needs resetting and the device needs to be unplugged.

\begin{table}[H]
\begin{tabular}{lll}
\hline
ID & Device Name & Spectrometer \\ \hline
\multicolumn{1}{c}{A1} & Analyser 1 & Old (e, 2e) \\
\multicolumn{1}{c}{A2} & Analyser 2 & Old (e, 2e) \\
\multicolumn{1}{c}{EG} & Electron gun & Old (e, 2e) \\
\multicolumn{1}{c}{HWP} & Half-waveplate rotator & New (e, 2e) \\
AN & Analysers & New (e, 2e) \\
FC & Faraday cup current detector & New (e, 2e) \\
SC & Stepper controller & New \& Old (e, 2e)
\end{tabular}
\caption{The devices and corresponding IDs of the electron spectrometers at the current time this manual was written.}
\label{table:IDTable}
\end{table}

5. To reset that COM port first identify the not-connecting device's USB connection and unplug it and plug it into a different serial port (if you have to unplug a different device and switch the serial ports then this is fine). Devices should have their USB cables labelled with the device ID. 
\textbf{Note:} if not, then please find the device and label its USB port with its ID with a label-maker. 

Then, restart the PC and when booted up again start the main program and repeat steps 1-4. If the device is still not found then restart the PC and check in Devices \& Printers that the COM port that the device is connected to can be seen. If this problem still persists check to see if the device is not faulty by communicating to it over serial from another program (e.g. using the Arduino IDE Serial Monitor) and using the command "ID" with the correct checksum type. 

\textbf{Note:} Very rarely, one of the devices will switch checksum types so send the serial command with "ID"+checksum, where checksum is the checksum of "ID" with the SpellmanMPS or ReadableChars checksum types. To find the checksum go into the "SVI\_FindCommPort" VI and double click the "SVI\_Calculate\_checksum" VI to calculate the checksum for "ID" for the different checksum types. If the checksum for a device connected to the New (e, 2e) switches to Spellman MPS type then switch it to ReadableChars.
