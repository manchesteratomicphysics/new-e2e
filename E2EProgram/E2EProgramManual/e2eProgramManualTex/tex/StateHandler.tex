\chapter{State Handler}

The main program must execute lots of tasks consecutively. These tasks are in the form of while loops and if left to run in the main block diagram without sequencing the while loops would give race conditions where different tasks are executed in the wrong order. There must be a queue of tasks which is populated by user actions and orderly executed and removed from the list of tasks. This is encapsulated within the main program in the FGV StateQueue. The StateQueue FGV can:
\begin{itemize}
    \item \textbf{Init}: Initialises the state queue with a signel state. This is initailised in figure \ref{fig:InitHardware} with the Running MCA task. 
    \item \textbf{Add 1}: A single state is added to the end of the state queue.
    \item \textbf{Add N}: A number of states is added to the end of the state queue.
    \item \textbf{Insert 1}: A state is added at a set position in the state queue.
    \item \textbf{Insert N}: N states are added starting at a set position in the state queue.
    \item \textbf{Pop}: Gets the next state in the queue and deletes it from the array of states.
    \item \textbf{Peek}: Get the next state in the queue.
\end{itemize}
With these commands tasks can be added to the queue and lined up to be executed. For example, in figure \ref{fig:StateHandlerInputs}, the Pop function is used with a Case Structure to execute the list of tasks sequentially e.g. an electron energy scan is performed, then the data is saved, and the state goes back to running the MCA in Idle mode. With the State Queue FGV the iteration of the queue can be accessed as well and can be used in loops.

In figure \ref{fig:StateHandlerInputs} the Laser Status shared variable can be seen. This is used in conjuction with a user button to set the experiment to pause if the laser is not locked. If the laser goes out of lock then the MCA stops collecting data and the experiment only resumes once the laser is back on lock. 

\begin{figure}[H]
    \centering
    \includegraphics[scale = 0.6]{img/StateHandlerInputs.PNG}
    \caption{State Handler loop inputs. The State shown is in "Running MCA" in the Case Structure.}
    \label{fig:StateHandlerInputs}
\end{figure}

In the Running MCA Idle state the user-input ROI Settings for the MCA (Signal Min, Signal Max, Background Min, Background Max) are used to calculate the coincidence counts and signal-to-noise ratio using the RealTimeCoincidenceMCACALC subVI as shown in figure \ref{fig:IdleMCA}. In this same loop the faraday cup and pressure readings are read from the DAQ card and displayed on the front panel. These readings are performed when the MCA is idle as the DAQ card can only make analog or digital readings at once so the MCA cannot be read whilst analog readings of pressure are obtained. The Running MCA state is added to the end of the state queue in this loop as well. 

The MCA can be in different modes which is encapsulated by a Case Structure: Idle, Electron Gun Energy Scan, Analyser Energy Scan, Tracking Energy Scan, Angular Scan, HWP Scan. In these different modes the MCA can measured as required. 

\begin{figure}[H]
    \centering
    \includegraphics[scale = 0.6]{img/IdleRunningMCA.PNG}
    \caption{Running the MCA in Idle mode.}
    \label{fig:IdleMCA}
\end{figure}

Saving data is a task which is controlled by the state handler. The inputs to the Case Structures used for saving the data for various modes is displayed in figure \ref{fig:SavingDataInputs}. For each scan step the data is saved in a \href{https://en.wikipedia.org/wiki/JSON}{JSON} file format \cite{JSONWiki}. 

\begin{figure}[H]
    \centering
    \includegraphics[scale = 0.7]{img/SavingDataInputs.PNG}
    \caption{Saving data inputs.}
    \label{fig:SavingDataInputs}
\end{figure}

The saving data Case Structures for saving an Electron Gun Energy Scan is shown in figure \ref{fig:SavingData}. In this figure the energy scan is counting the Analyser Rates. The Current Energy (eV) and Mean Count Rates local variables are appended to the front panel graph using the EnergyGraph FGV. With this FGV the graph can also be reset and read. The Mean Count Rates are also added to the Save Summary. The summary is a block of data at the start of the JSON save file which gives a brief overview of the experimental scan. This includes the count or coincidence rates array for that run, scan variable (e.g. energies of the electron gun scanned over), electron energy analyser settings, electron gun settings, and laser locking information. 

The SaveJSONHandler FGV is used to load, save, and handle any file operations. The FGV uses the JKI JSON library. This library uses variant data types and obfuscates the creation of JSON formats from strings. The save format is a it's own datatype called SaveFormatV2. It is a cluster of elements. To add data to the save file the SaveFormatV2 datatype must be edited to include the name of the data's reference and its data type in the cluster of elements. 

Once the experiment is finished the MCA is paused, data is saved, and the MCA is reset. If the user has turned on the Post-Process JSON file in the front panel's Settings tab the PrettifyJSON\_Caller subVI is run in the SaveRunData subVI. This VI calls the PrettifyJSON.py python file from the windows terminal. Python 3 or higher must be on the windows command path for this to execute. The post-processing changes the JSON file to be in an easy-to-read and more compact form - data is unchanged. Also, in this Case Structure the Scan button is stopped and some front panel controls are enabled again. 

\begin{figure}[H]
    \centering
    \includegraphics[scale = 0.6]{img/SavingData.PNG}
    \caption{Saving data Case Structures.}
    \label{fig:SavingData}
\end{figure}

When a scan is being conducted and parameters need to be changed the MCA should not be read during this time. For example, figure \ref{fig:SetGunFine} shows the setting of Gun Fine during an electron gun energy scan. The Gun Energies array is read and the Current Step iterator is used to access the array at the correct position for the step in the scan. The value of the desired voltage to be set is used as the input to the UpdateGunFineDAQAO VI where the Analog Out on the NI DAQ card is set to that desired voltage. The loop then waits for 500 ms before continuing with the next step in the state handler. This gives time for the DAQ card and power supply to settle to the set voltage when changed. 

\begin{figure}[H]
    \centering
    \includegraphics[scale = 0.45]{img/SetGunFine.PNG}
    \caption{Set Gun Fine}
    \label{fig:SetGunFine}
\end{figure}
