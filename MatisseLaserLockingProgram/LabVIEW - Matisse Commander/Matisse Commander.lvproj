﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="14008000">
	<Property Name="CCSymbols" Type="Str">WAVEMETER,HIGHFINESSE;</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Property Name="varPersistentID:{2024775D-76DE-4324-9BEA-672645BF524A}" Type="Ref">/My Computer/Untitled Library 3.lvlib/TestVariable1</Property>
	<Property Name="varPersistentID:{7EC94F7E-C92C-442D-A29B-447A2DF64AFE}" Type="Ref">/My Computer/LaserNetworkVariables.lvlib/LaserInfo</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Plug-ins" Type="Folder">
			<Item Name="MCP HighFinesse.llb" Type="Document" URL="../Dependent/Plug-Ins/MCP HighFinesse.llb"/>
			<Item Name="MCP Wavemeter.llb" Type="Document" URL="../Dependent/Plug-Ins/MCP Wavemeter.llb"/>
			<Item Name="MCP External PID.llb" Type="Document" URL="../Dependent/Plug-Ins/MCP External PID.llb"/>
			<Item Name="MCP Mousewheel.llb" Type="Document" URL="../Dependent/Plug-Ins/MCP Mousewheel.llb"/>
			<Item Name="MCP LocalGoTo Plug-In.llb" Type="Document" URL="../Dependent/Plug-Ins/MCP LocalGoTo Plug-In.llb"/>
			<Item Name="MCP Strain Gauge PID.llb" Type="Document" URL="../Dependent/Plug-Ins/MCP Strain Gauge PID.llb"/>
			<Item Name="MCP WM Network.llb" Type="Document" URL="../Dependent/Plug-Ins/MCP WM Network.llb"/>
		</Item>
		<Item Name="Manuals" Type="Folder" URL="../Dependent/Manuals">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="Addons" Type="Folder"/>
		<Item Name="MC Splash Screen.vi" Type="VI" URL="../Source/MC Splash Screen.vi"/>
		<Item Name="Matisse Commander.ini" Type="Document" URL="../Source/Matisse Commander.ini"/>
		<Item Name="Matisse Commander Help.chm" Type="Document" URL="../Matisse Commander Help.chm"/>
		<Item Name="Sirah MC.ico" Type="Document" URL="../Source/Sirah MC.ico"/>
		<Item Name="MC Main Always include.vi" Type="VI" URL="../Source/Misc/MC Main Always include.vi"/>
		<Item Name="Read ASCII Hex.vi" Type="VI" URL="../Source/Updater/Read ASCII Hex.vi"/>
		<Item Name="Bifi Range.ctl" Type="VI" URL="../Source/BiFi/Bifi Range.ctl"/>
		<Item Name="MC Set Cursor After New.vi" Type="VI" URL="../Source/BiFi/Calibration Table Editor/MC Set Cursor After New.vi"/>
		<Item Name="MC Dialog Calibration Wavemeter.vi" Type="VI" URL="../Source/BiFi/Calibration Table Editor/MC Dialog Calibration Wavemeter.vi"/>
		<Item Name="SVI_StabiliseLaser.vi" Type="VI" URL="../SVI_StabiliseLaser.vi"/>
		<Item Name="TypeDef_StabilisationControls.ctl" Type="VI" URL="../TypeDef_StabilisationControls.ctl"/>
		<Item Name="TypeDef_SlowPiezoPIControls.ctl" Type="VI" URL="../TypeDef_SlowPiezoPIControls.ctl"/>
		<Item Name="SVI_AutoBiFiScan.vi" Type="VI" URL="../SVI_AutoBiFiScan.vi"/>
		<Item Name="SVI_FindBestMotorPosition.vi" Type="VI" URL="../SVI_FindBestMotorPosition.vi"/>
		<Item Name="FGV_BiFiGraph.vi" Type="VI" URL="../FGV_BiFiGraph.vi"/>
		<Item Name="SVI_AutoThinEtalonScan.vi" Type="VI" URL="../SVI_AutoThinEtalonScan.vi"/>
		<Item Name="FGV_ThinEtalonGraph.vi" Type="VI" URL="../FGV_ThinEtalonGraph.vi"/>
		<Item Name="SVI_FindClosestThinEtalonPeaksTroughs.vi" Type="VI" URL="../SVI_FindClosestThinEtalonPeaksTroughs.vi"/>
		<Item Name="SVI_ReverseGraphDataOrder.vi" Type="VI" URL="../SVI_ReverseGraphDataOrder.vi"/>
		<Item Name="SVI_Find_nearest_in_array.vi" Type="VI" URL="../SVI_Find_nearest_in_array.vi"/>
		<Item Name="SVI_ResetSlowPiezoAndPiezoEtalon.vi" Type="VI" URL="../SVI_ResetSlowPiezoAndPiezoEtalon.vi"/>
		<Item Name="SVI_LockAndTunePiezoEtalon2.vi" Type="VI" URL="../SVI_LockAndTunePiezoEtalon2.vi"/>
		<Item Name="SVI_StabiliseLaserI.vi" Type="VI" URL="../SVI_StabiliseLaserI.vi"/>
		<Item Name="TypeDef_ControlStateEnum.ctl" Type="VI" URL="../TypeDef_ControlStateEnum.ctl"/>
		<Item Name="TypeDef_GetSetState.ctl" Type="VI" URL="../TypeDef_GetSetState.ctl"/>
		<Item Name="FGV_GetSetState.vi" Type="VI" URL="../FGV_GetSetState.vi"/>
		<Item Name="FGV_SetUserMode.vi" Type="VI" URL="../FGV_SetUserMode.vi"/>
		<Item Name="TypeDef_UserMode.ctl" Type="VI" URL="../TypeDef_UserMode.ctl"/>
		<Item Name="Typedef_FGVControl.ctl" Type="VI" URL="../Typedef_FGVControl.ctl"/>
		<Item Name="FGV_BiFiSettings.vi" Type="VI" URL="../FGV_BiFiSettings.vi"/>
		<Item Name="FGV_ThinEtalonSettings.vi" Type="VI" URL="../FGV_ThinEtalonSettings.vi"/>
		<Item Name="TypeDef_PiezoEtalonControls.ctl" Type="VI" URL="../TypeDef_PiezoEtalonControls.ctl"/>
		<Item Name="TypeDef_BiFiSettings.ctl" Type="VI" URL="../TypeDef_BiFiSettings.ctl"/>
		<Item Name="TypeDef_ThinEtalonSettings.ctl" Type="VI" URL="../TypeDef_ThinEtalonSettings.ctl"/>
		<Item Name="SVI_WavelengthFrequencyStats.vi" Type="VI" URL="../SVI_WavelengthFrequencyStats.vi"/>
		<Item Name="FGV_SetGetWavelength.vi" Type="VI" URL="../FGV_SetGetWavelength.vi"/>
		<Item Name="TypeDef_SetpointControls.ctl" Type="VI" URL="../TypeDef_SetpointControls.ctl"/>
		<Item Name="SVI_PID_D_Contribution.vi" Type="VI" URL="../SVI_PID_D_Contribution.vi"/>
		<Item Name="SVI_PID_P_Contribution.vi" Type="VI" URL="../SVI_PID_P_Contribution.vi"/>
		<Item Name="SVI_dtInSeconds.vi" Type="VI" URL="../SVI_dtInSeconds.vi"/>
		<Item Name="SVI_PID_I_Contribution.vi" Type="VI" URL="../SVI_PID_I_Contribution.vi"/>
		<Item Name="SVI_PID_CoerceToMaxLaserChangeRate.vi" Type="VI" URL="../SVI_PID_CoerceToMaxLaserChangeRate.vi"/>
		<Item Name="SVI_WavelengthToFrequencyGraph.vi" Type="VI" URL="../SVI_WavelengthToFrequencyGraph.vi"/>
		<Item Name="SVI_PiezoEtalonInRange.vi" Type="VI" URL="../SVI_PiezoEtalonInRange.vi"/>
		<Item Name="SVI_RestartLockOrSkipToSlowPiezoControl.vi" Type="VI" URL="../SVI_RestartLockOrSkipToSlowPiezoControl.vi"/>
		<Item Name="SVI_DeltaWL_to_DeltaF.vi" Type="VI" URL="../SVI_DeltaWL_to_DeltaF.vi"/>
		<Item Name="Global_StabilisationStatus.vi" Type="VI" URL="../Global_StabilisationStatus.vi"/>
		<Item Name="SVI_AbsoluteErrorAndIsFrequencyInLockRange.vi" Type="VI" URL="../SVI_AbsoluteErrorAndIsFrequencyInLockRange.vi"/>
		<Item Name="FGV_StatusLog.vi" Type="VI" URL="../FGV_StatusLog.vi"/>
		<Item Name="TypeDef_LogActions.ctl" Type="VI" URL="../TypeDef_LogActions.ctl"/>
		<Item Name="SVI_CreateAppendLogFile.vi" Type="VI" URL="../SVI_CreateAppendLogFile.vi"/>
		<Item Name="SVI_LockedOrSeekingCheck.vi" Type="VI" URL="../SVI_LockedOrSeekingCheck.vi"/>
		<Item Name="FGV_SlowPiezoAndWavelengthCircularGraph.vi" Type="VI" URL="../FGV_SlowPiezoAndWavelengthCircularGraph.vi"/>
		<Item Name="FGV_CheckWavemeterIsSending.vi" Type="VI" URL="../FGV_CheckWavemeterIsSending.vi"/>
		<Item Name="SVI_SendSlackLogMessage.vi" Type="VI" URL="../SVI_SendSlackLogMessage.vi"/>
		<Item Name="TypeDef_SlackAction.ctl" Type="VI" URL="../TypeDef_SlackAction.ctl"/>
		<Item Name="SVI_SaveLastSessionSettings.vi" Type="VI" URL="../SVI_SaveLastSessionSettings.vi"/>
		<Item Name="SVI_LoadLastSessionSettings.vi" Type="VI" URL="../SVI_LoadLastSessionSettings.vi"/>
		<Item Name="Wavemeter081220.ctl" Type="VI" URL="../Wavemeter081220.ctl"/>
		<Item Name="FGV_ReadNetworkWavemeter.vi" Type="VI" URL="../FGV_ReadNetworkWavemeter.vi"/>
		<Item Name="TypeDef_Wavemeter.ctl" Type="VI" URL="../TypeDef_Wavemeter.ctl"/>
		<Item Name="TypeDef_BifiAndThinEtalonActions.ctl" Type="VI" URL="../TypeDef_BifiAndThinEtalonActions.ctl"/>
		<Item Name="FGV_BifiArrayGenerator.vi" Type="VI" URL="../FGV_BifiArrayGenerator.vi"/>
		<Item Name="FGV_LaserControlLoops.vi" Type="VI" URL="../FGV_LaserControlLoops.vi"/>
		<Item Name="SVI_SetBestBiFiPosition.vi" Type="VI" URL="../SVI_SetBestBiFiPosition.vi"/>
		<Item Name="TypeDef_GraphActions.ctl" Type="VI" URL="../TypeDef_GraphActions.ctl"/>
		<Item Name="SVI_SetBestThinEtalonPosition.vi" Type="VI" URL="../SVI_SetBestThinEtalonPosition.vi"/>
		<Item Name="SVI_ResetBifiThinEtalonSettings.vi" Type="VI" URL="../SVI_ResetBifiThinEtalonSettings.vi"/>
		<Item Name="MC Main.vi" Type="VI" URL="../MC Main.vi"/>
		<Item Name="LaserNetworkVariables.lvlib" Type="Library" URL="../LaserNetworkVariables.lvlib"/>
		<Item Name="Untitled Library 3.lvlib" Type="Library" URL="../Untitled Library 3.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Acquire Input Data.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/Acquire Input Data.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Bit-array To Byte-array.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/Bit-array To Byte-array.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Built App File Layout.vi" Type="VI" URL="/&lt;vilib&gt;/AppBuilder/Built App File Layout.vi"/>
				<Item Name="Calc Long Word Padded Width.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Calc Long Word Padded Width.vi"/>
				<Item Name="Check Color Table Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Color Table Size.vi"/>
				<Item Name="Check Data Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Data Size.vi"/>
				<Item Name="Check File Permissions.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check File Permissions.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Path.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Path.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Close File+.vi"/>
				<Item Name="Close Input Device.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/Close Input Device.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="closeJoystick.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/closeJoystick.vi"/>
				<Item Name="closeKeyboard.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/closeKeyboard.vi"/>
				<Item Name="closeMouse.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/closeMouse.vi"/>
				<Item Name="Color to RGB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/Color to RGB.vi"/>
				<Item Name="Compare Two Paths.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Compare Two Paths.vi"/>
				<Item Name="compatOverwrite.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOverwrite.vi"/>
				<Item Name="compatReadText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReadText.vi"/>
				<Item Name="compatReturnToEnter.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReturnToEnter.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Create ActiveX Event Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Create ActiveX Event Queue.vi"/>
				<Item Name="Create Error Clust.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Create Error Clust.vi"/>
				<Item Name="Create Mask By Alpha.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Create Mask By Alpha.vi"/>
				<Item Name="Destroy ActiveX Event Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Destroy ActiveX Event Queue.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Directory of Top Level VI.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Directory of Top Level VI.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrorDescriptions.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/ErrorDescriptions.vi"/>
				<Item Name="errorList.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/errorList.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Escape Characters for HTTP.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Escape Characters for HTTP.vi"/>
				<Item Name="EventData.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/EventData.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Flip and Pad for Picture Control.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Flip and Pad for Picture Control.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Generate Temporary File Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Generate Temporary File Path.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Initialize Mouse.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/Initialize Mouse.vi"/>
				<Item Name="joystickAcquire.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/joystickAcquire.vi"/>
				<Item Name="keyboardAcquire.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/keyboardAcquire.vi"/>
				<Item Name="LabVIEWHTTPClient.lvlib" Type="Library" URL="/&lt;vilib&gt;/httpClient/LabVIEWHTTPClient.lvlib"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVDateTimeRec.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVDateTimeRec.ctl"/>
				<Item Name="LVMenuShortCut.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVMenuShortCut.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="MD5Checksum core.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/MD5Checksum.llb/MD5Checksum core.vi"/>
				<Item Name="MD5Checksum File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/MD5Checksum.llb/MD5Checksum File.vi"/>
				<Item Name="MD5Checksum format message-digest.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/MD5Checksum.llb/MD5Checksum format message-digest.vi"/>
				<Item Name="MD5Checksum pad.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/MD5Checksum.llb/MD5Checksum pad.vi"/>
				<Item Name="mouseAcquire.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/mouseAcquire.vi"/>
				<Item Name="NI_AAL_SigProc.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AAL_SigProc.lvlib"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="NI_AALPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALPro.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_Gmath.lvlib" Type="Library" URL="/&lt;vilib&gt;/gmath/NI_Gmath.lvlib"/>
				<Item Name="NI_HTML.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/HTML/NI_HTML.lvclass"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_Matrix.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/Matrix/NI_Matrix.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_report.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/NI_report.lvclass"/>
				<Item Name="NI_ReportGenerationCore.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/NIReport.llb/NI_ReportGenerationCore.lvlib"/>
				<Item Name="NI_Standard Report.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Standard Report/NI_Standard Report.lvclass"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="OccFireType.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/OccFireType.ctl"/>
				<Item Name="Open File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Open File+.vi"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Open URL in Default Browser (path).vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser (path).vi"/>
				<Item Name="Open URL in Default Browser (string).vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser (string).vi"/>
				<Item Name="Open URL in Default Browser core.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser core.vi"/>
				<Item Name="Open URL in Default Browser.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser.vi"/>
				<Item Name="Path To Command Line String.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Path To Command Line String.vi"/>
				<Item Name="Path to URL.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Path to URL.vi"/>
				<Item Name="PathToUNIXPathString.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/CFURL.llb/PathToUNIXPathString.vi"/>
				<Item Name="Query Input Devices.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/Query Input Devices.vi"/>
				<Item Name="Read File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read File+ (string).vi"/>
				<Item Name="Read From Spreadsheet File (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (DBL).vi"/>
				<Item Name="Read From Spreadsheet File (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (I64).vi"/>
				<Item Name="Read From Spreadsheet File (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (string).vi"/>
				<Item Name="Read From Spreadsheet File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File.vi"/>
				<Item Name="Read Lines From File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Lines From File.vi"/>
				<Item Name="Read PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Read PNG File.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Recursive File List.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="SetMenuItemInfoSCConverter.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/SetMenuItemInfoSCConverter.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="Wait On ActiveX Event.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Wait On ActiveX Event.vi"/>
				<Item Name="Wait types.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Wait types.ctl"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write BMP Data To Buffer.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data To Buffer.vi"/>
				<Item Name="Write BMP Data.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data.vi"/>
				<Item Name="Write BMP File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP File.vi"/>
				<Item Name="Write GIF File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/gif.llb/Write GIF File.vi"/>
				<Item Name="Write JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Write JPEG File.vi"/>
				<Item Name="Write PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Write PNG File.vi"/>
				<Item Name="Write Spreadsheet String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Spreadsheet String.vi"/>
				<Item Name="Write To Spreadsheet File (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (DBL).vi"/>
				<Item Name="Write To Spreadsheet File (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (I64).vi"/>
				<Item Name="Write To Spreadsheet File (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (string).vi"/>
				<Item Name="Write To Spreadsheet File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File.vi"/>
			</Item>
			<Item Name="Add Protocol Dataset.vi" Type="VI" URL="../Source/Protocol Window/Add Protocol Dataset.vi"/>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Bifi Parameter.ctl" Type="VI" URL="../Source/BiFi/Bifi Parameter.ctl"/>
			<Item Name="Button.ctl" Type="VI" URL="../Source/Dialog/Button.ctl"/>
			<Item Name="Calibration Table Menu.rtm" Type="Document" URL="../Source/BiFi/Calibration Table Editor/Calibration Table Menu.rtm"/>
			<Item Name="Cancel Button 96x12.ctl" Type="VI" URL="../Source/MC Dialog Controls/Cancel Button 96x12.ctl"/>
			<Item Name="Configuration Cluster.ctl" Type="VI" URL="../Source/MC Dialog Controls/Configuration Cluster.ctl"/>
			<Item Name="Control Protocol Channel.ctl" Type="VI" URL="../Source/Protocol Window/Control Protocol Channel.ctl"/>
			<Item Name="Dialog Protocol.vi" Type="VI" URL="../Source/Protocol Window/Dialog Protocol.vi"/>
			<Item Name="FGV_FrontPanelSetPointUnits.vi" Type="VI" URL="../FGV_FrontPanelSetPointUnits.vi"/>
			<Item Name="FGV_PiezoEtalonSettings.vi" Type="VI" URL="../FGV_PiezoEtalonSettings.vi"/>
			<Item Name="FGV_PowerCircularGraph.vi" Type="VI" URL="../FGV_PowerCircularGraph.vi"/>
			<Item Name="FGV_ScanLaser.vi" Type="VI" URL="../FGV_ScanLaser.vi"/>
			<Item Name="FGV_SlowPiezoSettings.vi" Type="VI" URL="../FGV_SlowPiezoSettings.vi"/>
			<Item Name="FGV_StateQueue.vi" Type="VI" URL="../FGV_StateQueue.vi"/>
			<Item Name="FGV_ThinEtalonGeneratorI32.vi" Type="VI" URL="../FGV_ThinEtalonGeneratorI32.vi"/>
			<Item Name="Find Control Refnum.vi" Type="VI" URL="../Source/Misc/Find Control Refnum.vi"/>
			<Item Name="Global New Command.vi" Type="VI" URL="../Source/Protocol Window/Global New Command.vi"/>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="LowLevelAPI.lvlibp" Type="LVLibp" URL="../Dependent/LowLevelAPI.lvlibp">
				<Item Name="API Button Type.ctl" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/Class_VISA/VISA/API Button Type.ctl"/>
				<Item Name="API Check for Custom Error.vi" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/Errors/API Check for Custom Error.vi"/>
				<Item Name="API Communication Type.ctl" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/Program/API Communication Type.ctl"/>
				<Item Name="API Device.ctl" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/Program/API Device.ctl"/>
				<Item Name="API Dialog Error.vi" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/Program/API Dialog Error.vi"/>
				<Item Name="API Exit Global.vi" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/Program/API Exit Global.vi"/>
				<Item Name="API Format into Fix.vi" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/Program/API Format into Fix.vi"/>
				<Item Name="API Help Window.vi" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/Program/API Help Window.vi"/>
				<Item Name="API Init VISA Choose Device.vi" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/Class_VISA/VISA/API Init VISA Choose Device.vi"/>
				<Item Name="API OK Button Template.ctl" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/Class_VISA/VISA/API OK Button Template.ctl"/>
				<Item Name="API Program Check device.vi" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/Program/API Program Check device.vi"/>
				<Item Name="API Program to Device.vi" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/Program/API Program to Device.vi"/>
				<Item Name="API Select VISA Device.vi" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/Class_VISA/API Select VISA Device.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/1abvi3w/vi.lib/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Bit-array To Byte-array.vi" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/1abvi3w/vi.lib/picture/pictutil.llb/Bit-array To Byte-array.vi"/>
				<Item Name="Check Path.vi" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/1abvi3w/vi.lib/picture/jpeg.llb/Check Path.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Communication.lvclass" Type="LVClass" URL="../Dependent/LowLevelAPI.lvlibp/Class Communication/Communication.lvclass"/>
				<Item Name="Create Mask By Alpha.vi" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/1abvi3w/vi.lib/picture/picture.llb/Create Mask By Alpha.vi"/>
				<Item Name="Directory of Top Level VI.vi" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/1abvi3w/vi.lib/picture/jpeg.llb/Directory of Top Level VI.vi"/>
				<Item Name="Dummy.lvclass" Type="LVClass" URL="../Dependent/LowLevelAPI.lvlibp/Class_Dummy/Dummy.lvclass"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/1abvi3w/vi.lib/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Network Server.lvclass" Type="LVClass" URL="../Dependent/LowLevelAPI.lvlibp/Network Server/Network Server.lvclass"/>
				<Item Name="Network.lvclass" Type="LVClass" URL="../Dependent/LowLevelAPI.lvlibp/Class_Network/Network.lvclass"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="../Dependent/LowLevelAPI.lvlibp/1abvi3w/vi.lib/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="Read PNG File.vi" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/1abvi3w/vi.lib/picture/png.llb/Read PNG File.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/1abvi3w/vi.lib/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="VISA Find Search Mode.ctl" Type="VI" URL="../Dependent/LowLevelAPI.lvlibp/1abvi3w/vi.lib/Instr/_visa.llb/VISA Find Search Mode.ctl"/>
				<Item Name="VISA.lvclass" Type="LVClass" URL="../Dependent/LowLevelAPI.lvlibp/Class_VISA/VISA.lvclass"/>
			</Item>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="lvinput.dll" Type="Document" URL="/&lt;resource&gt;/lvinput.dll"/>
			<Item Name="Matisse API CONFIGURATION Get Device.vi" Type="VI" URL="../Dependent/Matisse API.llb/Matisse API CONFIGURATION Get Device.vi"/>
			<Item Name="Matisse API.lvlibp" Type="LVLibp" URL="../Dependent/Matisse API.lvlibp">
				<Item Name="Matisse API CONFIGURATION Get Device.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API CONFIGURATION Get Device.vi"/>
				<Item Name="Matisse API CONFIGURATION Set Device.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API CONFIGURATION Set Device.vi"/>
				<Item Name="Matisse API DEVICE Reset.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API DEVICE Reset.vi"/>
				<Item Name="Matisse API DPOW Get DC.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API DPOW Get DC.vi"/>
				<Item Name="Matisse API DPOW Get Low Power Level.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API DPOW Get Low Power Level.vi"/>
				<Item Name="Matisse API DPOW Get Wave Table.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API DPOW Get Wave Table.vi"/>
				<Item Name="Matisse API DPOW Set Low Power Level.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API DPOW Set Low Power Level.vi"/>
				<Item Name="Matisse API FEF Get Amplitude.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API FEF Get Amplitude.vi"/>
				<Item Name="Matisse API FEF Get Phase Shift.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API FEF Get Phase Shift.vi"/>
				<Item Name="Matisse API FEF Set Amplitude.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API FEF Set Amplitude.vi"/>
				<Item Name="Matisse API FEF Set Phase Shift.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API FEF Set Phase Shift.vi"/>
				<Item Name="Matisse API FPZ Get Control SetPoint.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API FPZ Get Control SetPoint.vi"/>
				<Item Name="Matisse API FPZ Get Control Status.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API FPZ Get Control Status.vi"/>
				<Item Name="Matisse API FPZ Get Current Position.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API FPZ Get Current Position.vi"/>
				<Item Name="Matisse API FPZ Get Input.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API FPZ Get Input.vi"/>
				<Item Name="Matisse API FPZ Get Integral Gain.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API FPZ Get Integral Gain.vi"/>
				<Item Name="Matisse API FPZ Get Lock Point.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API FPZ Get Lock Point.vi"/>
				<Item Name="Matisse API FPZ Get Lock Status.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API FPZ Get Lock Status.vi"/>
				<Item Name="Matisse API FPZ Set Control Set Point.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API FPZ Set Control Set Point.vi"/>
				<Item Name="Matisse API FPZ Set Control Status.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API FPZ Set Control Status.vi"/>
				<Item Name="Matisse API FPZ Set Current Position.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API FPZ Set Current Position.vi"/>
				<Item Name="Matisse API FPZ Set Integral Gain.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API FPZ Set Integral Gain.vi"/>
				<Item Name="Matisse API FPZ Set Lock Point.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API FPZ Set Lock Point.vi"/>
				<Item Name="Matisse API Identification.ctl" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API Identification.ctl"/>
				<Item Name="Matisse API Identification.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API Identification.vi"/>
				<Item Name="Matisse API MOTBI Calibration Get Motor Factor.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API MOTBI Calibration Get Motor Factor.vi"/>
				<Item Name="Matisse API MOTBI Calibration Get Motor Offset.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API MOTBI Calibration Get Motor Offset.vi"/>
				<Item Name="Matisse API MOTBI Calibration Get Order.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API MOTBI Calibration Get Order.vi"/>
				<Item Name="Matisse API MOTBI Calibration Get Plate Thickness.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API MOTBI Calibration Get Plate Thickness.vi"/>
				<Item Name="Matisse API MOTBI Calibration Set Motor Factor.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API MOTBI Calibration Set Motor Factor.vi"/>
				<Item Name="Matisse API MOTBI Calibration Set Motor Offset.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API MOTBI Calibration Set Motor Offset.vi"/>
				<Item Name="Matisse API MOTBI Calibration Set Order.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API MOTBI Calibration Set Order.vi"/>
				<Item Name="Matisse API MOTBI Calibration Set Plate Thickness.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API MOTBI Calibration Set Plate Thickness.vi"/>
				<Item Name="Matisse API MOTBI Get Calibration Parameters.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API MOTBI Get Calibration Parameters.vi"/>
				<Item Name="Matisse API MOTBI Get Cavity Scan.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API MOTBI Get Cavity Scan.vi"/>
				<Item Name="Matisse API MOTBI Get Reference Scan.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API MOTBI Get Reference Scan.vi"/>
				<Item Name="Matisse API MOTBI Get Wavelength.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API MOTBI Get Wavelength.vi"/>
				<Item Name="Matisse API MOTBI Set Calibration Parameters.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API MOTBI Set Calibration Parameters.vi"/>
				<Item Name="Matisse API MOTBI Set Cavity Scan.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API MOTBI Set Cavity Scan.vi"/>
				<Item Name="Matisse API MOTBI Set Reference Scan.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API MOTBI Set Reference Scan.vi"/>
				<Item Name="Matisse API MOTBI Set Wavelength.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API MOTBI Set Wavelength.vi"/>
				<Item Name="Matisse API Motor Clear Errors.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API Motor Clear Errors.vi"/>
				<Item Name="Matisse API Motor Get Increment.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API Motor Get Increment.vi"/>
				<Item Name="Matisse API Motor Get Maximum.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API Motor Get Maximum.vi"/>
				<Item Name="Matisse API Motor Get Position.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API Motor Get Position.vi"/>
				<Item Name="Matisse API Motor Get Status.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API Motor Get Status.vi"/>
				<Item Name="Matisse API Motor Go To Home.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API Motor Go To Home.vi"/>
				<Item Name="Matisse API Motor Halt.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API Motor Halt.vi"/>
				<Item Name="Matisse API Motor Set Increment.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API Motor Set Increment.vi"/>
				<Item Name="Matisse API Motor Set Position.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API Motor Set Position.vi"/>
				<Item Name="Matisse API Motor Set Relative Position.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API Motor Set Relative Position.vi"/>
				<Item Name="Matisse API Motor Status Cluster.ctl" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API Motor Status Cluster.ctl"/>
				<Item Name="Matisse API Motor Statuscode To String.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API Motor Statuscode To String.vi"/>
				<Item Name="Matisse API Motor Wait.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API Motor Wait.vi"/>
				<Item Name="Matisse API Motor.ctl" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API Motor.ctl"/>
				<Item Name="Matisse API PARAMETER Add Additional Parameter.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PARAMETER Add Additional Parameter.vi"/>
				<Item Name="Matisse API PARAMETER Delete Parameter Set.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PARAMETER Delete Parameter Set.vi"/>
				<Item Name="Matisse API PARAMETER Get Active Parameter Set.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PARAMETER Get Active Parameter Set.vi"/>
				<Item Name="Matisse API PARAMETER Get Additional Parameter Value.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PARAMETER Get Additional Parameter Value.vi"/>
				<Item Name="Matisse API PARAMETER List Configurations.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PARAMETER List Configurations.vi"/>
				<Item Name="Matisse API PARAMETER Load Parameter Set.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PARAMETER Load Parameter Set.vi"/>
				<Item Name="Matisse API PARAMETER Set Default Parameter Set.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PARAMETER Set Default Parameter Set.vi"/>
				<Item Name="Matisse API PARAMETER Set Parameter Set.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PARAMETER Set Parameter Set.vi"/>
				<Item Name="Matisse API PDH Get Attenuator.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PDH Get Attenuator.vi"/>
				<Item Name="Matisse API PDH Get Control Status.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PDH Get Control Status.vi"/>
				<Item Name="Matisse API PDH Get DSP Offset.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PDH Get DSP Offset.vi"/>
				<Item Name="Matisse API PDH Get Fast Offset.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PDH Get Fast Offset.vi"/>
				<Item Name="Matisse API PDH Get Phase Shift.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PDH Get Phase Shift.vi"/>
				<Item Name="Matisse API PDH Get Slow Offset.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PDH Get Slow Offset.vi"/>
				<Item Name="Matisse API PDH Identification.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PDH Identification.vi"/>
				<Item Name="Matisse API PDH Multiplexer Input.ctl" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PDH Multiplexer Input.ctl"/>
				<Item Name="Matisse API PDH Reset.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PDH Reset.vi"/>
				<Item Name="Matisse API PDH Set Attenuator.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PDH Set Attenuator.vi"/>
				<Item Name="Matisse API PDH Set Cavity EOM Status.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PDH Set Cavity EOM Status.vi"/>
				<Item Name="Matisse API PDH Set Control Status.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PDH Set Control Status.vi"/>
				<Item Name="Matisse API PDH Set DSP Offset.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PDH Set DSP Offset.vi"/>
				<Item Name="Matisse API PDH Set EOM Control Status.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PDH Set EOM Control Status.vi"/>
				<Item Name="Matisse API PDH Set Fast Offset.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PDH Set Fast Offset.vi"/>
				<Item Name="Matisse API PDH Set Modulation Status.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PDH Set Modulation Status.vi"/>
				<Item Name="Matisse API PDH Set Multiplexer Input.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PDH Set Multiplexer Input.vi"/>
				<Item Name="Matisse API PDH Set Phase Shift.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PDH Set Phase Shift.vi"/>
				<Item Name="Matisse API PDH Set Slow Offset.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PDH Set Slow Offset.vi"/>
				<Item Name="Matisse API PID Get Entry.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PID Get Entry.vi"/>
				<Item Name="Matisse API PID Get Ordinal.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PID Get Ordinal.vi"/>
				<Item Name="Matisse API PID Get Protocol.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PID Get Protocol.vi"/>
				<Item Name="Matisse API PID Get Statistics.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PID Get Statistics.vi"/>
				<Item Name="Matisse API PID Set Protocol.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PID Set Protocol.vi"/>
				<Item Name="Matisse API PZETL Get Amplitude.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PZETL Get Amplitude.vi"/>
				<Item Name="Matisse API PZETL Get Average.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PZETL Get Average.vi"/>
				<Item Name="Matisse API PZETL Get Baseline.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PZETL Get Baseline.vi"/>
				<Item Name="Matisse API PZETL Get Cavity Scan.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PZETL Get Cavity Scan.vi"/>
				<Item Name="Matisse API PZETL Get Control Status.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PZETL Get Control Status.vi"/>
				<Item Name="Matisse API PZETL Get Error Signal.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PZETL Get Error Signal.vi"/>
				<Item Name="Matisse API PZETL Get Oversampling.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PZETL Get Oversampling.vi"/>
				<Item Name="Matisse API PZETL Get Phase Shift.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PZETL Get Phase Shift.vi"/>
				<Item Name="Matisse API PZETL Get Proportional Gain.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PZETL Get Proportional Gain.vi"/>
				<Item Name="Matisse API PZETL Get Reference Scan.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PZETL Get Reference Scan.vi"/>
				<Item Name="Matisse API PZETL Get Sample Rate.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PZETL Get Sample Rate.vi"/>
				<Item Name="Matisse API PZETL Sample Rate.ctl" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PZETL Sample Rate.ctl"/>
				<Item Name="Matisse API PZETL Set Amplitude.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PZETL Set Amplitude.vi"/>
				<Item Name="Matisse API PZETL Set Average.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PZETL Set Average.vi"/>
				<Item Name="Matisse API PZETL Set Baseline.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PZETL Set Baseline.vi"/>
				<Item Name="Matisse API PZETL Set Cavity Scan.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PZETL Set Cavity Scan.vi"/>
				<Item Name="Matisse API PZETL Set Control Status.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PZETL Set Control Status.vi"/>
				<Item Name="Matisse API PZETL Set Oversampling.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PZETL Set Oversampling.vi"/>
				<Item Name="Matisse API PZETL Set Phase Shift.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PZETL Set Phase Shift.vi"/>
				<Item Name="Matisse API PZETL Set Proportional Gain.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PZETL Set Proportional Gain.vi"/>
				<Item Name="Matisse API PZETL Set Reference Scan.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PZETL Set Reference Scan.vi"/>
				<Item Name="Matisse API PZETL Set Sample Rate.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API PZETL Set Sample Rate.vi"/>
				<Item Name="Matisse API REFCELL Get Gain.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API REFCELL Get Gain.vi"/>
				<Item Name="Matisse API REFCELL Get Gaincode.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API REFCELL Get Gaincode.vi"/>
				<Item Name="Matisse API REFCELL Get Lower Limit.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API REFCELL Get Lower Limit.vi"/>
				<Item Name="Matisse API REFCELL Get NOW.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API REFCELL Get NOW.vi"/>
				<Item Name="Matisse API REFCELL Get Oversampling.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API REFCELL Get Oversampling.vi"/>
				<Item Name="Matisse API REFCELL Get Sampling Mode.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API REFCELL Get Sampling Mode.vi"/>
				<Item Name="Matisse API REFCELL Get Table.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API REFCELL Get Table.vi"/>
				<Item Name="Matisse API REFCELL Get Upper Limit.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API REFCELL Get Upper Limit.vi"/>
				<Item Name="Matisse API REFCELL Set Gaincode.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API REFCELL Set Gaincode.vi"/>
				<Item Name="Matisse API REFCELL Set Lower Limit.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API REFCELL Set Lower Limit.vi"/>
				<Item Name="Matisse API REFCELL Set NOW.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API REFCELL Set NOW.vi"/>
				<Item Name="Matisse API REFCELL Set Oversampling.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API REFCELL Set Oversampling.vi"/>
				<Item Name="Matisse API REFCELL Set Sampling Mode.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API REFCELL Set Sampling Mode.vi"/>
				<Item Name="Matisse API REFCELL Set Upper Limit.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API REFCELL Set Upper Limit.vi"/>
				<Item Name="Matisse API Scan Device.ctl" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API Scan Device.ctl"/>
				<Item Name="Matisse API SCAN Get Cavity Calibration.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SCAN Get Cavity Calibration.vi"/>
				<Item Name="Matisse API SCAN Get Current Position.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SCAN Get Current Position.vi"/>
				<Item Name="Matisse API SCAN Get Device.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SCAN Get Device.vi"/>
				<Item Name="Matisse API SCAN Get Falling Ramp Speed.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SCAN Get Falling Ramp Speed.vi"/>
				<Item Name="Matisse API SCAN Get Lower Limit.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SCAN Get Lower Limit.vi"/>
				<Item Name="Matisse API SCAN Get Mode.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SCAN Get Mode.vi"/>
				<Item Name="Matisse API SCAN Get Reference Calibration.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SCAN Get Reference Calibration.vi"/>
				<Item Name="Matisse API SCAN Get Rising Ramp Speed.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SCAN Get Rising Ramp Speed.vi"/>
				<Item Name="Matisse API SCAN Get Status.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SCAN Get Status.vi"/>
				<Item Name="Matisse API SCAN Get Upper Limit.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SCAN Get Upper Limit.vi"/>
				<Item Name="Matisse API SCAN Set Cavity Calibration.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SCAN Set Cavity Calibration.vi"/>
				<Item Name="Matisse API SCAN Set Current Position.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SCAN Set Current Position.vi"/>
				<Item Name="Matisse API SCAN Set Device.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SCAN Set Device.vi"/>
				<Item Name="Matisse API SCAN Set Falling Ramp Speed.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SCAN Set Falling Ramp Speed.vi"/>
				<Item Name="Matisse API SCAN Set Lower Limit.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SCAN Set Lower Limit.vi"/>
				<Item Name="Matisse API SCAN Set Mode.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SCAN Set Mode.vi"/>
				<Item Name="Matisse API SCAN Set Reference Calibration.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SCAN Set Reference Calibration.vi"/>
				<Item Name="Matisse API SCAN Set Rising Ramp Speed.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SCAN Set Rising Ramp Speed.vi"/>
				<Item Name="Matisse API SCAN Set Status.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SCAN Set Status.vi"/>
				<Item Name="Matisse API SCAN Set Upper Limit.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SCAN Set Upper Limit.vi"/>
				<Item Name="Matisse API SPZ Get Control SetPoint.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SPZ Get Control SetPoint.vi"/>
				<Item Name="Matisse API SPZ Get Control Status.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SPZ Get Control Status.vi"/>
				<Item Name="Matisse API SPZ Get Current Position.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SPZ Get Current Position.vi"/>
				<Item Name="Matisse API SPZ Get Free Speed.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SPZ Get Free Speed.vi"/>
				<Item Name="Matisse API SPZ Get Lock Integral Gain.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SPZ Get Lock Integral Gain.vi"/>
				<Item Name="Matisse API SPZ Get Lock Proportional Gain.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SPZ Get Lock Proportional Gain.vi"/>
				<Item Name="Matisse API SPZ Get Reference Scan.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SPZ Get Reference Scan.vi"/>
				<Item Name="Matisse API SPZ Set Control SetPoint.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SPZ Set Control SetPoint.vi"/>
				<Item Name="Matisse API SPZ Set Control Status.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SPZ Set Control Status.vi"/>
				<Item Name="Matisse API SPZ Set Current Position.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SPZ Set Current Position.vi"/>
				<Item Name="Matisse API SPZ Set Free Speed.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SPZ Set Free Speed.vi"/>
				<Item Name="Matisse API SPZ Set Lock Integral Gain.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SPZ Set Lock Integral Gain.vi"/>
				<Item Name="Matisse API SPZ Set Lock Proportional Gain.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SPZ Set Lock Proportional Gain.vi"/>
				<Item Name="Matisse API SPZ Set Reference Scan.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API SPZ Set Reference Scan.vi"/>
				<Item Name="Matisse API TE Flank Orientation.ctl" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API TE Flank Orientation.ctl"/>
				<Item Name="Matisse API TE Get Average.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API TE Get Average.vi"/>
				<Item Name="Matisse API TE Get Cavity Scan.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API TE Get Cavity Scan.vi"/>
				<Item Name="Matisse API TE Get Control Goal.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API TE Get Control Goal.vi"/>
				<Item Name="Matisse API TE Get Control Status.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API TE Get Control Status.vi"/>
				<Item Name="Matisse API TE Get DC.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API TE Get DC.vi"/>
				<Item Name="Matisse API TE Get Error Signal.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API TE Get Error Signal.vi"/>
				<Item Name="Matisse API TE Get Flank Orientation.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API TE Get Flank Orientation.vi"/>
				<Item Name="Matisse API TE Get Integral Gain.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API TE Get Integral Gain.vi"/>
				<Item Name="Matisse API TE Get Proportional Gain.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API TE Get Proportional Gain.vi"/>
				<Item Name="Matisse API TE Get Reference Scan.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API TE Get Reference Scan.vi"/>
				<Item Name="Matisse API TE Set Average.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API TE Set Average.vi"/>
				<Item Name="Matisse API TE Set Cavity Scan.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API TE Set Cavity Scan.vi"/>
				<Item Name="Matisse API TE Set Control Goal.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API TE Set Control Goal.vi"/>
				<Item Name="Matisse API TE Set Control Status.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API TE Set Control Status.vi"/>
				<Item Name="Matisse API TE Set Flank Orientation.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API TE Set Flank Orientation.vi"/>
				<Item Name="Matisse API TE Set Integral Gain.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API TE Set Integral Gain.vi"/>
				<Item Name="Matisse API TE Set Proportional Gain.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API TE Set Proportional Gain.vi"/>
				<Item Name="Matisse API TE Set Reference Scan.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API TE Set Reference Scan.vi"/>
				<Item Name="Matisse API Throw Error.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API Throw Error.vi"/>
				<Item Name="Matisse API TWI Read.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API TWI Read.vi"/>
				<Item Name="Matisse API TWI Send.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API TWI Send.vi"/>
				<Item Name="Matisse API UPDATE Data.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API UPDATE Data.vi"/>
				<Item Name="Matisse API UPDATE Execute.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API UPDATE Execute.vi"/>
				<Item Name="Matisse API UPDATE Get Checksum.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API UPDATE Get Checksum.vi"/>
				<Item Name="Matisse API UPDATE Reset.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/src/Matisse API UPDATE Reset.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="../Dependent/Matisse API.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="../Dependent/Matisse API.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="Matisse HL API Convert Frequency to Unit.vi" Type="VI" URL="../Source/Misc/Matisse HL API Convert Frequency to Unit.vi"/>
			<Item Name="Matisse HL API Convert Unit to Frequency.vi" Type="VI" URL="../Source/Misc/Matisse HL API Convert Unit to Frequency.vi"/>
			<Item Name="MC Abort Function.vi" Type="VI" URL="../Source/Misc/MC Abort Function.vi"/>
			<Item Name="MC Adapt Front Panel.vi" Type="VI" URL="../Source/Misc/MC Adapt Front Panel.vi"/>
			<Item Name="MC Add Help Menu.vi" Type="VI" URL="../Source/Misc/MC Add Help Menu.vi"/>
			<Item Name="MC Advanced GUI Global LV2.vi" Type="VI" URL="../Source/Globals/MC Advanced GUI Global LV2.vi"/>
			<Item Name="MC Airy Function Inverse.vi" Type="VI" URL="../Source/Misc/MC Airy Function Inverse.vi"/>
			<Item Name="MC Airy Levenberg Marquardt Prep.vi" Type="VI" URL="../Source/Stabilized/Sideoffringe/Airy Fit/MC Airy Levenberg Marquardt Prep.vi"/>
			<Item Name="MC Airy Levenberg Marquardt.vi" Type="VI" URL="../Source/Stabilized/Sideoffringe/Airy Fit/MC Airy Levenberg Marquardt.vi"/>
			<Item Name="MC AiryFit DLL Calculate.vi" Type="VI" URL="../Source/Stabilized/Sideoffringe/Airy Fit/MC AiryFit DLL Calculate.vi"/>
			<Item Name="MC App Running Global LV2.vi" Type="VI" URL="../Source/Globals/MC App Running Global LV2.vi"/>
			<Item Name="MC BiFi AutoFit.vi" Type="VI" URL="../Source/BiFi/Calibration Table Editor/fit/MC BiFi AutoFit.vi"/>
			<Item Name="MC Bifi Built Arrays.vi" Type="VI" URL="../Source/BiFi/MC Bifi Built Arrays.vi"/>
			<Item Name="MC BiFi Calibration BiFi Fit Function.vi" Type="VI" URL="../Source/BiFi/Calibration Table Editor/fit/MC BiFi Calibration BiFi Fit Function.vi"/>
			<Item Name="MC BiFi Calibration BiFi Function.vi" Type="VI" URL="../Source/BiFi/Calibration Table Editor/fit/MC BiFi Calibration BiFi Function.vi"/>
			<Item Name="MC BiFi Calibration Core Fit.vi" Type="VI" URL="../Source/BiFi/Calibration Table Editor/fit/MC BiFi Calibration Core Fit.vi"/>
			<Item Name="MC Bifi Find Order.vi" Type="VI" URL="../Source/BiFi/MC Bifi Find Order.vi"/>
			<Item Name="MC Bifi Init Parameter Cluster.vi" Type="VI" URL="../Source/BiFi/MC Bifi Init Parameter Cluster.vi"/>
			<Item Name="MC Bifi Pull Double Values.vi" Type="VI" URL="../Source/BiFi/MC Bifi Pull Double Values.vi"/>
			<Item Name="MC BiFi Set Colors.vi" Type="VI" URL="../Source/BiFi/MC BiFi Set Colors.vi"/>
			<Item Name="MC Bifi Set Windows Title.vi" Type="VI" URL="../Source/BiFi/MC Bifi Set Windows Title.vi"/>
			<Item Name="MC Birefringent Scan Options Get_Set.vi" Type="VI" URL="../Source/BiFi/MC Birefringent Scan Options Get_Set.vi"/>
			<Item Name="MC Burst Scan Initialization.vi" Type="VI" URL="../Source/Misc/MC Burst Scan Initialization.vi"/>
			<Item Name="MC Burst Scan Next Step.vi" Type="VI" URL="../Source/Misc/MC Burst Scan Next Step.vi"/>
			<Item Name="MC Calculate Wavelength from Biref Pos.vi" Type="VI" URL="../Source/BiFi/MC Calculate Wavelength from Biref Pos.vi"/>
			<Item Name="MC Calibration Fit Add Penalty Residue.vi" Type="VI" URL="../Source/BiFi/Calibration Table Editor/fit/MC Calibration Fit Add Penalty Residue.vi"/>
			<Item Name="MC Calibration Table Build Graph.vi" Type="VI" URL="../Source/BiFi/Calibration Table Editor/MC Calibration Table Build Graph.vi"/>
			<Item Name="MC Calibration Table Read From File.vi" Type="VI" URL="../Source/BiFi/Calibration Table Editor/MC Calibration Table Read From File.vi"/>
			<Item Name="MC Calibration Table Write To File.vi" Type="VI" URL="../Source/BiFi/Calibration Table Editor/MC Calibration Table Write To File.vi"/>
			<Item Name="MC Catch Error.vi" Type="VI" URL="../Source/Misc/MC Catch Error.vi"/>
			<Item Name="MC Check Configuration.vi" Type="VI" URL="../Source/Misc/MC Check Configuration.vi"/>
			<Item Name="MC Check for Matisse Device Presence.vi" Type="VI" URL="../Source/Misc/MC Check for Matisse Device Presence.vi"/>
			<Item Name="MC Check Overwrite With Dialog.vi" Type="VI" URL="../Source/Misc/MC Check Overwrite With Dialog.vi"/>
			<Item Name="MC Communication Options.vi" Type="VI" URL="../Source/Dialog/MC Communication Options.vi"/>
			<Item Name="MC Config Ref Global LV2.vi" Type="VI" URL="../Source/Globals/MC Config Ref Global LV2.vi"/>
			<Item Name="MC Configuration Stabilized.vi" Type="VI" URL="../Source/Stabilized/MC Configuration Stabilized.vi"/>
			<Item Name="MC Configuration.ctl" Type="VI" URL="../Source/Stabilized/MC Configuration.ctl"/>
			<Item Name="MC Control Loop Live View Options Get_Set.vi" Type="VI" URL="../Source/Misc/MC Control Loop Live View Options Get_Set.vi"/>
			<Item Name="MC Control Loop Status Get_Set.vi" Type="VI" URL="../Source/Misc/MC Control Loop Status Get_Set.vi"/>
			<Item Name="MC ControlScan Measurement Options Get_Set.vi" Type="VI" URL="../Source/Slow Piezo/MC ControlScan Measurement Options Get_Set.vi"/>
			<Item Name="MC ControlScan Parameters get_set.vi" Type="VI" URL="../Source/Slow Piezo/MC ControlScan Parameters get_set.vi"/>
			<Item Name="MC ControlScan Value Measurement.vi" Type="VI" URL="../Source/Slow Piezo/MC ControlScan Value Measurement.vi"/>
			<Item Name="MC Convert PZETL CL Phaseshift to-from Degree.vi" Type="VI" URL="../Source/PZETL/MC Convert PZETL CL Phaseshift to-from Degree.vi"/>
			<Item Name="MC Convert Scan Speed Voltage to-from Frequency Change.vi" Type="VI" URL="../Source/Slow Piezo/MC Convert Scan Speed Voltage to-from Frequency Change.vi"/>
			<Item Name="MC Convert Wavelength to MotBi Position.vi" Type="VI" URL="../Source/BiFi/MC Convert Wavelength to MotBi Position.vi"/>
			<Item Name="MC Device BiFi Configuration.ctl" Type="VI" URL="../Source/BiFi/MC Device BiFi Configuration.ctl"/>
			<Item Name="MC Device Conf Birefringent Conf Get_Set from_to file.vi" Type="VI" URL="../Source/Device Config/MC Device Conf Birefringent Conf Get_Set from_to file.vi"/>
			<Item Name="MC Device Conf Birefringent Get_Set Configuration.vi" Type="VI" URL="../Source/Device Config/MC Device Conf Birefringent Get_Set Configuration.vi"/>
			<Item Name="MC Device Conf ControlScan Conf Get_Set from_to file.vi" Type="VI" URL="../Source/Device Config/MC Device Conf ControlScan Conf Get_Set from_to file.vi"/>
			<Item Name="MC Device Conf ControlScan Get_Set Configuration.vi" Type="VI" URL="../Source/Device Config/MC Device Conf ControlScan Get_Set Configuration.vi"/>
			<Item Name="MC Device Conf DPOW Conf Get_Set from_to file.vi" Type="VI" URL="../Source/Device Config/MC Device Conf DPOW Conf Get_Set from_to file.vi"/>
			<Item Name="MC Device Conf DPOW Get_Set Configuration.vi" Type="VI" URL="../Source/Device Config/MC Device Conf DPOW Get_Set Configuration.vi"/>
			<Item Name="MC Device Conf FPZ Conf Get_Set from_to file.vi" Type="VI" URL="../Source/Device Config/MC Device Conf FPZ Conf Get_Set from_to file.vi"/>
			<Item Name="MC Device Conf FPZ Get_Set Configuration.vi" Type="VI" URL="../Source/Device Config/MC Device Conf FPZ Get_Set Configuration.vi"/>
			<Item Name="MC Device Conf Hardware Conf Get_Set from_to file.vi" Type="VI" URL="../Source/Device Config/MC Device Conf Hardware Conf Get_Set from_to file.vi"/>
			<Item Name="MC Device Conf Hardware Get_Set Configuration.vi" Type="VI" URL="../Source/Device Config/MC Device Conf Hardware Get_Set Configuration.vi"/>
			<Item Name="MC Device Conf Load Configuration INI File.vi" Type="VI" URL="../Source/Device Config/MC Device Conf Load Configuration INI File.vi"/>
			<Item Name="MC Device Conf PDH Conf Get_Set from_to file.vi" Type="VI" URL="../Source/Device Config/MC Device Conf PDH Conf Get_Set from_to file.vi"/>
			<Item Name="MC Device Conf PDH Get_Set Configuration.vi" Type="VI" URL="../Source/Device Config/MC Device Conf PDH Get_Set Configuration.vi"/>
			<Item Name="MC Device Conf PZETL Conf Get_Set from_to file.vi" Type="VI" URL="../Source/Device Config/MC Device Conf PZETL Conf Get_Set from_to file.vi"/>
			<Item Name="MC Device Conf PZETL Get_Set Configuration.vi" Type="VI" URL="../Source/Device Config/MC Device Conf PZETL Get_Set Configuration.vi"/>
			<Item Name="MC Device Conf RefCell SCAN Conf Get_Set from_to file.vi" Type="VI" URL="../Source/Device Config/MC Device Conf RefCell SCAN Conf Get_Set from_to file.vi"/>
			<Item Name="MC Device Conf RefCell SCAN Get_Set Configuration.vi" Type="VI" URL="../Source/Device Config/MC Device Conf RefCell SCAN Get_Set Configuration.vi"/>
			<Item Name="MC Device Conf Save Configuration INI File.vi" Type="VI" URL="../Source/Device Config/MC Device Conf Save Configuration INI File.vi"/>
			<Item Name="MC Device Conf SCAN Conf Get_Set from_to file.vi" Type="VI" URL="../Source/Device Config/MC Device Conf SCAN Conf Get_Set from_to file.vi"/>
			<Item Name="MC Device Conf SCAN Get_Set Configuration.vi" Type="VI" URL="../Source/Device Config/MC Device Conf SCAN Get_Set Configuration.vi"/>
			<Item Name="MC Device Conf SPZ Conf Get_Set from_to file.vi" Type="VI" URL="../Source/Device Config/MC Device Conf SPZ Conf Get_Set from_to file.vi"/>
			<Item Name="MC Device Conf SPZ Get_Set Configuration.vi" Type="VI" URL="../Source/Device Config/MC Device Conf SPZ Get_Set Configuration.vi"/>
			<Item Name="MC Device Conf TE Conf Get_Set from_to file.vi" Type="VI" URL="../Source/Device Config/MC Device Conf TE Conf Get_Set from_to file.vi"/>
			<Item Name="MC Device Conf TE Get_Set Configuration.vi" Type="VI" URL="../Source/Device Config/MC Device Conf TE Get_Set Configuration.vi"/>
			<Item Name="MC Device ControlScan Configuration.ctl" Type="VI" URL="../Source/Device Config/MC Device ControlScan Configuration.ctl"/>
			<Item Name="MC Device DPOW Configuration.ctl" Type="VI" URL="../Source/Device Config/MC Device DPOW Configuration.ctl"/>
			<Item Name="MC Device FPZ Configuration.ctl" Type="VI" URL="../Source/Device Config/MC Device FPZ Configuration.ctl"/>
			<Item Name="MC Device Hardware Configuration.ctl" Type="VI" URL="../Source/Device Config/MC Device Hardware Configuration.ctl"/>
			<Item Name="MC Device PDH Configuration.ctl" Type="VI" URL="../Source/Device Config/MC Device PDH Configuration.ctl"/>
			<Item Name="MC Device PZETL Configuration.ctl" Type="VI" URL="../Source/Device Config/MC Device PZETL Configuration.ctl"/>
			<Item Name="MC Device RefCell Scan Configuration.ctl" Type="VI" URL="../Source/Device Config/MC Device RefCell Scan Configuration.ctl"/>
			<Item Name="MC Device Scan Configuration.ctl" Type="VI" URL="../Source/Device Config/MC Device Scan Configuration.ctl"/>
			<Item Name="MC Device SPZ Configuration.ctl" Type="VI" URL="../Source/Device Config/MC Device SPZ Configuration.ctl"/>
			<Item Name="MC Device TE Configuration.ctl" Type="VI" URL="../Source/Device Config/MC Device TE Configuration.ctl"/>
			<Item Name="MC Dialog About.vi" Type="VI" URL="../Source/Dialog/MC Dialog About.vi"/>
			<Item Name="MC Dialog Airy Calculate Fit.vi" Type="VI" URL="../Source/Stabilized/Sideoffringe/Airy Fit/MC Dialog Airy Calculate Fit.vi"/>
			<Item Name="MC Dialog Birefringent Scan Options.vi" Type="VI" URL="../Source/BiFi/MC Dialog Birefringent Scan Options.vi"/>
			<Item Name="MC Dialog Birefringent Scan.vi" Type="VI" URL="../Source/BiFi/MC Dialog Birefringent Scan.vi"/>
			<Item Name="MC Dialog Calibration Fit Calculate Fit.vi" Type="VI" URL="../Source/BiFi/Calibration Table Editor/fit/MC Dialog Calibration Fit Calculate Fit.vi"/>
			<Item Name="MC Dialog Calibration Table Edit Data Points.vi" Type="VI" URL="../Source/BiFi/Calibration Table Editor/MC Dialog Calibration Table Edit Data Points.vi"/>
			<Item Name="MC Dialog Calibration Table Edit Parameter.vi" Type="VI" URL="../Source/BiFi/Calibration Table Editor/MC Dialog Calibration Table Edit Parameter.vi"/>
			<Item Name="MC Dialog Calibration Table Editor.vi" Type="VI" URL="../Source/BiFi/Calibration Table Editor/MC Dialog Calibration Table Editor.vi"/>
			<Item Name="MC Dialog Calibration Table New Run BiFi Measurement.vi" Type="VI" URL="../Source/BiFi/Calibration Table Editor/MC Dialog Calibration Table New Run BiFi Measurement.vi"/>
			<Item Name="MC Dialog Calibration Table New Set Range.vi" Type="VI" URL="../Source/BiFi/Calibration Table Editor/MC Dialog Calibration Table New Set Range.vi"/>
			<Item Name="MC Dialog Calibration Table New Wavemeter Set Range.vi" Type="VI" URL="../Source/BiFi/Calibration Table Editor/MC Dialog Calibration Table New Wavemeter Set Range.vi"/>
			<Item Name="MC Dialog Configuration Load from INI File.vi" Type="VI" URL="../Source/Dialog/MC Dialog Configuration Load from INI File.vi"/>
			<Item Name="MC Dialog Configuration Save to INI File.vi" Type="VI" URL="../Source/Dialog/MC Dialog Configuration Save to INI File.vi"/>
			<Item Name="MC Dialog Control Loop Live View Options.vi" Type="VI" URL="../Source/Dialog/MC Dialog Control Loop Live View Options.vi"/>
			<Item Name="MC Dialog Control Loop Live View.vi" Type="VI" URL="../Source/Dialog/MC Dialog Control Loop Live View.vi"/>
			<Item Name="MC Dialog Control Low Level.vi" Type="VI" URL="../Source/Dialog/MC Dialog Control Low Level.vi"/>
			<Item Name="MC Dialog ControlScan Measurement.vi" Type="VI" URL="../Source/Slow Piezo/MC Dialog ControlScan Measurement.vi"/>
			<Item Name="MC Dialog Device Configuration Administration Get New PS Name.vi" Type="VI" URL="../Source/Dialog/MC Dialog Device Configuration Administration Get New PS Name.vi"/>
			<Item Name="MC Dialog Device Configuration Administration.vi" Type="VI" URL="../Source/Dialog/MC Dialog Device Configuration Administration.vi"/>
			<Item Name="MC Dialog Device Hardware Configuration.vi" Type="VI" URL="../Source/Dialog/MC Dialog Device Hardware Configuration.vi"/>
			<Item Name="MC Dialog Display Options.vi" Type="VI" URL="../Source/Dialog/MC Dialog Display Options.vi"/>
			<Item Name="MC Dialog Enable GUI.vi" Type="VI" URL="../Source/Dialog/MC Dialog Enable GUI.vi"/>
			<Item Name="MC Dialog EOM Control Setup.vi" Type="VI" URL="../Source/Stabilized/MC Dialog EOM Control Setup.vi"/>
			<Item Name="MC Dialog Error.vi" Type="VI" URL="../Source/Dialog/MC Dialog Error.vi"/>
			<Item Name="MC Dialog FPZ Control Setup.vi" Type="VI" URL="../Source/Stabilized/MC Dialog FPZ Control Setup.vi"/>
			<Item Name="MC Dialog FPZ Find Gain Options.vi" Type="VI" URL="../Source/FastPiezo/MC Dialog FPZ Find Gain Options.vi"/>
			<Item Name="MC Dialog FPZ Find Gain.vi" Type="VI" URL="../Source/FastPiezo/MC Dialog FPZ Find Gain.vi"/>
			<Item Name="MC Dialog Integral Diode Signal Monitor.vi" Type="VI" URL="../Source/Misc/MC Dialog Integral Diode Signal Monitor.vi"/>
			<Item Name="MC Dialog Interactive Command Shell.rtm" Type="Document" URL="../Source/QWin_Temp/Matisse/Matisse Commander/Source/Dialog/MC Dialog Interactive Command Shell.rtm"/>
			<Item Name="MC Dialog Interactive Command Shell.vi" Type="VI" URL="../Source/Dialog/MC Dialog Interactive Command Shell.vi"/>
			<Item Name="MC Dialog Motor Control Options.vi" Type="VI" URL="../Source/Dialog/MC Dialog Motor Control Options.vi"/>
			<Item Name="MC Dialog Motor Control.vi" Type="VI" URL="../Source/Dialog/MC Dialog Motor Control.vi"/>
			<Item Name="MC Dialog Motor Status Live View.vi" Type="VI" URL="../Source/Dialog/MC Dialog Motor Status Live View.vi"/>
			<Item Name="MC Dialog PDH Control Setup.vi" Type="VI" URL="../Source/Stabilized/MC Dialog PDH Control Setup.vi"/>
			<Item Name="MC Dialog PDH Error Function Measurement.vi" Type="VI" URL="../Source/Stabilized/MC Dialog PDH Error Function Measurement.vi"/>
			<Item Name="MC Dialog PDH Frequency Noise.vi" Type="VI" URL="../Source/Stabilized/MC Dialog PDH Frequency Noise.vi"/>
			<Item Name="MC Dialog PDH Waveforms.vi" Type="VI" URL="../Source/Stabilized/MC Dialog PDH Waveforms.vi"/>
			<Item Name="MC Dialog Plug-Ins.vi" Type="VI" URL="../Source/Dialog/MC Dialog Plug-Ins.vi"/>
			<Item Name="MC Dialog Plugin Initialization.vi" Type="VI" URL="../Source/Plugin/MC Dialog Plugin Initialization.vi"/>
			<Item Name="MC Dialog PZETL Control Setup.vi" Type="VI" URL="../Source/PZETL/MC Dialog PZETL Control Setup.vi"/>
			<Item Name="MC Dialog PZETL Waveform.vi" Type="VI" URL="../Source/PZETL/MC Dialog PZETL Waveform.vi"/>
			<Item Name="MC Dialog RefCell Frequency Noise.vi" Type="VI" URL="../Source/Stabilized/MC Dialog RefCell Frequency Noise.vi"/>
			<Item Name="MC Dialog RefCell Spectrum Analysis with Fit.vi" Type="VI" URL="../Source/Stabilized/MC Dialog RefCell Spectrum Analysis with Fit.vi"/>
			<Item Name="MC Dialog RefCell Spectrum Measurement with Fit.vi" Type="VI" URL="../Source/Stabilized/MC Dialog RefCell Spectrum Measurement with Fit.vi"/>
			<Item Name="MC Dialog RefCell Waveform.vi" Type="VI" URL="../Source/Stabilized/MC Dialog RefCell Waveform.vi"/>
			<Item Name="MC Dialog Scan Device Configuration.vi" Type="VI" URL="../Source/Slow Piezo/MC Dialog Scan Device Configuration.vi"/>
			<Item Name="MC Dialog Scan Setup.vi" Type="VI" URL="../Source/Slow Piezo/MC Dialog Scan Setup.vi"/>
			<Item Name="MC Dialog SPZ Control Setup.vi" Type="VI" URL="../Source/Slow Piezo/MC Dialog SPZ Control Setup.vi"/>
			<Item Name="MC Dialog TE Control Setup.vi" Type="VI" URL="../Source/ThinEtalon/MC Dialog TE Control Setup.vi"/>
			<Item Name="MC Dialog TE Scan Options.vi" Type="VI" URL="../Source/ThinEtalon/MC Dialog TE Scan Options.vi"/>
			<Item Name="MC Dialog TE Scan.vi" Type="VI" URL="../Source/ThinEtalon/MC Dialog TE Scan.vi"/>
			<Item Name="MC Dialog TE Signal Monitor.vi" Type="VI" URL="../Source/ThinEtalon/MC Dialog TE Signal Monitor.vi"/>
			<Item Name="MC Display BiFi.vi" Type="VI" URL="../Source/BiFi/MC Display BiFi.vi"/>
			<Item Name="MC Display EOM Update.vi" Type="VI" URL="../Source/Main Panel/MC Display EOM Update.vi"/>
			<Item Name="MC Display FastPiezo.vi" Type="VI" URL="../Source/FastPiezo/MC Display FastPiezo.vi"/>
			<Item Name="MC Display Integral Diode Update.vi" Type="VI" URL="../Source/Main Panel/MC Display Integral Diode Update.vi"/>
			<Item Name="MC Display PZETL.vi" Type="VI" URL="../Source/PZETL/MC Display PZETL.vi"/>
			<Item Name="MC Display RefCell.vi" Type="VI" URL="../Source/Stabilized/MC Display RefCell.vi"/>
			<Item Name="MC Display Scan Device.vi" Type="VI" URL="../Source/Main Panel/MC Display Scan Device.vi"/>
			<Item Name="MC Display Set Serial Number in Header.vi" Type="VI" URL="../Source/Misc/MC Display Set Serial Number in Header.vi"/>
			<Item Name="MC Display Set Status.vi" Type="VI" URL="../Source/Misc/MC Display Set Status.vi"/>
			<Item Name="MC Display Slow Piezo.vi" Type="VI" URL="../Source/Misc/MC Display Slow Piezo.vi"/>
			<Item Name="MC Display TE.vi" Type="VI" URL="../Source/ThinEtalon/MC Display TE.vi"/>
			<Item Name="MC EOM Set Color.vi" Type="VI" URL="../Source/EOM/MC EOM Set Color.vi"/>
			<Item Name="MC FastPiezo Integral Gain Measurement.vi" Type="VI" URL="../Source/FastPiezo/MC FastPiezo Integral Gain Measurement.vi"/>
			<Item Name="MC FastPiezo Set Color.vi" Type="VI" URL="../Source/FastPiezo/MC FastPiezo Set Color.vi"/>
			<Item Name="MC FastPiezo Set Status.vi" Type="VI" URL="../Source/FastPiezo/MC FastPiezo Set Status.vi"/>
			<Item Name="MC File Dialog Get Device Configuration File Name.vi" Type="VI" URL="../Source/Dialog/MC File Dialog Get Device Configuration File Name.vi"/>
			<Item Name="MC FPZ Measure Gain Analyze Gain.vi" Type="VI" URL="../Source/FastPiezo/MC FPZ Measure Gain Analyze Gain.vi"/>
			<Item Name="MC FPZ Measure Gain From.vi" Type="VI" URL="../Source/FastPiezo/MC FPZ Measure Gain From.vi"/>
			<Item Name="MC FPZ Measure Gain Inc.vi" Type="VI" URL="../Source/FastPiezo/MC FPZ Measure Gain Inc.vi"/>
			<Item Name="MC FPZ Measure Gain Safety Margin.vi" Type="VI" URL="../Source/FastPiezo/MC FPZ Measure Gain Safety Margin.vi"/>
			<Item Name="MC FPZ Measure Gain Threshold.vi" Type="VI" URL="../Source/FastPiezo/MC FPZ Measure Gain Threshold.vi"/>
			<Item Name="MC FPZ Measure Gain To.vi" Type="VI" URL="../Source/FastPiezo/MC FPZ Measure Gain To.vi"/>
			<Item Name="MC Get Configuration.vi" Type="VI" URL="../Source/Misc/MC Get Configuration.vi"/>
			<Item Name="MC Get Network Server Ressource.vi" Type="VI" URL="../Source/Misc/MC Get Network Server Ressource.vi"/>
			<Item Name="MC Get Set Scan Control Parameter.vi" Type="VI" URL="../Source/Misc/MC Get Set Scan Control Parameter.vi"/>
			<Item Name="MC Get VISA Ressource.vi" Type="VI" URL="../Source/Misc/MC Get VISA Ressource.vi"/>
			<Item Name="MC Global Bifi Parameter.vi" Type="VI" URL="../Source/BiFi/MC Global Bifi Parameter.vi"/>
			<Item Name="MC Init Clear Error Code.vi" Type="VI" URL="../Source/Misc/MC Init Clear Error Code.vi"/>
			<Item Name="MC Init Network Server.vi" Type="VI" URL="../Source/Misc/MC Init Network Server.vi"/>
			<Item Name="MC Init Show current action.vi" Type="VI" URL="../Source/Misc/MC Init Show current action.vi"/>
			<Item Name="MC Init.vi" Type="VI" URL="../Source/Misc/MC Init.vi"/>
			<Item Name="MC Integral Diode Set Color.vi" Type="VI" URL="../Source/Integral Diode/MC Integral Diode Set Color.vi"/>
			<Item Name="MC Interactive Command Shell Execute.vi" Type="VI" URL="../Source/Misc/MC Interactive Command Shell Execute.vi"/>
			<Item Name="MC Laser Matisse Dummy.vi" Type="VI" URL="../Source/Misc/MC Laser Matisse Dummy.vi"/>
			<Item Name="MC Laser Type.vi" Type="VI" URL="../Source/Misc/MC Laser Type.vi"/>
			<Item Name="MC Load Hardware Configuration.vi" Type="VI" URL="../Source/Misc/MC Load Hardware Configuration.vi"/>
			<Item Name="MC Load Stab Menu.vi" Type="VI" URL="../Source/Stabilized/MC Load Stab Menu.vi"/>
			<Item Name="MC Main Create Dir.vi" Type="VI" URL="../Source/Misc/MC Main Create Dir.vi"/>
			<Item Name="MC Main INI File.vi" Type="VI" URL="../Source/Misc/MC Main INI File.vi"/>
			<Item Name="MC Menu Adapt to Laser Configuration.vi" Type="VI" URL="../Source/Misc/MC Menu Adapt to Laser Configuration.vi"/>
			<Item Name="MC Message Dialog Shutdown Error.vi" Type="VI" URL="../Source/Dialog/MC Message Dialog Shutdown Error.vi"/>
			<Item Name="MC Misc Save To Parameter Set.vi" Type="VI" URL="../Source/Misc/MC Misc Save To Parameter Set.vi"/>
			<Item Name="MC Motor Control Options Get_Set.vi" Type="VI" URL="../Source/Misc/MC Motor Control Options Get_Set.vi"/>
			<Item Name="MC Open Help Window.vi" Type="VI" URL="../Source/Misc/MC Open Help Window.vi"/>
			<Item Name="MC Optical Elements Position Get_Set.vi" Type="VI" URL="../Source/Misc/MC Optical Elements Position Get_Set.vi"/>
			<Item Name="MC Panel All Cluster References.ctl" Type="VI" URL="../Source/Main Panel/MC Panel All Cluster References.ctl"/>
			<Item Name="MC Panel Birefringent Filter Control References.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Birefringent Filter Control References.ctl"/>
			<Item Name="MC Panel Birefringent Filter Control.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Birefringent Filter Control.ctl"/>
			<Item Name="MC Panel Button Enable Scanning.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Button Enable Scanning.ctl"/>
			<Item Name="MC Panel Button Locked.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Button Locked.ctl"/>
			<Item Name="MC Panel Button Scan Device Control.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Button Scan Device Control.ctl"/>
			<Item Name="MC Panel Enumerator Scan Device.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Enumerator Scan Device.ctl"/>
			<Item Name="MC Panel EOM Cluster Reference.ctl" Type="VI" URL="../Source/Main Panel/MC Panel EOM Cluster Reference.ctl"/>
			<Item Name="MC Panel EOM Control References.ctl" Type="VI" URL="../Source/Main Panel/MC Panel EOM Control References.ctl"/>
			<Item Name="MC Panel EOM Control.ctl" Type="VI" URL="../Source/Main Panel/MC Panel EOM Control.ctl"/>
			<Item Name="MC Panel Extract Control References.vi" Type="VI" URL="../Source/Main Panel/MC Panel Extract Control References.vi"/>
			<Item Name="MC Panel Fast Piezo Cluster Reference.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Fast Piezo Cluster Reference.ctl"/>
			<Item Name="MC Panel Fast Piezo Control References.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Fast Piezo Control References.ctl"/>
			<Item Name="MC Panel Fast Piezo Control.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Fast Piezo Control.ctl"/>
			<Item Name="MC Panel Integral Power Diode Cluster Reference.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Integral Power Diode Cluster Reference.ctl"/>
			<Item Name="MC Panel Integral Power Diode Control References.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Integral Power Diode Control References.ctl"/>
			<Item Name="MC Panel Integral Power Diode Control.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Integral Power Diode Control.ctl"/>
			<Item Name="MC Panel Manage Control References.vi" Type="VI" URL="../Source/Main Panel/MC Panel Manage Control References.vi"/>
			<Item Name="MC Panel Piezo Etalon Cluster Reference.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Piezo Etalon Cluster Reference.ctl"/>
			<Item Name="MC Panel Piezo Etalon Control References.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Piezo Etalon Control References.ctl"/>
			<Item Name="MC Panel Piezo Etalon Control.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Piezo Etalon Control.ctl"/>
			<Item Name="MC Panel Ray_ThinEtalon Cluster Reference.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Ray_ThinEtalon Cluster Reference.ctl"/>
			<Item Name="MC Panel Ray_ThinEtalon.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Ray_ThinEtalon.ctl"/>
			<Item Name="MC Panel RefCell Right Click Menu.vi" Type="VI" URL="../Source/Main Panel/MC Panel RefCell Right Click Menu.vi"/>
			<Item Name="MC Panel Reference Cell Cluster Reference.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Reference Cell Cluster Reference.ctl"/>
			<Item Name="MC Panel Reference Cell Control References.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Reference Cell Control References.ctl"/>
			<Item Name="MC Panel Reference Cell Control.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Reference Cell Control.ctl"/>
			<Item Name="MC Panel Ring RefCell1 Cluster Reference.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Ring RefCell1 Cluster Reference.ctl"/>
			<Item Name="MC Panel Ring RefCell2 Cluster Reference.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Ring RefCell2 Cluster Reference.ctl"/>
			<Item Name="MC Panel Ring_RefCell1.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Ring_RefCell1.ctl"/>
			<Item Name="MC Panel Ring_RefCell2.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Ring_RefCell2.ctl"/>
			<Item Name="MC Panel Scan Device Cluster Reference.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Scan Device Cluster Reference.ctl"/>
			<Item Name="MC Panel Scan Device Control References.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Scan Device Control References.ctl"/>
			<Item Name="MC Panel Scan Device Control.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Scan Device Control.ctl"/>
			<Item Name="MC Panel Slow Piezo Cluster Reference.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Slow Piezo Cluster Reference.ctl"/>
			<Item Name="MC Panel Slow Piezo Control References.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Slow Piezo Control References.ctl"/>
			<Item Name="MC Panel Slow Piezo Control.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Slow Piezo Control.ctl"/>
			<Item Name="MC Panel Sorted Controls.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Sorted Controls.ctl"/>
			<Item Name="MC Panel Thin Etalon Cluster Reference.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Thin Etalon Cluster Reference.ctl"/>
			<Item Name="MC Panel Thin Etalon Control References.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Thin Etalon Control References.ctl"/>
			<Item Name="MC Panel Thin Etalon Control.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Thin Etalon Control.ctl"/>
			<Item Name="MC Panel Thin Etalon Diode Cluster Reference.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Thin Etalon Diode Cluster Reference.ctl"/>
			<Item Name="MC Panel Thin Etalon Power Diode Control.ctl" Type="VI" URL="../Source/Main Panel/MC Panel Thin Etalon Power Diode Control.ctl"/>
			<Item Name="MC PDH Frequency Noise Options Get_Set.vi" Type="VI" URL="../Source/Stabilized/MC PDH Frequency Noise Options Get_Set.vi"/>
			<Item Name="MC PDH Reset PDH Values.vi" Type="VI" URL="../Source/Stabilized/MC PDH Reset PDH Values.vi"/>
			<Item Name="MC PDH Set Status.vi" Type="VI" URL="../Source/Stabilized/MC PDH Set Status.vi"/>
			<Item Name="MC PDH Waveforms Cursor Move.vi" Type="VI" URL="../Source/Stabilized/MC PDH Waveforms Cursor Move.vi"/>
			<Item Name="MC Peak Analysis.vi" Type="VI" URL="../Source/Misc/MC Peak Analysis.vi"/>
			<Item Name="MC PID Loop Snashot.vi" Type="VI" URL="../Source/Misc/MC PID Loop Snashot.vi"/>
			<Item Name="MC Plugin Check to load plugin.vi" Type="VI" URL="../Source/Plugin/MC Plugin Check to load plugin.vi"/>
			<Item Name="MC Plugin Close.vi" Type="VI" URL="../Source/Plugin/MC Plugin Close.vi"/>
			<Item Name="MC Plugin Initialize.vi" Type="VI" URL="../Source/Plugin/MC Plugin Initialize.vi"/>
			<Item Name="MC Plugin Load Application Library.vi" Type="VI" URL="../Source/Plugin/MC Plugin Load Application Library.vi"/>
			<Item Name="MC Plugin Process Command.vi" Type="VI" URL="../Source/Plugin/MC Plugin Process Command.vi"/>
			<Item Name="MC Plugin Reference Global LV2.vi" Type="VI" URL="../Source/Plugin/MC Plugin Reference Global LV2.vi"/>
			<Item Name="MC Position Unit.ctl" Type="VI" URL="../Source/Misc/MC Position Unit.ctl"/>
			<Item Name="MC Protocol Add New Command.vi" Type="VI" URL="../Source/Protocol Window/MC Protocol Add New Command.vi"/>
			<Item Name="MC Protocol Array.ctl" Type="VI" URL="../Source/Protocol Window/MC Protocol Array.ctl"/>
			<Item Name="MC Protocol Command.ctl" Type="VI" URL="../Source/Protocol Window/MC Protocol Command.ctl"/>
			<Item Name="MC Protocol New Command.vi" Type="VI" URL="../Source/Protocol Window/MC Protocol New Command.vi"/>
			<Item Name="MC PZETL Set Color.vi" Type="VI" URL="../Source/PZETL/MC PZETL Set Color.vi"/>
			<Item Name="MC RefCell Control Loop Status Set.vi" Type="VI" URL="../Source/Stabilized/MC RefCell Control Loop Status Set.vi"/>
			<Item Name="MC RefCell Frequency Noise Options Get_Set.vi" Type="VI" URL="../Source/Stabilized/MC RefCell Frequency Noise Options Get_Set.vi"/>
			<Item Name="MC RefCell Measure Spectrum with Fit.vi" Type="VI" URL="../Source/Stabilized/MC RefCell Measure Spectrum with Fit.vi"/>
			<Item Name="MC RefCell Measure Spectrum.vi" Type="VI" URL="../Source/Stabilized/MC RefCell Measure Spectrum.vi"/>
			<Item Name="MC RefCell Piezo Set Color.vi" Type="VI" URL="../Source/RefCell/MC RefCell Piezo Set Color.vi"/>
			<Item Name="MC Runtime Menu.rtm" Type="Document" URL="../MC Runtime Menu.rtm"/>
			<Item Name="MC Scan Check Limits.vi" Type="VI" URL="../Source/Slow Piezo/MC Scan Check Limits.vi"/>
			<Item Name="MC Scan Control.ctl" Type="VI" URL="../Source/Slow Piezo/MC Scan Control.ctl"/>
			<Item Name="MC Scan Device Claibration Device Measurement Options Get_Set.vi" Type="VI" URL="../Source/Slow Piezo/MC Scan Device Claibration Device Measurement Options Get_Set.vi"/>
			<Item Name="MC Scan Device Set Color.vi" Type="VI" URL="../Source/Scan Device/MC Scan Device Set Color.vi"/>
			<Item Name="MC Scan Direction Arrow.ctl" Type="VI" URL="../Source/MC Dialog Controls/MC Scan Direction Arrow.ctl"/>
			<Item Name="MC Scan Get Current Settings.vi" Type="VI" URL="../Source/Slow Piezo/MC Scan Get Current Settings.vi"/>
			<Item Name="MC Scan Load Laser Scan Parameter.vi" Type="VI" URL="../Source/Slow Piezo/MC Scan Load Laser Scan Parameter.vi"/>
			<Item Name="MC Scan Mode.ctl" Type="VI" URL="../Source/Slow Piezo/MC Scan Mode.ctl"/>
			<Item Name="MC Scan Parameter.ctl" Type="VI" URL="../Source/Slow Piezo/MC Scan Parameter.ctl"/>
			<Item Name="MC Scan Perform Scan.vi" Type="VI" URL="../Source/Slow Piezo/MC Scan Perform Scan.vi"/>
			<Item Name="MC Scan Range Half.vi" Type="VI" URL="../Source/Misc/MC Scan Range Half.vi"/>
			<Item Name="MC Scan Read Config.vi" Type="VI" URL="../Source/Slow Piezo/MC Scan Read Config.vi"/>
			<Item Name="MC Scan Save Laser Scan Parameter.vi" Type="VI" URL="../Source/Slow Piezo/MC Scan Save Laser Scan Parameter.vi"/>
			<Item Name="MC Scan Scan Control to ID.vi" Type="VI" URL="../Source/Slow Piezo/MC Scan Scan Control to ID.vi"/>
			<Item Name="MC Scan Set Current Settings.vi" Type="VI" URL="../Source/Slow Piezo/MC Scan Set Current Settings.vi"/>
			<Item Name="MC Scan Set Position for Device.vi" Type="VI" URL="../Source/Slow Piezo/MC Scan Set Position for Device.vi"/>
			<Item Name="MC Scan Set Status.vi" Type="VI" URL="../Source/Slow Piezo/MC Scan Set Status.vi"/>
			<Item Name="MC Scan Setup Get_Set.vi" Type="VI" URL="../Source/Slow Piezo/MC Scan Setup Get_Set.vi"/>
			<Item Name="MC Scan TE v Biref DPOW_DC TE_DC.vi" Type="VI" URL="../Source/Misc/MC Scan TE v Biref DPOW_DC TE_DC.vi"/>
			<Item Name="MC Scan Write Config.vi" Type="VI" URL="../Source/Slow Piezo/MC Scan Write Config.vi"/>
			<Item Name="MC Set Color Definitions.vi" Type="VI" URL="../Source/Misc/MC Set Color Definitions.vi"/>
			<Item Name="MC Set Menu After.vi" Type="VI" URL="../Source/Misc/MC Set Menu After.vi"/>
			<Item Name="MC Shutdown Actions.vi" Type="VI" URL="../Source/Misc/MC Shutdown Actions.vi"/>
			<Item Name="MC Slider Color.ctl" Type="VI" URL="../Source/MC Dialog Controls/MC Slider Color.ctl"/>
			<Item Name="MC Slow Piezo Set Color.vi" Type="VI" URL="../Source/Slow Piezo/MC Slow Piezo Set Color.vi"/>
			<Item Name="MC Splash Screen Global.vi" Type="VI" URL="../Source/Globals/MC Splash Screen Global.vi"/>
			<Item Name="MC TE Control Goal Options Get_Set.vi" Type="VI" URL="../Source/ThinEtalon/MC TE Control Goal Options Get_Set.vi"/>
			<Item Name="MC TE Scan Command.ctl" Type="VI" URL="../Source/ThinEtalon/MC TE Scan Command.ctl"/>
			<Item Name="MC TE Set Color.vi" Type="VI" URL="../Source/ThinEtalon/MC TE Set Color.vi"/>
			<Item Name="MC TE Set Control Goal.vi" Type="VI" URL="../Source/ThinEtalon/MC TE Set Control Goal.vi"/>
			<Item Name="MC TX EOM Status Set.vi" Type="VI" URL="../Source/Slow Piezo/MC TX EOM Status Set.vi"/>
			<Item Name="MC Update Add Protocol Entry.vi" Type="VI" URL="../Source/Updater/MC Update Add Protocol Entry.vi"/>
			<Item Name="MC Update AutoFit BiFi Calibration.vi" Type="VI" URL="../Source/Updater/MC Update AutoFit BiFi Calibration.vi"/>
			<Item Name="MC Update Built Command.vi" Type="VI" URL="../Source/Updater/MC Update Built Command.vi"/>
			<Item Name="MC Update Built Loop.vi" Type="VI" URL="../Source/Updater/MC Update Built Loop.vi"/>
			<Item Name="MC Update Calc Checksum.vi" Type="VI" URL="../Source/Updater/MC Update Calc Checksum.vi"/>
			<Item Name="MC Update Calc curve From Fit Data.vi" Type="VI" URL="../Source/Updater/MC Update Calc curve From Fit Data.vi"/>
			<Item Name="MC Update Check for update.vi" Type="VI" URL="../Source/Updater/MC Update Check for update.vi"/>
			<Item Name="MC Update Convert Serial String To Double.vi" Type="VI" URL="../Source/Updater/MC Update Convert Serial String To Double.vi"/>
			<Item Name="MC Update Create Directory.vi" Type="VI" URL="../Source/Updater/MC Update Create Directory.vi"/>
			<Item Name="MC Update Dialog Disclaimer.vi" Type="VI" URL="../Source/Updater/MC Update Dialog Disclaimer.vi"/>
			<Item Name="MC Update Get Firmware Date.vi" Type="VI" URL="../Source/Updater/MC Update Get Firmware Date.vi"/>
			<Item Name="MC Update Get Firmware Version.vi" Type="VI" URL="../Source/Updater/MC Update Get Firmware Version.vi"/>
			<Item Name="MC Update Get Protocol Path.vi" Type="VI" URL="../Source/Updater/MC Update Get Protocol Path.vi"/>
			<Item Name="MC Update Handle Cancel Button.vi" Type="VI" URL="../Source/Updater/MC Update Handle Cancel Button.vi"/>
			<Item Name="MC Update Main Dialog.vi" Type="VI" URL="../Source/Updater/MC Update Main Dialog.vi"/>
			<Item Name="MC Update Program States.ctl" Type="VI" URL="../Source/Updater/MC Update Program States.ctl"/>
			<Item Name="MC Update Reset Laser.vi" Type="VI" URL="../Source/Updater/MC Update Reset Laser.vi"/>
			<Item Name="MC Update Reset Update Memory.vi" Type="VI" URL="../Source/Updater/MC Update Reset Update Memory.vi"/>
			<Item Name="MC Update Save New Fitted Data.vi" Type="VI" URL="../Source/Updater/MC Update Save New Fitted Data.vi"/>
			<Item Name="MC Update Send Command Low Level.vi" Type="VI" URL="../Source/Updater/MC Update Send Command Low Level.vi"/>
			<Item Name="MC Update Simple Error Handler.vi" Type="VI" URL="../Source/Updater/MC Update Simple Error Handler.vi"/>
			<Item Name="MC Update Test Checksum.vi" Type="VI" URL="../Source/Updater/MC Update Test Checksum.vi"/>
			<Item Name="MC Update Test Update Possible.vi" Type="VI" URL="../Source/Updater/MC Update Test Update Possible.vi"/>
			<Item Name="MC Update Update possible.vi" Type="VI" URL="../Source/Updater/MC Update Update possible.vi"/>
			<Item Name="MC Utility Check for firmware update.vi" Type="VI" URL="../Source/Utility/MC Utility Check for firmware update.vi"/>
			<Item Name="MC Utility End Program.vi" Type="VI" URL="../Source/Utility/MC Utility End Program.vi"/>
			<Item Name="MC Utility Get Supercaller Reference.vi" Type="VI" URL="../Source/Utility/MC Utility Get Supercaller Reference.vi"/>
			<Item Name="MC Utility Position Display Global.vi" Type="VI" URL="../Source/Utility/MC Utility Position Display Global.vi"/>
			<Item Name="MC Utility Protocol Left axis autoscale.vi" Type="VI" URL="../Source/Utility/MC Utility Protocol Left axis autoscale.vi"/>
			<Item Name="MC Utility Protocol Period.vi" Type="VI" URL="../Source/Utility/MC Utility Protocol Period.vi"/>
			<Item Name="MC Utility Protocol Right axis autoscale.vi" Type="VI" URL="../Source/Utility/MC Utility Protocol Right axis autoscale.vi"/>
			<Item Name="MC Utility Slider Fill Color.vi" Type="VI" URL="../Source/Utility/MC Utility Slider Fill Color.vi"/>
			<Item Name="MC Utility.dll" Type="Document" URL="../Dependent/MC Utility.dll"/>
			<Item Name="MC VISA Resource Global LV2.vi" Type="VI" URL="../Source/Globals/MC VISA Resource Global LV2.vi"/>
			<Item Name="MC Window Manager.vi" Type="VI" URL="../Source/Winmanager/MC Window Manager.vi"/>
			<Item Name="MC WinMan Close Window.vi" Type="VI" URL="../Source/Winmanager/MC WinMan Close Window.vi"/>
			<Item Name="MC WinMan Initialize Windows.vi" Type="VI" URL="../Source/Winmanager/MC WinMan Initialize Windows.vi"/>
			<Item Name="MC WinMan Open Window.vi" Type="VI" URL="../Source/Winmanager/MC WinMan Open Window.vi"/>
			<Item Name="MC WinMan VI References Global LV2.vi" Type="VI" URL="../Source/Winmanager/MC WinMan VI References Global LV2.vi"/>
			<Item Name="MC WinMan VI Status Get.vi" Type="VI" URL="../Source/Winmanager/MC WinMan VI Status Get.vi"/>
			<Item Name="MC WinMan VI Status Set.vi" Type="VI" URL="../Source/Winmanager/MC WinMan VI Status Set.vi"/>
			<Item Name="MC WinMan VI Window Borders Load from Config.vi" Type="VI" URL="../Source/Winmanager/MC WinMan VI Window Borders Load from Config.vi"/>
			<Item Name="MC WinMan VI Window Borders Write to Config.vi" Type="VI" URL="../Source/Winmanager/MC WinMan VI Window Borders Write to Config.vi"/>
			<Item Name="MC WinMan Wait For Asynchronous Windows.vi" Type="VI" URL="../Source/Winmanager/MC WinMan Wait For Asynchronous Windows.vi"/>
			<Item Name="MCP WM Get Value.ctl" Type="VI" URL="../Dependent/Plug-Ins/MCP Wavemeter.llb/MCP WM Get Value.ctl"/>
			<Item Name="MCP WM Measured.ctl" Type="VI" URL="../Dependent/Plug-Ins/MCP Wavemeter.llb/MCP WM Measured.ctl"/>
			<Item Name="MCP WM Read Wavemeter.vi" Type="VI" URL="../Dependent/Plug-Ins/MCP Wavemeter.llb/MCP WM Read Wavemeter.vi"/>
			<Item Name="MCP WM Unit.ctl" Type="VI" URL="../Dependent/Plug-Ins/MCP Wavemeter.llb/MCP WM Unit.ctl"/>
			<Item Name="Optical Elements Position.ctl" Type="VI" URL="../Source/Misc/Optical Elements Position.ctl"/>
			<Item Name="Pick Active Datasets.vi" Type="VI" URL="../Source/Protocol Window/Pick Active Datasets.vi"/>
			<Item Name="Read Protocol Data.vi" Type="VI" URL="../Source/Protocol Window/Read Protocol Data.vi"/>
			<Item Name="SC FCU Dialog Fit.rtm" Type="Document" URL="../Source/FCU/SC FCU Dialog Fit/SC FCU Dialog Fit.rtm"/>
			<Item Name="Set Up Protocol Graph.vi" Type="VI" URL="../Source/Protocol Window/Set Up Protocol Graph.vi"/>
			<Item Name="SVI_DeltaF_to_DeltaWL.vi" Type="VI" URL="../SVI_DeltaF_to_DeltaWL.vi"/>
			<Item Name="SVI_OLD_PID_I_Contribution.vi" Type="VI" URL="../SVI_OLD_PID_I_Contribution.vi"/>
			<Item Name="SVI_PowerStats.vi" Type="VI" URL="../SVI_PowerStats.vi"/>
			<Item Name="TypeDef_LockingInfo.ctl" Type="VI" URL="../TypeDef_LockingInfo.ctl"/>
			<Item Name="TypeDef_QueueCommand.ctl" Type="VI" URL="../TypeDef_QueueCommand.ctl"/>
			<Item Name="wlmData.dll" Type="Document" URL="../../../Dropbox (The University of Manchester)/AMP Group/Software/MatisseCommander/ReadWavemeter/wlmData.dll"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Matisse Commander" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{20E255E0-1D81-424C-AAF2-32EDB20859F1}</Property>
				<Property Name="App_INI_GUID" Type="Str">{C8246049-FA64-4208-8BA0-B95169833D2B}</Property>
				<Property Name="App_INI_itemID" Type="Ref">/My Computer/Matisse Commander.ini</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_waitDebugging" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{089CD461-F5C5-4442-A9A5-9148F1220883}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Matisse Commander</Property>
				<Property Name="Bld_compilerOptLevel" Type="Int">0</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../Redist/Executable</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{8E3BF5DD-AFAA-4A3B-B341-C78DF02652DE}</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">English</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">1</Property>
				<Property Name="Bld_version.build" Type="Int">18</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Bld_version.minor" Type="Int">16</Property>
				<Property Name="Destination[0].destName" Type="Str">Matisse Commander.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../Redist/Executable/NI_AB_PROJECTNAME</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../Redist/Executable</Property>
				<Property Name="Destination[1].path.type" Type="Str">relativeToProject</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/Sirah MC.ico</Property>
				<Property Name="Source[0].itemID" Type="Str">{71FBF4A7-F693-4FC5-8933-A6F14E5DE7E4}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref"></Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Matisse Commander Help.chm</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/MC Splash Screen.vi</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[3].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">4</Property>
				<Property Name="TgtF_companyName" Type="Str">Sirah Lasertechnik GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Matisse Commander</Property>
				<Property Name="TgtF_internalName" Type="Str">Matisse Commander</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">© 2015 Sirah Lasertechnik</Property>
				<Property Name="TgtF_productName" Type="Str">Matisse Commander</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{E9B61120-328B-47C3-AA9F-1D55DBA2174D}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Matisse Commander.exe</Property>
			</Item>
			<Item Name="Matisse Commander Installer" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">Sirah</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{7E4A95BB-B299-4910-AE05-FD4F3F514D64}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="Destination[1].name" Type="Str">Matisse Commander</Property>
				<Property Name="Destination[1].parent" Type="Str">{7E4A95BB-B299-4910-AE05-FD4F3F514D64}</Property>
				<Property Name="Destination[1].tag" Type="Str">{4129A2AE-7654-46C1-B689-84558664DEA7}</Property>
				<Property Name="Destination[1].type" Type="Str">userFolder</Property>
				<Property Name="Destination[2].name" Type="Str">Plug-ins</Property>
				<Property Name="Destination[2].parent" Type="Str">{4129A2AE-7654-46C1-B689-84558664DEA7}</Property>
				<Property Name="Destination[2].tag" Type="Str">{A70D8511-3A56-462F-ABAE-68ECC3E3F11A}</Property>
				<Property Name="Destination[2].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">3</Property>
				<Property Name="DistPart[0].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[0].productID" Type="Str">{331B6132-FA2A-458D-828D-B0DA0237739A}</Property>
				<Property Name="DistPart[0].productName" Type="Str">NI LabVIEW Runtime 2014 SP1 f3</Property>
				<Property Name="DistPart[0].SoftDep[0].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[0].productName" Type="Str">NI LabVIEW Runtime 2014 SP1 Non-English Support.</Property>
				<Property Name="DistPart[0].SoftDep[0].upgradeCode" Type="Str">{CAC8FA79-6D3D-4263-BB7B-1A706EF87C08}</Property>
				<Property Name="DistPart[0].SoftDep[1].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[1].productName" Type="Str">NI ActiveX Container</Property>
				<Property Name="DistPart[0].SoftDep[1].upgradeCode" Type="Str">{1038A887-23E1-4289-B0BD-0C4B83C6BA21}</Property>
				<Property Name="DistPart[0].SoftDep[10].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[10].productName" Type="Str">NI VC2010MSMs</Property>
				<Property Name="DistPart[0].SoftDep[10].upgradeCode" Type="Str">{EFBA6F9E-F934-4BD7-AC51-60CCA480489C}</Property>
				<Property Name="DistPart[0].SoftDep[11].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[11].productName" Type="Str">NI mDNS Responder 14.0</Property>
				<Property Name="DistPart[0].SoftDep[11].upgradeCode" Type="Str">{9607874B-4BB3-42CB-B450-A2F5EF60BA3B}</Property>
				<Property Name="DistPart[0].SoftDep[12].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[12].productName" Type="Str">NI Deployment Framework 2014</Property>
				<Property Name="DistPart[0].SoftDep[12].upgradeCode" Type="Str">{838942E4-B73C-492E-81A3-AA1E291FD0DC}</Property>
				<Property Name="DistPart[0].SoftDep[13].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[13].productName" Type="Str">NI Error Reporting 2014</Property>
				<Property Name="DistPart[0].SoftDep[13].upgradeCode" Type="Str">{42E818C6-2B08-4DE7-BD91-B0FD704C119A}</Property>
				<Property Name="DistPart[0].SoftDep[2].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[2].productName" Type="Str">NI Service Locator 14.0</Property>
				<Property Name="DistPart[0].SoftDep[2].upgradeCode" Type="Str">{B235B862-6A92-4A04-A8B2-6D71F777DE67}</Property>
				<Property Name="DistPart[0].SoftDep[3].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[3].productName" Type="Str">NI System Web Server 14.5</Property>
				<Property Name="DistPart[0].SoftDep[3].upgradeCode" Type="Str">{FCF64B73-B7D4-4971-8F11-24BAF7CC3E6C}</Property>
				<Property Name="DistPart[0].SoftDep[4].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[4].productName" Type="Str">Math Kernel Libraries</Property>
				<Property Name="DistPart[0].SoftDep[4].upgradeCode" Type="Str">{3BDD0408-2F90-4B42-9777-5ED1D4BE67A8}</Property>
				<Property Name="DistPart[0].SoftDep[5].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[5].productName" Type="Str">NI Logos 14.0</Property>
				<Property Name="DistPart[0].SoftDep[5].upgradeCode" Type="Str">{5E4A4CE3-4D06-11D4-8B22-006008C16337}</Property>
				<Property Name="DistPart[0].SoftDep[6].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[6].productName" Type="Str">NI TDM Streaming 14.0</Property>
				<Property Name="DistPart[0].SoftDep[6].upgradeCode" Type="Str">{4CD11BE6-6BB7-4082-8A27-C13771BC309B}</Property>
				<Property Name="DistPart[0].SoftDep[7].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[7].productName" Type="Str">NI LabVIEW Web Server 2014</Property>
				<Property Name="DistPart[0].SoftDep[7].upgradeCode" Type="Str">{4A8BDBBB-DA1C-45C9-8563-74C034FBD357}</Property>
				<Property Name="DistPart[0].SoftDep[8].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[8].productName" Type="Str">NI LabVIEW Real-Time NBFifo 2014</Property>
				<Property Name="DistPart[0].SoftDep[8].upgradeCode" Type="Str">{4372F3E3-5935-4012-93AB-B6710CE24920}</Property>
				<Property Name="DistPart[0].SoftDep[9].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[0].SoftDep[9].productName" Type="Str">NI VC2008MSMs</Property>
				<Property Name="DistPart[0].SoftDep[9].upgradeCode" Type="Str">{FDA3F8BB-BAA9-45D7-8DC7-22E1F5C76315}</Property>
				<Property Name="DistPart[0].SoftDepCount" Type="Int">14</Property>
				<Property Name="DistPart[0].upgradeCode" Type="Str">{4722F14B-8434-468D-840D-2B0CD8CBD5EA}</Property>
				<Property Name="DistPart[1].flavorID" Type="Str">_deployment_</Property>
				<Property Name="DistPart[1].productID" Type="Str">{C7E75B1A-9155-464A-840E-A1051C52EF2D}</Property>
				<Property Name="DistPart[1].productName" Type="Str">NI-VISA Runtime 14.0.1</Property>
				<Property Name="DistPart[1].upgradeCode" Type="Str">{8627993A-3F66-483C-A562-0D3BA3F267B1}</Property>
				<Property Name="DistPartCount" Type="Int">2</Property>
				<Property Name="INST_author" Type="Str">Sirah</Property>
				<Property Name="INST_autoIncrement" Type="Bool">true</Property>
				<Property Name="INST_buildLocation" Type="Path">../Matisse Commander/Redist/Installer</Property>
				<Property Name="INST_buildLocation.type" Type="Str">relativeToCommon</Property>
				<Property Name="INST_buildSpecName" Type="Str">Matisse Commander Installer</Property>
				<Property Name="INST_defaultDir" Type="Str">{4129A2AE-7654-46C1-B689-84558664DEA7}</Property>
				<Property Name="INST_productName" Type="Str">Matisse Commander</Property>
				<Property Name="INST_productVersion" Type="Str">1.16.5</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">14018013</Property>
				<Property Name="MSI_arpCompany" Type="Str">Sirah Lasertechnik</Property>
				<Property Name="MSI_arpURL" Type="Str">http://www.sirah.com/</Property>
				<Property Name="MSI_distID" Type="Str">{E8B9245C-C160-4599-B266-9B8E89CE1274}</Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{0E1AE8E1-A26C-4E27-9391-5B04A4A16DAE}</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{4129A2AE-7654-46C1-B689-84558664DEA7}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{4129A2AE-7654-46C1-B689-84558664DEA7}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">Matisse Commander.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">0</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">Matisse Commander</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str">Sirah</Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">1</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{E9B61120-328B-47C3-AA9F-1D55DBA2174D}</Property>
				<Property Name="Source[0].FileCount" Type="Int">1</Property>
				<Property Name="Source[0].name" Type="Str">Matisse Commander</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/Matisse Commander</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="Source[1].dest" Type="Str">{4129A2AE-7654-46C1-B689-84558664DEA7}</Property>
				<Property Name="Source[1].File[0].dest" Type="Str">{4129A2AE-7654-46C1-B689-84558664DEA7}</Property>
				<Property Name="Source[1].File[0].name" Type="Str">LabVIEW - MCP Custom Plug-In.zip</Property>
				<Property Name="Source[1].File[0].tag" Type="Ref">/My Computer/Addons/LabVIEW - MCP Custom Plug-In.zip</Property>
				<Property Name="Source[1].FileCount" Type="Int">1</Property>
				<Property Name="Source[1].name" Type="Str">Addons</Property>
				<Property Name="Source[1].tag" Type="Ref">/My Computer/Addons</Property>
				<Property Name="Source[1].type" Type="Str">Folder</Property>
				<Property Name="Source[2].dest" Type="Str">{4129A2AE-7654-46C1-B689-84558664DEA7}</Property>
				<Property Name="Source[2].File[0].dest" Type="Str">{4129A2AE-7654-46C1-B689-84558664DEA7}</Property>
				<Property Name="Source[2].File[0].name" Type="Str">Matisse Programmer's Guide.pdf</Property>
				<Property Name="Source[2].File[0].tag" Type="Ref">/My Computer/Manuals/Matisse Programmer's Guide.pdf</Property>
				<Property Name="Source[2].FileCount" Type="Int">1</Property>
				<Property Name="Source[2].name" Type="Str">Manuals</Property>
				<Property Name="Source[2].tag" Type="Ref">/My Computer/Manuals</Property>
				<Property Name="Source[2].type" Type="Str">Folder</Property>
				<Property Name="Source[3].dest" Type="Str">{A70D8511-3A56-462F-ABAE-68ECC3E3F11A}</Property>
				<Property Name="Source[3].name" Type="Str">MCP External PID.llb</Property>
				<Property Name="Source[3].tag" Type="Ref">/My Computer/Plug-ins/MCP External PID.llb</Property>
				<Property Name="Source[3].type" Type="Str">File</Property>
				<Property Name="Source[4].dest" Type="Str">{A70D8511-3A56-462F-ABAE-68ECC3E3F11A}</Property>
				<Property Name="Source[4].name" Type="Str">MCP HighFinesse.llb</Property>
				<Property Name="Source[4].tag" Type="Ref">/My Computer/Plug-ins/MCP HighFinesse.llb</Property>
				<Property Name="Source[4].type" Type="Str">File</Property>
				<Property Name="Source[5].dest" Type="Str">{A70D8511-3A56-462F-ABAE-68ECC3E3F11A}</Property>
				<Property Name="Source[5].name" Type="Str">MCP Mousewheel.llb</Property>
				<Property Name="Source[5].tag" Type="Ref">/My Computer/Plug-ins/MCP Mousewheel.llb</Property>
				<Property Name="Source[5].type" Type="Str">File</Property>
				<Property Name="Source[6].dest" Type="Str">{A70D8511-3A56-462F-ABAE-68ECC3E3F11A}</Property>
				<Property Name="Source[6].name" Type="Str">MCP Wavemeter.llb</Property>
				<Property Name="Source[6].tag" Type="Ref">/My Computer/Plug-ins/MCP Wavemeter.llb</Property>
				<Property Name="Source[6].type" Type="Str">File</Property>
				<Property Name="Source[7].dest" Type="Str">{A70D8511-3A56-462F-ABAE-68ECC3E3F11A}</Property>
				<Property Name="Source[7].name" Type="Str">MCP LocalGoTo Plug-In.llb</Property>
				<Property Name="Source[7].tag" Type="Ref">/My Computer/Plug-ins/MCP LocalGoTo Plug-In.llb</Property>
				<Property Name="Source[7].type" Type="Str">File</Property>
				<Property Name="Source[8].dest" Type="Str">{A70D8511-3A56-462F-ABAE-68ECC3E3F11A}</Property>
				<Property Name="Source[8].name" Type="Str">MCP Strain Gauge PID.llb</Property>
				<Property Name="Source[8].tag" Type="Ref">/My Computer/Plug-ins/MCP Strain Gauge PID.llb</Property>
				<Property Name="Source[8].type" Type="Str">File</Property>
				<Property Name="Source[9].dest" Type="Str">{A70D8511-3A56-462F-ABAE-68ECC3E3F11A}</Property>
				<Property Name="Source[9].name" Type="Str">MCP WM Network.llb</Property>
				<Property Name="Source[9].tag" Type="Ref">/My Computer/Plug-ins/MCP WM Network.llb</Property>
				<Property Name="Source[9].type" Type="Str">File</Property>
				<Property Name="SourceCount" Type="Int">10</Property>
			</Item>
			<Item Name="MatisseTX_200417" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{C423B5A0-9131-47F6-9EF8-4B60A5F10A1B}</Property>
				<Property Name="App_INI_GUID" Type="Str">{24FF7D0F-8567-48FE-85F8-5685D3F9C7E0}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{38569A47-9E0C-424A-9E7A-F708A5BD1A36}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">MatisseTX_200417</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/MatisseTX_200417</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{700800CE-B2C2-4842-9003-0FF22460D636}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">MatisseTX_200417.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/MatisseTX_200417/MatisseTX_200417.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/MatisseTX_200417/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{CDCD5868-D6E1-4580-BFAA-73A2AEBE5839}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref"></Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_fileDescription" Type="Str">MatisseTX_200417</Property>
				<Property Name="TgtF_internalName" Type="Str">MatisseTX_200417</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2017 </Property>
				<Property Name="TgtF_productName" Type="Str">MatisseTX_200417</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{54FFD3B6-0BAF-4110-A562-17EDCA242978}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">MatisseTX_200417.exe</Property>
			</Item>
			<Item Name="MatisseDX_200417" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{D42068AE-C584-4E07-A443-E06459BC548D}</Property>
				<Property Name="App_INI_GUID" Type="Str">{F6159329-B3F9-44EC-81A9-1756C9F05FD4}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{6997245C-EF17-4276-9709-B541D055805B}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">MatisseDX_200417</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/MatisseDX_200417</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{C7222728-57C8-422A-896C-F7DE5D143CC9}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">MatisseDX_200417.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/MatisseDX_200417/MatisseDX_200417.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/MatisseDX_200417/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{CDCD5868-D6E1-4580-BFAA-73A2AEBE5839}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref"></Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_fileDescription" Type="Str">MatisseDX_200417</Property>
				<Property Name="TgtF_internalName" Type="Str">MatisseDX_200417</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2017 </Property>
				<Property Name="TgtF_productName" Type="Str">MatisseDX_200417</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{6465E26D-CF28-41D7-A2E6-8C12F4D2CBC8}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">MatisseDX_200417.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
