@REM Create Zip archives of sources
cd ..
"c:\Program Files\7-Zip\7z.exe" a -w"..\" -r ".\Dependent\Addons\LabVIEW - MCP Custom Plug-In.zip" "..\Matisse Commander Plug-ins\MCP Custom Plug-in\*"
"c:\Program Files\7-Zip\7z.exe" a -w"..\" -r ".\Dependent\Addons\LabVIEW - MCP External PID.zip" "..\Matisse Commander Plug-ins\MCP External PID\*"
"c:\Program Files\7-Zip\7z.exe" a -w"..\" -r ".\Dependent\Addons\LabVIEW - MCP HighFinesse.zip" "..\Matisse Commander Plug-ins\MCP HighFinesse\*"
"c:\Program Files\7-Zip\7z.exe" a -w"..\" -r ".\Dependent\Addons\LabVIEW - MCP LocalGoTo.zip" "..\Matisse Commander Plug-ins\MCP LocalGoTo\*"
"c:\Program Files\7-Zip\7z.exe" a -w"..\" -r ".\Dependent\Addons\LabVIEW - MCP Scrollwheel Scan.zip" "..\Matisse Commander Plug-ins\MCP ScrollwheelScan\*"
"c:\Program Files\7-Zip\7z.exe" a -w"..\" -r ".\Dependent\Addons\LabVIEW - MCP Strain Gauge PID.zip" "..\Matisse Commander Plug-ins\MCP Strain Gauge\*"
"c:\Program Files\7-Zip\7z.exe" a -w"..\" -r ".\Dependent\Addons\LabVIEW - MCP Wavemeter.zip" "..\Matisse Commander Plug-ins\MCP Wavemeter\*"
"c:\Program Files\7-Zip\7z.exe" a -w"..\" -r ".\Dependent\Addons\LabVIEW - MCP Wavemeter Custom.zip" "..\Matisse Commander Plug-ins\MCP Wavemeter Custom\*"
"c:\Program Files\7-Zip\7z.exe" a -w"..\" -r ".\Dependent\Addons\LabVIEW - MCP WM Network.zip" "..\Matisse Commander Plug-ins\MCP WM Network\*"
"c:\Program Files\7-Zip\7z.exe" a -w"..\" -x!Redist ".\Dependent\Addons\LabVIEW Wavemeter Server.zip" "..\Matisse Commander Plug-ins\Wavemeter Server\*.*"
"c:\Program Files\7-Zip\7z.exe" a -w"..\" -r ".\Dependent\Addons\LabVIEW Matisse API.zip" "..\Matisse API\*"
del ".\Dependent\Addons\LabVIEW - Matisse Commander.zip"
"c:\Program Files\7-Zip\7z.exe" a -x!Addons -w"..\" -r ".\Dependent\Addons\LabVIEW - Matisse Commander.zip" "..\Matisse Commander\Dependent" "..\Matisse Commander\Source"
"c:\Program Files\7-Zip\7z.exe" a -x!*.exe -w"..\" ".\Dependent\Addons\LabVIEW - Matisse Commander.zip" "*.*"
pause