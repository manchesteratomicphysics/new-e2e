@REM copy plug-in LLBs 
copy "..\..\Matisse Commander Plug-ins\MCP External PID\Redist\MCP*.llb" ".\Plug-Ins"
copy "..\..\Matisse Commander Plug-ins\MCP HighFinesse\Redist\MCP*.llb" ".\Plug-Ins"
copy "..\..\Matisse Commander Plug-ins\MCP LocalGoTo\Redist\MCP*.llb" ".\Plug-Ins"
copy "..\..\Matisse Commander Plug-ins\MCP Scrollwheel Scan\Redist\MCP*.llb" ".\Plug-Ins"
copy "..\..\Matisse Commander Plug-ins\MCP Strain Gauge\Redist\MCP*.llb" ".\Plug-Ins"
copy "..\..\Matisse Commander Plug-ins\MCP Wavemeter\Redist\MCP*.llb" ".\Plug-Ins"
copy "..\..\Matisse Commander Plug-ins\MCP WM Network\Redist\MCP*.llb" ".\Plug-Ins"
pause